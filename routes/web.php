<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\IgnoreKataController;
use App\Http\Controllers\KelompokKonsolidasi;
use App\Http\Controllers\KementrianController;
use App\Http\Controllers\KeteranganController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\PageController;
use App\Http\Controllers\PaketKonsolidasiController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SirupController;
use App\Http\Controllers\RepositoryController;
use App\Http\Controllers\UserController;
use App\Models\Banner;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   
    return view('welcome');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');


Route::get('/populer', function () {
    return view('tabel-populer');
});
Route::get('/kontak', [PageController::class, 'kontak'])->name('kontak');
Route::get('/contoh-konsolidasi', [PageController::class, 'contohKonsolidasi'])->name('contoh-konsolidasi');
Route::get('/test-mail', [MailController::class, 'composeEmail']);
Route::get('/test-mail-curl', [MailController::class, 'curlEmail']);
Route::get('send-mail', function () {
   
    $data = array('name'=>"Virat Gandhi");
   
      Mail::send(['text'=>'mail'], $data, function($message) {
         $message->to('dadittya.adi@gmail.com', 'Dadittya')->subject
            ('Testing Email from LKPP Project');
         $message->from('dadittya.smtp@gmail.com','Dadittya');
      });
      echo "Basic Email Sent. Check your inbox.";
});

Route::get('tahapan-konsolidasi', function () {
    return view('tahapan-konsolidasi');
})->name('tahapan-konsolidasi');

Route::get('test-api', function() {
    set_time_limit(0);
    
    $respons = Http::get('https://inaproc.lkpp.go.id/isb/api/316a729d-0df1-4bb4-90a8-76a8b6d36c3a/json/736986884/PaketPenyediaOpt1618/tipe/4:12/parameter/2021:K38');

    return $respons;
});
//Kementrian Route
Route::get('/', [KementrianController::class, 'index'])->name('kementrian-lembaga');
Route::post('/kementrian-lembaga', [KementrianController::class, 'getKl'])->name('kementrian-get-kl');
Route::post('/pemerintah-daerah', [KementrianController::class, 'getPemda'])->name('kementrian-get-pemda');
Route::post('/satker', [KementrianController::class, 'getSatker'])->name('kementrian-get-satker');
Route::get('/last-paket', [KementrianController::class, 'getLastPaket'])->name('kementrian-get-last-paket');
Route::post('/jenis-pengadaan', [KementrianController::class, 'getJenisPengadaan'])->name('kementrian-get-jenis-pengadaan');
Route::post('/kementrian-lembaga/kata-popular', [KementrianController::class, 'kataPopular'])->name('kementrian-lembaga.kata-popular');
Route::get('/kementrian-lembaga/kata-popular/data', [KementrianController::class, 'datatable'])->name('kementrian-lembaga.kata-popular-data');
Route::get('/kementrian-lembaga/kata-popular/get-jumlah-paket', [KementrianController::class, 'getJumlahPaket'])->name('kementrian-lembaga.get-jumlah-paket');
Route::get('/kementrian-lembaga/kata-popular/get-jumlah-kata', [KementrianController::class, 'getJumlahKata'])->name('kementrian-lembaga.get-jumlah-kata');
Route::post('/kementrian-lembaga/detail-paket', [KementrianController::class, 'detailPaketAsli'])->name('kementrian-lembaga.data-paket-detail');
Route::post('/kementrian-lembaga/ignore-word', [KementrianController::class, 'datatable'])->name('kementrian-lembaga.ignore-word');

//Add & Store Konsolidasi
Route::post('/store-paket-konsolidasi', [PaketKonsolidasiController::class, 'storePaket'])->name('store-paket-konsolidasi');
//Get Data Konsolidasi
Route::get('/paket-konsolidasi', [PaketKonsolidasiController::class, 'getAllData'])->name('get-paket-konsolidasi');
Route::get('/paket-konsolidasi-data', [PaketKonsolidasiController::class, 'konsolidasiDatatable'])->name('get-paket-konsolidasi-data');
Route::post('/paket-konsolidasi-delete', [PaketKonsolidasiController::class, 'konsolidasiDelete'])->name('get-paket-konsolidasi-delete');
Route::post('/paket-konsolidasi-delete-all', [PaketKonsolidasiController::class, 'konsolidasiDeleteAll'])->name('paket-konsolidasi-delete-all');


//Detail Konsolidasi
Route::get('/paket-konsolidasi-detail/{konsolidasi_id}/{klasifikasi?}', [PaketKonsolidasiController::class, 'konsolidasiDetail'])->name('get-paket-konsolidasi-detail');
Route::get('/paket-konsolidasi-detail-data', [PaketKonsolidasiController::class, 'konsolidasiDetailData'])->name('get-paket-konsolidasi-detail-data');
Route::post('/paket-konsolidasi-detail-update-resiko', [PaketKonsolidasiController::class, 'konsolidasiUpdateResiko'])->name('paket-konsolidasi-update-resiko');
Route::post('/paket-konsolidasi-detail-delete-paket-asli', [PaketKonsolidasiController::class, 'konsolidasiDeletePaket'])->name('paket-konsolidasi-detail-delete-paket-asli');
Route::post('/paket-konsolidasi-detail-delete-paket-all', [PaketKonsolidasiController::class, 'deletePaketAll'])->name('paket-konsolidasi-detail-delete-paket-asli-all');
Route::get('/paket-konsolidasi-detail-report/{anggaran}/{konsolidasi_id}/{allow_download?}/{hide_pareto?}', [ReportController::class, 'konsolidasiDetailData'])->name('get-paket-konsolidasi-detail-report');
Route::post('/paket-konsolidasi-detail-update-nama', [PaketKonsolidasiController::class, 'konsolidasiUpdateName'])->name('paket-konsolidasi-update-nama');
Route::post('/paket-konsolidasi-detail-save-selected', [PaketKonsolidasiController::class, 'saveSelected'])->name('paket-konsolidasi-detail-save-selected');

Route::post('/get-kab-kota', [KementrianController::class, 'getKabKota'])->name('get-kab-kota')->middleware('cors');
Route::get('/get-provinsi/{kl}', [KementrianController::class, 'getProvinsi'])->name('get-provinsi')->middleware('cors');

//Kelompok Paket Konsolidasi
Route::get('/kelompok-paket-konsolidasi', [KelompokKonsolidasi::class, 'index'])->name('get-kelompok-konsolidasi');
Route::post('/kelompok-paket-konsolidasi', [KelompokKonsolidasi::class, 'index'])->name('get-kelompok-konsolidasi-post');
Route::post('/kelompok-paket-konsolidasi/delete', [KelompokKonsolidasi::class, 'delete'])->name('kelompok-konsolidasi-delete');
Route::get('/kelompok-paket-konsolidasi-kecil-tinggi', [KelompokKonsolidasi::class, 'kondisiPertama'])->name('kelompok-konsolidasi-kecil-tinggi');
Route::get('/kelompok-paket-konsolidasi-besar-tinggi', [KelompokKonsolidasi::class, 'kondisiKedua'])->name('kelompok-konsolidasi-besar-tinggi');
Route::get('/kelompok-paket-konsolidasi-kecil-rendah', [KelompokKonsolidasi::class, 'kondisiKetiga'])->name('kelompok-konsolidasi-kecil-rendah');
Route::get('/kelompok-paket-konsolidasi-besar-rendah', [KelompokKonsolidasi::class, 'kondisiKeempat'])->name('kelompok-konsolidasi-besar-rendah');


Route::get('/reload-captcha', [AdminController::class, 'reloadCaptcha']);
//Route::get('/helper/admin/activate', [HelperController::class, 'setAdmin']);
//Route::get('/helper/ignore/clear', [HelperController::class, 'clearIgnoreList']);
Route::get('/helper/log/get', [HelperController::class, 'getLog']);
Route::get('/helper/errorlog/get', [HelperController::class, 'getErrorLog']);
Route::get('/helper/errorlog/clear', [HelperController::class, 'clearErrorLog']);
//Route::get('/helper/banner/rename', [HelperController::class, 'renameBannerFile']);
Route::get('/helper/rup/sync/{year}/{instansiId}', [SirupController::class, 'getRup']);
Route::get('/helper/satker/sync/{year}/{instansiId}', [SirupController::class, 'getSatker']);
Route::get('/helper/instansi/sync', [SirupController::class, 'getInstansi']);
Route::get('/helper/reset/myPassword', [HelperController::class, 'resetMyPassword']);
Route::get('/helper/addadmin/{email}', [HelperController::class, 'addAdmin']);
Route::get('/helper/removeadmin/{email}', [HelperController::class, 'removeAdmin']);

Route::prefix('report')->name('report.')->middleware('cors')->group(function () {
    Route::get('/chart', [ReportController::class, 'index']);
    Route::get('/data', [ReportController::class, 'getData'])->name('data');
    Route::get('/anggaran_pengadaan', [ReportController::class, 'anggaran_pengadaan'])->name('anggaran_pengadaan');
    Route::get('/ringkasan', [ReportController::class, 'ringkasan'])->name('ringkasan');
    Route::get('/ringkasan-data', [ReportController::class, 'getRes'])->name('ringkasan.data');
    Route::post('/datatable', [ReportController::class, 'datatable'])->name('datatable');
    Route::get('/anggaran_terbesar', [ReportController::class, 'anggaranTerbesar'])->name('anggaran_terbesar');
    Route::get('/download', [ReportController::class, 'download'])->name('download');
    Route::get('/pareto', [ReportController::class, 'pareto'])->name('pareto');
    Route::get('/pareto/page/{instansiId}/{tahun}', [ReportController::class, 'paretoPage'])->name('pareto.page');
});

Route::middleware(['auth', 'IsAdminMiddleware'])->prefix('admin')->name('admin.')->group(function () {
    Route::get('dashboard', [AdminController::class, 'index'])->name('dashboard');
    Route::prefix('artikel')->name('artikel.')->group(function () {
        Route::get('/', [ArtikelController::class, 'index'])->name('index');
        Route::get('tambah', [ArtikelController::class, 'create'])->name('tambah');
        Route::post('store', [ArtikelController::class, 'store'])->name('store');
        Route::get('edit/{id}', [ArtikelController::class, 'edit'])->name('edit');
        Route::put('update/{id}', [ArtikelController::class, 'update'])->name('update');
        Route::get('datatable', [ArtikelController::class, 'datatableArtikel'])->name('datatable');
        Route::delete('destroy/{id}', [ArtikelController::class, 'destroy'])->name('destroy');
    });

    Route::prefix('repository')->name('repository.')->group(function () {
        Route::get('/', [RepositoryController::class, 'index'])->name('index');
        Route::get('tambah', [RepositoryController::class, 'create'])->name('tambah');
        Route::post('store', [RepositoryController::class, 'store'])->name('store');
        Route::get('edit/{id}', [RepositoryController::class, 'edit'])->name('edit');
        Route::put('update/{id}', [RepositoryController::class, 'update'])->name('update');
        Route::get('datatable', [RepositoryController::class, 'datatableRepository'])->name('datatable');
        Route::delete('destroy/{id}', [RepositoryController::class, 'destroy'])->name('destroy');
    });

    Route::prefix('user')->name('user.')->group(function () {
        Route::get('/', [UserController::class, 'index'])->name('index');
        //Route::get('tambah', [RepositoryController::class, 'create'])->name('tambah');
        //Route::post('store', [RepositoryController::class, 'store'])->name('store');
        //Route::get('edit/{id}', [RepositoryController::class, 'edit'])->name('edit');
        //Route::put('update/{id}', [RepositoryController::class, 'update'])->name('update');
        Route::get('datatable', [UserController::class, 'datatableUser'])->name('datatable');
        //Route::delete('destroy/{id}', [RepositoryController::class, 'destroy'])->name('destroy');
        Route::post('verified/{id}', [UserController::class, 'verified'])->name('verified');
    });

    Route::prefix('banner')->name('banner.')->group(function () {
        Route::get('/', [BannerController::class, 'index'])->name('index');
        Route::get('tambah', [BannerController::class, 'create'])->name('tambah');
        Route::post('store', [BannerController::class, 'store'])->name('store');
        Route::get('edit/{id}', [BannerController::class, 'edit'])->name('edit');
        Route::put('update/{id}', [BannerController::class, 'update'])->name('update');
        Route::delete('destroy/{id}', [BannerController::class, 'destroy'])->name('destroy');
        Route::get('datatable', [BannerController::class, 'datatableBanner'])->name('datatable');
    });

    Route::prefix('ignore-kata')->name('ignore-kata.')->group(function () {
        Route::get('/', [IgnoreKataController::class, 'index'])->name('index');
        Route::get('tambah', [IgnoreKataController::class, 'create'])->name('tambah');
        Route::post('store', [IgnoreKataController::class, 'store'])->name('store');
        Route::get('edit/{id}', [IgnoreKataController::class, 'edit'])->name('edit');
        Route::put('update/{id}', [IgnoreKataController::class, 'update'])->name('update');
        Route::delete('destroy/{id}', [IgnoreKataController::class, 'destroy'])->name('destroy');
        Route::get('datatable', [IgnoreKataController::class, 'datatable'])->name('datatable');
        Route::post('import', [IgnoreKataController::class, 'import'])->name('import');
    });

    Route::prefix('keterangan')->name('keterangan.')->group(function () {
        Route::get('/', [KeteranganController::class, 'index'])->name('index');
        Route::get('datatable', [KeteranganController::class, 'datatable'])->name('datatable');
        Route::get('tambah', [KeteranganController::class, 'create'])->name('tambah');
        Route::post('store', [KeteranganController::class, 'store'])->name('store');
        Route::get('edit/{id}', [KeteranganController::class, 'edit'])->name('edit');
        Route::put('update/{id}', [KeteranganController::class, 'update'])->name('update');
        Route::delete('destroy/{id}', [KeteranganController::class, 'destroy'])->name('destroy');
    });

    Route::prefix('konsolidasi')->name('konsolidasi.')->group(function () {
        Route::get('/', [AdminController::class, 'listKonsolidasi'])->name('index');
        Route::get('datatable', [AdminController::class, 'konsolidasiData'])->name('datatable');
        Route::post('datatable-detail', [AdminController::class, 'konsolidasiDataDetail'])->name('datatable-detail');
    });

});


require __DIR__ . '/auth.php';
