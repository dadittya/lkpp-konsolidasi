<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Password anda telah di reset',
    'sent' => 'Tautan untuk reset password telah dikirim',
    'throttled' => 'Tunggu beberapa saat kemudia coba lagi',
    'token' => 'Tautan ini tidak berfungsi, silakan request ulang',
    'user' => "User tidak ditemukan",

];
