@extends('web.layouts.global')
@section('slider')
@include('web.layouts.utils.slider')
@endsection
@section('content')
<section>
    <div class="col-lg-12 " style="z-index: 9999999 !important; margin-top: -130px;">
        <div class="card rounded-pill shadow p-5">
            <div class="card-body">
                <form id="form-filter" action="" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label for="" class="text-bold mb-2">Pilih KL/Pemda</label>
                                <div >
                                    <select name="kl[]" id="kl" class="form-control select2 p-5 " style="width: 100%; border:0px; outline:none;" multiple >
                                        <option value="">-- Pilih --</option>
                                        {{-- <option selected value="Pemerintah Daerah Kota Bau-Bau">Pemerintah Daerah Kota Bau-Bau</option>
                                        <option value="Pemerintah Daerah Kota Bekasi">Pemerintah Daerah Kota Bekasi</option> --}}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="text-bold mb-2">Satker</label>
                                <div >
                                    <select name="satker[]" id="satker" class="form-control select2" style="width: 100%; border:0px; outline:none;" multiple>
                                        <option value="">-- Pilih --</option>
                                        {{-- <option value="DINAS PENANAMAN MODAL DAN PTSP KOTA BAUBAU">DINAS PENANAMAN MODAL DAN PTSP KOTA BAUBAU</option>
                                        <option value="BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH">BADAN PENGELOLAAN KEUANGAN DAN ASET DAERAH</option> --}}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="text-bold mb-2">Jenis Pengadaan</label>
                                <div >
                                    <select name="jenis_pengadaan[]" id="jenis_pengadaan" class="form-control select2" style="width: 100%; border:0px; outline:none;" multiple>
                                        {{-- <option value="">-- Pilih --</option> --}}
                                    </select>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-md-4 ">
                            <div class="form-group">
                                <label for="" class="text-bold mb-2">Pemerintah Daerah</label>
                                <div >
                                    <select name="pemda[]" id="pemda" class="form-control select2" style="width: 100%; border:0px; outline:none;" multiple>
                                        <option value="" selected>-- Pilih --</option>
                                    </select>
                                </div>
                            </div>
                        </div> --}}
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label for="" class="text-bold mb-2">Pilih Provinsi</label>
                                <div >
                                    <select name="provinsi[]" id="provinsi" class="form-control select2" style="width: 100%; border:0px; outline:none;" >
                                        <option value="" selected>-- Pilih --</option>
                                        {{-- <option value="Sulawesi Tenggara">Sulawesi Tenggara</option>
                                        <option value="Jawa Barat">Jawa Barat</option> --}}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label for="" class="text-bold mb-2">Pilih Kabupaten/Kota</label>
                                <div >
                                    <select name="pemkot[]" id="pemkot" class="form-control select2" style="width: 100%; border:0px; outline:none;" >
                                        <option value="" selected>-- Pilih --</option>
                                        {{-- <option value="Baubau (Kota)">Baubau (Kota)</option>
                                        <option value="Bekasi (Kota)">Bekasi (Kota)</option> --}}
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="" class="text-bold mb-2">Pilih Tahun</label>
                                <div >
                                    <select name="tahun_anggaran" id="tahun_anggaran" class="form-control select2" style="width: 100%; border:0px; outline:none;">
                                        @foreach ($tahun_anggaran as $item)
                                        <option value="{{ $item->nama }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 mt-2  float-end px-3">
                        <button type="submit" id="btn_cari" class="btn  btn-outline-secondary btn-rounded ml-3 mr-5 float-end" style="border-radius:36px;">Cari</button>
                        <div class="float-end">
                            <label for="suku_kata">Jumlah Kata</label>
                        <select name="suku_kata" id="suku_kata">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                        </select>
                        </div>
                        {{-- <button type="submit" id="report" class="btn  btn-outline-secondary btn-rounded mt-3 float-end ml-1" style="border-radius:36px;">Get Report</button> --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

@endsection

@push('script')
<script>
    const CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function() {
        //Get Data Provinsi
        getProv()
        getKlpd()
        getJenis()

        //Select2
        let kl = $('#kl').select2({
            placeholder: " --Pilih Kementrian / Lembaga-- "
        });
        let satker = $('#satker').select2({
            placeholder: "--Pilih--",
            disabled: true
        })
        let jenisPengadaan = $('#jenis_pengadaan').select2({
            placeholder: "--Pilih--",
            disabled: false
        })
        let provinsi = $('#provinsi').select2({
            placeholder: "--Pilih--",
            disabled: true
        })
        let pemkot = $('#pemkot').select2({
            placeholder: "--Pilih--",
            disabled: true
        })
        let tahun = $('#tahun_anggaran').select2({
            placeholder: "--Pilih--"
        })

        // const selectKl = $("#kl").select2({
        //     ajax: {
        //         url: "{{ route('kementrian-get-kl') }}"
        //         , type: 'POST'
        //         , dataType: 'json'
        //         , delay: 250
        //         , data: function(params) {
        //                 return {
        //                     _token: CSRF_TOKEN
        //                     , q: params.term, // search term
        //                     page: params.current_page
        //                 };

        //                 return {
        //                     _token: CSRF_TOKEN
        //                     , term: params.term || ''
        //                     , page: params.page || 1
        //                 }

        //             }
        //             , processResults: function(data, params) {
        //                 params.current_page = params.current_page || 1;

        //                 return {
        //                     results: data.results
        //                     , pagination: {
        //                         more: (params.current_page * 30) < data.total
        //                     }
        //                 };
        //             }
        //         , autoWidth: true
        //         , cache: true
        //     }
        //     , placeholder: ' --Pilih Kementrian / Lembaga-- '
        // });
    });

    $(document).ready(function() {
        $('#kl').change(function() {
            $('#satker').prop("disabled", false);
            $('#provinsi').prop("disabled", false);
            $('#satker').prop('selectedIndex', 0);
            //$('#jenis_pengadaan').prop('selectedIndex', 0);
            // let lembagas = $("select[name='kl[]']").map(function() {
            //     return $(this).text();
            // }).get();

            let selected = $('#kl').select2("data");
            let lembagas = [];
            for (let i = 0; i <= selected.length - 1; i++) {
                lembagas.push(selected[i].id)


                if (selected[i].text.substring(0,10) == "Pemerintah") {
                    $('#provinsi').prop("disabled", true);
                }
            }

            $.ajax({
                type: "POST"
                , url: "{{ route('kementrian-get-satker') }}"
                , dataType: "json"
                , data: {
                    lembagas: lembagas
                    , _token: "{{ csrf_token() }}"
                }
                , success: function(response) {
                    //console.log(response);
                    $('#satker').empty();
                    $('#satker').append('<option value="">-- Pilih --</option>');
                    $.each(response, function(i, value) {
                        //console.log(value);
                        opt = `<option value="${value}">${i}</option>`
                        $('#satker').append(opt)
                    });
                }
            });
        });

        // $('#satker').change(function() {
        //     let selected = $('#satker').select2("data");
        //     let satker = [];
        //     for (let i = 0; i <= selected.length - 1; i++) {
        //         satker.push(selected[i].text)
        //     }

            
         //});

        $(document).on('change', '#provinsi', function() {
            $('#pemkot').prop("disabled", false);
            $('#pemkot').prop('selectedIndex', 0);
            //$('#tahun_anggaran').prop('selectedIndex', 0);
            let p = $(this).val()
            let opt
            let headers = {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                , 'Access-Control-Allow-Origin': '*'
            }

            let selected = $('#provinsi').select2("data");
            let provinsi = [];
            for (let i = 0; i <= selected.length - 1; i++) {
                provinsi.push(selected[i].id)
            }


            $.ajax({
                type: "POST"
                , url: "{{ url('get-kab-kota') }}"
                , data: {
                    provinsi : provinsi,
                    _token: CSRF_TOKEN
                }
                , dataType: "json"
                , success: function(response) {
                    $('#pemkot').empty();
                    $('#pemkot').append('<option value="">-- Pilih --</option>');
                    $.each(response, function(i, value) {
                        opt = `<option value=${value}>${i}</option>`
                        $('#pemkot').append(opt)
                    });
                }
            });

        })

        $('#pemkot').change(function() {
            //$('#tahun_anggaran').prop('selectedIndex', 0);
        });

    });

    $('#report').on('click', function() {
        $('#form-filter').attr('action', "{{ route('report.anggaran_pengadaan') }}")
    })
    $('#btn_cari').on('click', function() {
        $('#form-filter').attr('action', "{{ route('kementrian-lembaga.kata-popular') }}")
    })

    const getProv = () => {
        $.ajax({
            type: "get"
            , url: "{{ url('get-provinsi') }}/" + kl
            , dataType: "json"
            , data: {
                _token: "{{ csrf_token() }}"
            }
            , success: function(response) {
                $('#provinsi').empty();
                $('#provinsi').append('<option value="">-- Pilih --</option>');
                $.each(response, function(i, value) {
                    opt = `<option value="${value}">${i}</option>`
                    $('#provinsi').append(opt)
                });
            }
        });
    }
    const getKlpd = () => {
        $.ajax({
            type: "POST"
            , url: "{{ route('kementrian-get-kl') }}"
            , dataType: "json"
            , data: {
                _token: "{{ csrf_token() }}"
            }
            , success: function(response) {
                
                $('#kl').empty();
                $('#kl').append('<option value="">-- Pilih --</option>');
                $.each(response, function(i, value) {
                    opt = `<option value="${value}">${i}</option>`
                    $('#kl').append(opt)
                });
            }
        });
    }

    const getJenis = () => {
        $.ajax({
                 type: "POST"
                 , url: "{{ route('kementrian-get-jenis-pengadaan') }}"
                 , dataType: "json"
                 , data: {
                     _token: "{{ csrf_token() }}"
                 }
                 , success: function(response) {
                     //console.log(response.jenisPengadaan)
                     $('#jenis_pengadaan').empty();
                     //$('#jenis_pengadaan').append('<option value="">-- Pilih --</option>');
                     $.each(response, function(i, value) {
                         //console.log(value.jenis_pengadaan)
                         opt = `<option value="${value}">${i}</option>`
                         $('#jenis_pengadaan').append(opt)
                     });
                 }
             });

    }


</script>
@endpush
