@extends('admin.layouts.app')

@section('title', 'Artikel')

@section('content')
<div class="row">
    <div class="col-12">
        <a href="{{ route('admin.artikel.tambah') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
            class="fas fa-plus fa-sm text-white-50"></i> Tambah Artikel
        </a>
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Artikel</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="table-artikel" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Judul Artikel</th>
                                <th>Deskripsi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $(document).ready(function(){
            $('#table-artikel').DataTable({
                serverSide: true,
                processing: true,
                scrollX: true,
                order: [[ 1, "desc" ]],
                language: {
                    searchPlaceholder: "Cari ..."
                },
                bFilter: true,
                ajax: {
                    url: "{{ route('admin.artikel.datatable') }}",
                    type: 'get'
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        orderable: false, 
                        searchable: false
                    },
                    {
                        name: 'judul',
                        data: 'judul'
                    },
                    {
                        name: 'plain_text',
                        render: function(type,data,row){
                            return row.plain_text.substr(0,150)
                        }
                    },
                    {
                        name: 'aksi',
                        data: 'aksi',
                    },
                ],
            })
        })
    </script>
@endpush