@extends('admin.layouts.app')

@section('title', 'Tambah Artikel')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card shadow my-4">
            <div class="card-body">
                <div class="col-12">
                    <div class="p-2">
                        <form class="user" action="{{ route('admin.artikel.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-4">
                                <div class="col-6">
                                    <label>Judul</label>
                                    <input type="text" class="form-control form-control-user"
                                        placeholder="Judul Artikel" name="judul">
                                    @error('judul')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-6">
                                    <label>Foto</label>
                                    <input type="file" class="form-control form-control-user" name="foto">
                                    @error('foto')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-10">
                                    <label>Body</label>
                                    <textarea id="summernote" name="body"></textarea>
                                    @error('body')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 400,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: true
            });
        });
    </script>
@endpush