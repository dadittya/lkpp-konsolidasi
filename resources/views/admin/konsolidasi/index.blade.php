@extends('admin.layouts.app')

@section('title', 'Konsolidasi')

@section('content')
    <div class="row">
        <div class="col-12">
            {{-- <a href="{{ route('admin.keterangan.tambah') }}"
                class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-plus fa-sm text-white-50"></i> Tambah Keterangan
            </a> --}}
            <div class="card shadow my-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Data Konsolidasi</h6>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="table-konsolidasi" style="width: 100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Paket Konsolidasi</th>
                                    <th>Tingkat Resiko</th>
                                    <th>User Pembuat</th>
                                    <th>#</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalKonsolidasi" tabindex="-1" aria-labelledby="modalKonsolidasiLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Detail Paket Konsolidasi</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Data Konsolidasi</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="table-konsolidasi-detail" style="width: 100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Satuan Kerja</th>
                                            <th>Nama Paket</th>
                                            <th>Pagu Anggaran</th>
                                            <th>Kumulatif Pagu</th>
                                            <th>Persentase Terhadap Total</th>
                                            <th>Klasifikasi Anggaran</th>
                                            <th>Metode Pengadaan</th>
                                            <th>Sumber Dana</th>
                                            <th>Kode Rup</th>
                                            {{-- <th>Created At</th> --}}
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#table-konsolidasi').DataTable({
                serverSide: true,
                processing: true,
                scrollX: true,
                order: [
                    [1, "desc"]
                ],
                language: {
                    searchPlaceholder: "Cari ..."
                },
                bFilter: true,
                ajax: {
                    url: "{{ route('admin.konsolidasi.datatable') }}",
                    type: 'get'
                },
                columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        name: 'nama_paket_konsolidasi',
                        data: 'nama_paket_konsolidasi'
                    },
                    {
                        name: 'tingkat_resiko',
                        data: 'tingkat_resiko',
                        render: function (data,type,row) {
                            if(row.tingkat_resiko !== null){
                                return `<a>${row.tingkat_resiko}</a>`
                            }
                            return `<a>-</a>`
                        }
                    },
                    {
                        name: 'user.user.name',
                        data: 'user.user.name'
                    },
                    {
                        name: 'aksi',
                        data: 'aksi',
                        render: function(data, type, row) {
                            return `<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modalKonsolidasi" id="btn_detail" onclick="detail(${row.id})">Lihat</button>`
                        }
                    }
                ],
            })
        });

        const detail = (id) => {
            $('#table-konsolidasi-detail').DataTable().destroy();
            $('#table-konsolidasi-detail').DataTable({
                serverSide: true,
                processing: true,
                scrollX: true,
                order: [
                    [1, "desc"]
                ],
                language: {
                    searchPlaceholder: "Cari ..."
                },
                bFilter: true,
                ajax: {
                    url: "{{ route('admin.konsolidasi.datatable-detail') }}",
                    type: 'post',
                    data: {
                        id,
                        _token: "{{ csrf_token() }}"
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        name: 'nama_satker',
                        data: 'nama_satker'
                    },
                    {
                        name: 'nama_paket_asli',
                        data: 'nama_paket_asli'
                    },
                    {
                        name: 'pagu_anggaran',
                        data: 'pagu_anggaran'
                    },
                    {
                        name: 'kumulatif_pagu',
                        data: 'kumulatif_pagu'
                    },
                    {
                        name: 'persentase_terhadap_total',
                        data: 'persentase_terhadap_total'
                    },
                    {
                        name: 'klasifikasi_anggaran',
                        data: 'klasifikasi_anggaran'
                    },
                    {
                        name: 'metode_pengadaan',
                        data: 'metode_pengadaan'
                    },
                    {
                        name: 'sumber_dana',
                        data: 'sumber_dana'
                    },
                    {
                        name: 'id_sirup',
                        data: 'id_sirup'
                    },
                    // {
                    //     name: 'created_at',
                    //     data: 'created_at'
                    // }
                ],
            })
        }
    </script>
@endpush
