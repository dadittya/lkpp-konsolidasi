@extends('admin.layouts.app')

@section('title', 'Ignore Kata')

@section('content')
<div class="row">
    <div class="col-12">
        <a href="{{ route('admin.ignore-kata.tambah') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
            class="fas fa-plus fa-sm text-white-50"></i> Tambah Kata
        </a>
        <form method="post" action="{{ route('admin.ignore-kata.import') }}" enctype="multipart/form-data">
            <div class="card-header py-3">
                <div class="modal-body">

                    {{ csrf_field() }}

                    <label>Pilih file excel</label>
                    <div class="form-group">
                        <input type="file" name="file" required="required">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Import</button>
                </div>
            </div>
        </form>
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Ignore Kata</h6>
            </div>
            
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="table-artikel" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kata</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $(document).ready(function(){
            $('#table-artikel').DataTable({
                serverSide: true,
                processing: true,
                scrollX: true,
                order: [[ 1, "desc" ]],
                language: {
                    searchPlaceholder: "Cari ..."
                },
                bFilter: true,
                ajax: {
                    url: "{{ route('admin.ignore-kata.datatable') }}",
                    type: 'get'
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        orderable: false, 
                        searchable: false
                    },
                    {
                        name: 'kata',
                        data: 'kata'
                    },
                    {
                        name: 'aksi',
                        data: 'aksi',
                    },
                ],
            })
        })
    </script>
@endpush