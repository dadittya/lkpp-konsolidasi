@extends('admin.layouts.app')

@section('title', 'Tambah Banner')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card shadow my-4">
            <div class="card-body">
                <div class="col-12">
                    <div class="p-2">
                        <form class="user" action="{{ route('admin.banner.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row mb-4">
                                <div class="col-6">
                                    <label>Nama</label>
                                    <input type="text" class="form-control form-control-user" name="nama" placeholder="Judul Banner">
                                    @error('nama')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-6">
                                    <label>Image</label>
                                    {{-- <div class="dropzone" id="dropzone">
                                    </div> --}}
                                    <input type="file" name="file">
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    var uploadedDocumentMap = {}
    Dropzone.options.dropzone = {
        url: "{{ url('admin.banner.store') }}",
        maxFilesize: 2, // MB
        addRemoveLinks: true,
        headers: {
        'X-CSRF-TOKEN': "{{ csrf_token() }}"
        },
        success: function (file, response) {
            console.log(response)
            $('form').append('<input type="hidden" name="document[]" value="' + response.name + '">')
            uploadedDocumentMap[file.name] = response.name
        },
        removedfile: function (file) {
            file.previewElement.remove()
            var name = ''
            if (typeof file.file_name !== 'undefined') {
                name = file.file_name
            } else {
                name = uploadedDocumentMap[file.name]
            }
            $('form').find('input[name="document[]"][value="' + name + '"]').remove()
        },
        init: function () {
            // @if(isset($project) && $project->document)
            //     var files =
            //     {!! json_encode($project->document) !!}
            //     for (var i in files) {
            //         var file = files[i]
            //         this.options.addedfile.call(this, file)
            //         file.previewElement.classList.add('dz-complete')
            //         $('form').append('<input type="hidden" name="document[]" value="' + file.file_name + '">')
            //     }
            // @endif
        }
    }

</script>
@endpush