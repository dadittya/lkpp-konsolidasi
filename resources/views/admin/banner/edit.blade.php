@extends('admin.layouts.app')

@section('title', 'Banner')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Detail Banner</h6>
            </div>
            <div class="card-body">
                <h3>{{$banner->nama}}</h3>
                <img src="{{ asset('images/'. $banner->images) }}" width="300">
            </div>
        </div>
    </div>
</div>
@endsection