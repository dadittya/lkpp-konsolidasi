@extends('admin.layouts.app')

@section('title', 'Edit Repository')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card shadow my-4">
            <div class="card-body">
                <div class="col-12">
                    <div class="p-2">
                        <form class="user" action="{{ route('admin.repository.update', ['id' => $repository->id]) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="row mb-4">
                                <div class="col-6">
                                    <label>Judul</label>
                                    <input type="text" class="form-control form-control-user"
                                        placeholder="Nama" name="name" value="{{ $repository->name }}">
                                    @error('name')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-10">
                                    <label>Deskripsi</label>
                                    <input type="text" class="form-control form-control-user"
                                        placeholder="Description" name="description" value="{{ $repository->description }}">
                                    @error('description')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-6">
                                    <label>URL</label>
                                    <input type="text" class="form-control form-control-user"
                                        placeholder="youtube.com" name="url" value="{{ $repository->url }}">
                                    @error('url')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 400,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: true
            });
        });
    </script>
@endpush