<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('kementrian-lembaga') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">LKPP</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('admin.dashboard') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.user.index') }}">
            <i class="fas fa-fw fa-table"></i>
            <span>User</span>
        </a>
    </li> 
    {{-- <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.artikel.index') }}">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Artikel</span></a>
    </li> --}}

    <!-- Nav Item - Tables -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.repository.index') }}">
            <i class="fas fa-fw fa-table"></i>
            <span>Repository</span>
        </a>
    </li> 

    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.ignore-kata.index') }}">
            <i class="fas fa-fw fa-table"></i>
            <span>Ignore Kata</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.keterangan.index') }}">
            <i class="fas fa-fw fa-table"></i>
            <span>Keterangan</span>
        </a>
    </li>

    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.konsolidasi.index') }}">
            <i class="fas fa-fw fa-table"></i>
            <span>List Konsolidasi</span>
        </a>
    </li>

</ul>
