@extends('admin.layouts.app')

@section('title', 'Edit Keterangan')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card shadow my-4">
            <div class="card-body">
                <div class="col-12">
                    <div class="p-2">
                        <form class="user" action="{{ route('admin.keterangan.update', $data->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="row mb-4">
                                <div class="col-6">
                                    <label>Title</label>
                                    <input type="text" class="form-control form-control-user"
                                        placeholder="Titel Keterangan" name="title" value="{{ $data->title }}">
                                    @error('title')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-6">
                                    <label>Tipe</label>
                                    <input type="text" class="form-control form-control-user"
                                        placeholder="Tipe Keterangan" name="tipe" value="{{ $data->tipe }}">
                                    @error('tipe')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-10">
                                    <label>Konten</label>
                                    <textarea id="summernote" name="konten">{{ $data->konten }}</textarea>
                                    @error('konten')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 400,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: true
            });
        });
    </script>
@endpush