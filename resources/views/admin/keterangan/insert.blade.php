@extends('admin.layouts.app')

@section('title', 'Tambah Keterangan')

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card shadow my-4">
            <div class="card-body">
                <div class="col-12">
                    <div class="p-2">
                        <form class="user" action="{{ route('admin.keterangan.store') }}" method="POST">
                            @csrf
                            <div class="row mb-4">
                                <div class="col-6">
                                    <label>Title</label>
                                    <input type="text" class="form-control form-control-user"
                                        placeholder="Titel Keterangan" name="title">
                                    @error('title')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-6">
                                    <label>Tipe</label>
                                    <select class="form-control" name="tipe">
                                        <option value="0" selected>Supply Position Model</option>
                                        <option value="1">Supplier Perception Model 2</option>
                                      </select>
                                    @error('tipe')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-10">
                                    <label>Konten</label>
                                    <textarea id="summernote" name="konten"></textarea>
                                    @error('konten')
                                        <span class="text-danger">{{$message}}</span>
                                    @enderror
                                </div>
                            </div>
                            <div class="row mb-4">
                                <div class="col-3">
                                    <button type="submit" class="btn btn-primary btn-user btn-block">
                                        Simpan
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#summernote').summernote({
                height: 400,                 // set editor height
                minHeight: null,             // set minimum height of editor
                maxHeight: null,             // set maximum height of editor
                focus: true
            });
        });
    </script>
@endpush
