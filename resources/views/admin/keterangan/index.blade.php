@extends('admin.layouts.app')

@section('title', 'Keterangan')

@section('content')
<div class="row">
    <div class="col-12">
        <a href="{{ route('admin.keterangan.tambah') }}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i
            class="fas fa-plus fa-sm text-white-50"></i> Tambah Keterangan
        </a>
        <div class="card shadow my-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">Data Keterangan</h6>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="table-keterangan" style="width: 100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Titel</th>
                                <th>Deskripsi</th>
                                <th>Tipe</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
    <script>
        $(document).ready(function(){
            $('#table-keterangan').DataTable({
                serverSide: true,
                processing: true,
                scrollX: true,
                order: [[ 1, "desc" ]],
                language: {
                    searchPlaceholder: "Cari ..."
                },
                bFilter: true,
                ajax: {
                    url: "{{ route('admin.keterangan.datatable') }}",
                    type: 'get'
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        name: 'title',
                        data: 'title'
                    },
                    {
                        name: 'konten_text',
                        data: 'konten_text'
                    },
                    {
                        name: 'tipe',
                        data: 'tipe',
                        render: function(data, type, row){
                            return row.tipe == "0"? "Supply Position Model" : "Supplier Perception Model"
                        }
                    },
                    {
                        name: 'aksi',
                        data: 'aksi',
                    },
                ],
            })
        })
    </script>
@endpush
