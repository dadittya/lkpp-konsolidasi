@extends('web.layouts.global')
@section('content')
    <a id="btn_back" class="float-start btn btn-outline-primary shadow">Back To Filter <i class="fas fa-arrow-circle-left ml-2"></i></a>
    <div class="row mt-10 py-3">
        <div class="col-12">
            <div class="card mb-4">
                <div class="card-body text-wrap">
                    <h5 class="fw-bold text-lg">Kata Popular Di :</h5>
                    @foreach ($strLembaga as $item)
                        <div class="mt-1  badge bg-primary">{{ $item }}</div>
                    @endforeach
                    @foreach ($strSatker as $item)
                        <div class="m-1  badge bg-primary">{{ $item }}</div>
                    @endforeach
                    @foreach ($jenisStr as $item)
                        <div class="m-1  badge bg-primary">{{ $item }}</div>
                    @endforeach
                    @foreach ($strProvinsi as $item)
                        <div class="m-1  badge bg-primary">{{ $item }}</div>
                    @endforeach
                    @foreach ($strKota as $item)
                        <div class="m-1  badge bg-primary">{{ $item }}</div>
                    @endforeach 
                    <div class="m-1 badge bg-primary">Tahun {{ $tahun_anggaran }}</div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8">
                    <div class="card shadow-sm">
                        <div class="card-header">
                            {{-- <h5 class="fw-bolder">{{$provinsi}}</h5> --}}
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <form action="" method="POST" id="form_word">
                                    @csrf
                                    @foreach ($satker[0] as $s)
                                        <input type="hidden" name="satker[]" id="" value="{{ $s ? $s : '' }}">
                                    @endforeach
                                    @foreach ($jenisPengadaan[0] as $jp)
                                        <input type="hidden" name="jenis_pengadaan[]" id="" value="{{ $jp ? $jp : '' }}">
                                    @endforeach
                                    @foreach ($provinsi[0] as $p)
                                        <input type="hidden" name="provinsi[]" id="" value="{{ $p ? $p : '' }}">
                                    @endforeach
                                    @foreach ($kota[0] as $k)
                                        {{-- @dd($k) --}}
                                        <input type="hidden" name="kota[]" id="" value="{{ $k ? $k : '' }}">
                                    @endforeach
                                    @foreach ($lembaga[0] as $item)
                                        <input type="hidden" name="lembaga[]" id="" value="{{ $item ? $item : '' }}">
                                    @endforeach
                                    <input type="hidden" name="tahun_anggaran" id="" value="{{ $tahun_anggaran }}">
                                    <table id="table_1" class="table table-striped table-hover border-primary "
                                        style="width:100%">
                                        <thead class="table-primary">
                                            <tr>
                                                {{-- <th>No <i class="fas fa-sort"></i></th> --}}
                                                <th class="text-center"><input type="checkbox" name="select" id="select">
                                                </th>
                                                {{-- <th>#</th> --}}
                                                <th>Kata Populer <i class="fas fa-sort"></i></th>
                                                <th id="t_jumlah_kata">Jumlah Kata <i class="fas fa-sort"></i></th>
                                                <th id="t_jumlah_paket">Jumlah Paket <i class="fas fa-sort"></i></th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody id="t_body">

                                        </tbody>
                                    </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card shadow-sm mb-3 p-4">
                        <label for="" class="form-label">Nama Paket Konsolidasi</label>
                        <input class="form-control d-inline" autocomplete="off" id="nama_paket"
                            name="nama_paket_konsolidasi" type="text" placeholder="Masukan Nama Paket"
                            aria-label="default input example">
                        <button class="btn btn-outline-primary d-inline mt-2" data-bs-toggle="tooltip"
                            data-bs-placement="top" title="Click Untuk Mendapatkan Paket Berdasarkan Kata Yang Dipilih"
                            id="btn_proses">
                            <span hidden id="spinner" class="spinner-grow spinner-grow-sm" role="status"
                                aria-hidden="true"></span> Buat Usulan Paket Konsolidasi</button>
                    </div>
                    </form>
                    <div class="row">
                        <div class="col-12">
                            <div class="card shadow-sm">
                                <div class="card-header">
                                    Daftar Potensi Konsolidasi
                                </div>
                                <div class="card-body">
                                    <ul class="list-group" id="list_paket">
                                        {{-- @forelse ($data as $item)
                                    <li class="list-group-item" data-bs-toggle="tooltip" data-bs-placement="top" title="Click Nama Paket Untuk Lihat Detail Paket"> <a href="{{ url('paket-konsolidasi-detail/'.$item->id) }}" id="">{{ $item->nama_paket_konsolidasi }}</a></li>
                                    @empty
                                    <li class="list-group-item text-center">--Not found data--</li>
                                    @endforelse --}}
                                    </ul>
                                </div>
                                <a class="btn btn-primary btn-sm m-3 shadow"
                                    href="{{ route('get-paket-konsolidasi') }}">Lihat Semua..</a>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5 px-3">
                        <input type="hidden" name="kata_selected" id="kata_selected">
                        {{-- <button type="button" class="btn btn-success">Proses</button> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script>
        let status = 0;
        let katas = [];
        $('[name=select]').click(function() {
            $(':checkbox').prop('checked', this.checked)
        })

        $('#btn_back').click(function(e) {
            e.preventDefault();
            window.history.back();
        });


        $(document).on('click', '#btn_proses', function(e) {
            e.preventDefault()
            if ($('input:checked').length >= 1 && $('#nama_paket').val() !== null) {
                // $('#form_word').attr('action', "{{ route('store-paket-konsolidasi') }}").submit()
                $.ajax({
                    type: "POST",
                    url: "{{ route('store-paket-konsolidasi') }}",
                    data: $('#form_word').serialize(),
                    beforeSend: function() {
                        $('button').attr('disabled', true)
                        $('.loading').attr('hidden', false)
                        $('#spinner').attr('hidden', false)
                    },
                    success: function(res) {
                        console.log(res)
                        $('#form_word').trigger("reset");
                        $('.loading').attr('hidden', true)
                        $('#spinner').attr('hidden', true)
                        $('button').attr('disabled', false)
                        if (res.success == true) {
                            swal(res.message, {
                                icon: 'success'
                            })

                            getLastPaket()
                        } else {
                            swal(res.message, {
                                icon: 'warning'
                            })
                        }
                    },
                    error: function(xhr) {
                        $('.loading').attr('hidden', true)
                        $('#spinner').attr('hidden', true)
                        $('button').attr('disabled', false)
                        swal(xhr.responseJSON.message, {
                            icon: 'warning'
                        })
                    }
                });
            } else {
                $('.loading').attr('hidden', true)
                $('button').attr('disabled', false)
                swal('Minimal Pilih Satu Kata Untuk Diproses', {
                    icon: 'warning'
                })
                return;
            }
        })

        $(document).ready(function() {
            $('[name=kata]').click(function() {
                $(':checkbox').prop('checked', this.checked)
            })

            getLastPaket()
            getKataPopular()


        });

        function getKataPopular() {
            let lembagas = $("input[name='lembaga[]']").map(function() {
                return $(this).val();
            }).get();
            let satker = $("input[name='satker[]']").map(function() {
                return $(this).val();
            }).get();
            let jenis_pengadaan = $("input[name='jenis_pengadaan[]']").map(function() {
                return $(this).val();
            }).get();
            let provinsi = $("input[name='provinsi[]']").map(function() {
                return $(this).val();
            }).get();
            let kota = $("input[name='kota[]']").map(function() {
                return $(this).val();
            }).get();
            $.ajax({
                type: "GET",
                url: "{{ route('kementrian-lembaga.kata-popular-data') }}",
                data: {
                    provinsi: provinsi,
                    tahun_anggaran: "{{ $tahun_anggaran }}",
                    lembaga: lembagas,
                    satker: satker,
                    jenis_pengadaan: jenis_pengadaan,
                    kota: kota,
                    suku_kata: "{{ $suku_kata }}",
                    _token: "{{ csrf_token() }}"
                },
                beforeSend: function() {
                    $('#t_body').append(
                        `<tr>
                        <td colspan="6" class="text-center">
                            <div class="spinner-grow spinner-grow-sm text-secondary">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                            <div class="spinner-grow spinner-grow-sm text-danger">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                            <div class="spinner-grow spinner-grow-sm text-info">
                                <span class="visually-hidden">Loading...</span>
                            </div>
                        </td>
                    </tr>
                            `
                    );
                },
                success: function(res) {
                    console.log(res)
                    $('#t_body').empty();
                    if (res.data.length !== 0) {
                        let arr = res.data.sort((a, b) => {
                            return b.jumlah_kata - a.jumlah_kata;
                        });

                        let no = 1;
                        console.log(arr)
                        $.each(arr, function(i, val) {
                            katas.push(val)

                            $('#t_body').append(
                                `<tr id="word_${i}" class="item kata_popular_${i}">
                                <td><input class="kata" type="checkbox" name="kata[]" value="${val.word}"  ></td>
                                <td>${val.word}</td>
                                <td>
                                    ${val.jumlah_kata}
                                </td>
                                <td>
                                    <div class="" value="${i}" id="btn_paket_${i}">
                                            <div class="spinner-border spinner-border-sm text-secondary" role="status">
                                                <span class="visually-hidden">Loading...</span>
                                            </div>
                                        </div>
                                </td>
                                <td>
                                    <button class="btn btn-outline-danger btn-sm" value="${i}" id="btn_hide" data-bs-toggle="tooltip" data-bs-placement="top" title="Click Untuk Hide Kata Ini"><i class="fas fa-eye-slash"></i> Hide</button>
                                </td>
                            </tr>
                            `
                            );
                        });


                    } else {
                        $('#t_body').append(
                            `<tr>
                            <td colspan="6" class="text-center">Tidak Ada Data</td>
                        </tr>`
                        );
                    }

                    getJumlahPaket()
                },
                error: function() {
                    $('#t_body').empty();
                }
            });
        }

        $(document).ready(function() {

            $('th').each(function(col) {
                $(this).hover(
                    function() {
                        $(this).addClass('focus');
                    },
                    function() {
                        $(this).removeClass('focus');
                    }
                );
                $(this).click(function() {
                    if ($(this).is('.asc')) {
                        $(this).removeClass('asc');
                        $(this).addClass('desc selected');
                        sortOrder = -1;
                    } else {
                        $(this).addClass('asc selected');
                        $(this).removeClass('desc');
                        sortOrder = 1;
                    }
                    $(this).siblings().removeClass('asc selected');
                    $(this).siblings().removeClass('desc selected');
                    var arrData = $('table').find('tbody >tr:has(td)').get();
                    arrData.sort(function(a, b) {
                        var val1 = $(a).children('td').eq(col).text().toUpperCase();
                        var val2 = $(b).children('td').eq(col).text().toUpperCase();
                        if ($.isNumeric(val1) && $.isNumeric(val2))
                            return sortOrder == 1 ? val1 - val2 : val2 - val1;
                        else
                            return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
                    });
                    $.each(arrData, function(index, row) {
                        $('tbody').append(row);
                    });
                });
            });
        });

        $(document).on('click', '#btn_hide', function(e) {
            e.preventDefault();
            let id = $(this).val()
            let html = "#word_" + id
            console.log(id)
            $(html).remove();
        });

        function getJumlahPaket() {
            let lembagas = $("input[name='lembaga[]']").map(function() {
                return $(this).val();
            }).get();
            let satker = $("input[name='satker[]']").map(function() {
                return $(this).val();
            }).get();
            let jenis_pengadaan = $("input[name='jenis_pengadaan[]']").map(function() {
                return $(this).val();
            }).get();
            let provinsi = $("input[name='provinsi[]']").map(function() {
                return $(this).val();
            }).get();
            let kota = $("input[name='kota[]']").map(function() {
                return $(this).val();
            }).get();


            $.each(katas, function(i, val) {
                // console.log(val,'ok')
                $.ajax({
                    type: "GET",
                    url: "{{ route('kementrian-lembaga.get-jumlah-paket') }}",
                    data: {
                        provinsi: provinsi,
                        tahun_anggaran: "{{ $tahun_anggaran }}",
                        lembaga: lembagas,
                        satker: satker,
                        jenis_pengadaan: jenis_pengadaan,
                        kota: kota,
                        word: val.word,
                        suku_kata: "{{ $suku_kata }}",
                        _token: "{{ csrf_token() }}"
                    },
                    dataType: "json",
                    success: function(res) {
                        let jml = '#btn_paket_' + i;
                        //console.log(jml, 'paket')
                        $(jml).text(res.data)

                        //$('#t_jumlah_kata').trigger('click');
                        //console.log('oke')
                    }
                });
                // $.ajax({
                //     type: "GET",
                //     url: "{{ route('kementrian-lembaga.get-jumlah-kata') }}",
                //     data: {
                //         provinsi: provinsi,
                //         tahun_anggaran: "{{ $tahun_anggaran }}",
                //         lembaga: lembagas,
                //         satker: satker,
                //         jenis_pengadaan: jenis_pengadaan,
                //         kota: kota,
                //         word: val.word,
                //         _token: "{{ csrf_token() }}"
                //     },
                //     dataType: "json",
                //     success: function(res) {
                //         let word = '#jumlah_kata_' + i;
                //         let kata_popular = '.kata_popular_' + i;
                //         console.log(res, 'kata')
                //         $(word).text(res.data)

                //         if(res.data === 0){
                //             $(kata_popular).remove()
                //         }
                //         $('#t_jumlah_kata').trigger('click');
                //         console.log('oke')
                //     }
                // });
            });
        }



        function getLastPaket() {
            $.ajax({
                type: "GET",
                url: "{{ route('kementrian-get-last-paket') }}",
                success: function(res) {
                    console.log(res)
                    $('#list_paket').empty();
                    if (res.data.data.length > 0) {
                        $.each(res.data.data, function(i, val) {
                            console.log(val)
                            $('#list_paket').append(
                                `<li id="list" class="list-group-item"> <a id="">${val.nama_paket_konsolidasi}</a><span class="ml-5 badge ${i == 0 ? 'bg-success': '' }">New</span></li>`
                            );

                            $("#list").fadeOut(100).fadeIn(100);
                            $("#list").css({
                                "backgroundColor": "#2B97EF",
                                "color": "white"
                            });

                        });
                    } else {
                        $('#list_paket').append(
                            `<li id="list" class="list-group-item text-center"> <a id="" >-</a></li>`
                        );

                    }

                }
            });
        }
    </script>
@endpush
