@extends('web.layouts.global')
@section('content')
@php
    $no=1
@endphp
<div class="row justify-content-md-center mt-4">
    @if($allowDownload)
        <div class="col-md-auto">
            <a href="javascript:void(0)" id="downloadExcel" class="btn btn-primary">Unduh Rangkuman Hasil</a>
        </div>
    @endif

    @if(!$hidePareto)
        <div class="col-md-auto">
            <a href="{{ route('report.pareto.page', ['instansiId' => $instansiId, 'tahun' => $tahun])}}" target = "_blank"class="btn btn-success">Lihat Pareto</a>
        </div>
    @endif
</div>
<div class="row mb-4" id="report" tag="report">
    <div class="col-12">
        <div class="row mt-10">
            <div class="col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <p style="font-size: 14pt; font-weight: bold; text-align: center">{{ ucwords($namaPaket)}} <br>
                        </p>
                        <div class="table-responsive" style="margin-top: 2rem">
                            <table class="table table-bordered display" style="width:100%" id="tabledata" name="tabledata">
                                <thead class="table-danger">
                                    <tr>
                                        <th >No</th>
                                        <th >ID Sirup</th>
                                        <th >Nama Paket</th>
                                        <th >Satker</th>
                                        <th >Klasifikasi Anggaran</th>
                                        <th >Metode</th>
                                        <th >Jenis</th>
                                        <th >PPK</th>
                                        <th >Pagu Anggaran</th>
                                    </tr>
                                </thead>
                                <tbody id="t_body">
                                    <?php
                                        $total = 0;
                                    ?>
                                    @foreach ($data as $item)
                                        
                                    <?php
                                        $total = $total + $item['pagu_anggaran']
                                    ?>
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$item['id_sirup']}}</td>
                                        <td>{{$item['nama_paket_asli']}}</td>
                                        <td>{{$item['nama_satker']}}</td>
                                        <td>{{$item['klasifikasi_anggaran']  }}</td>
                                        <td>{{$item['metode_pengadaan'] }}</td>
                                        <td>{{$item['jenis_pengadaan'] }}</td>
                                        <td>{{$item['ppk']}}</td>
                                        <td style="text-align: right">{{!$allowDownload ? number_format($item['pagu_anggaran'], 0, '', '.') :$item['pagu_anggaran']}}</td>
                                    </tr>
                                    @endforeach
                                    <tr>
                                        <td colspan="8"> Total </td>
                                        <td style="text-align: right"> {{ !$allowDownload ? number_format($total, 0, '', '.') : $total}}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                
    
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2pdf.js/0.10.1/html2pdf.bundle.min.js" integrity="sha512-GsLlZN/3F2ErC5ifS5QtgpiJtWd43JWSuIgh7mbzZ8zBps+dvLusV+eNQATqgA/HdeKFVgA5v3S/cIrLF7QnIg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.18.5/xlsx.full.min.js"></script>
    <script>
         $(document).ready(function(){
            let str = "{{ ucwords($paket->nama_paket_konsolidasi)}}";

            $('#downloadPdf').click(function(event) {
       
//                 var element = document.getElementById('report');
// html2pdf(element);

            var divContents = $("#report").html();
            var printWindow = window.open('', '', 'height=400,width=800');
            // printWindow.document.write('<html><head><title>DIV Contents</title>');
            // printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            // printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
                // New Promise-based usage:
                //html2pdf().set(opt).from(element).save();

                //window.jsPDF = window.jspdf.jsPDF;
                //window.html2canvas = html2canvas

                // html2canvas(document.querySelector('#report')).
                //     then(canvas => {
                //         const img = canvas.toDataURL('image/png')

                //         var pdf = new jsPDF("p","mm","Legal");
                //         pdf.addImage(img, 'PNG', 6,2,204,356)
                //         pdf.save(str + '.pdf');
                //     })
                // // get size of report page
                // var reportPageHeight = $('#reportPage').innerHeight();
                // var reportPageWidth = $('#reportPage').innerWidth();
            
            });

            function exportReportToExcel() {
                
            }

            $('#downloadExcel').click(function(event) {
                HtmlTOExcel('xlsx')
            });

            function HtmlTOExcel(type, fun, dl) {
                var elt = document.getElementById('tabledata');
                var wb = XLSX.utils.table_to_book(elt, { sheet: "sheet1" });
                return dl ?
                    XLSX.write(wb, { bookType: type, bookSST: true, type: 'base64' }) :
                    XLSX.writeFile(wb, fun || ('{{ $paket->nama_paket_konsolidasi }}' + '.' + (type || 'xlsx')));
            }


        });

        
      
    </script>
@endpush