@extends('web.layouts.global')
@section('content')
<div class="row my-10">
    <div class="col-12 container">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Kategori Berdasarkan Cara Pengadaan</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="myChart1" height="200"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Kategori Berdasarkan Cara Pengadaan</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <th>Jenis Pengadaan</th>
                                <th>Pagu</th>
                                <th>Jumlah Paket</th>
                            </thead>
                            <tbody>
                                @foreach ($e as $item) 
                                    <tr>
                                        <td>{{$item['tipe_paket']}}</td>
                                        <td>{{"Rp " . number_format($item['pagu'],0,'','.')}}</td>
                                        <td>{{ $item['paket'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row my-10">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Kategori Berdasarkan Jenis Pengadaan</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="myChart" height="200"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Kategori Berdasarkan Jenis Pengadaan</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <th>Jenis Pengadaan</th>
                                <th>Pagu</th>
                                <th>Jumlah Paket</th>
                            </thead>
                            <tbody>
                                @foreach ($a as $item) 
                                    <tr>
                                        <td>{{$item['jenis_pengadaan']}}</td>
                                        <td>{{"Rp " . number_format($item['pagu'],0,'','.')}}</td>
                                        <td>{{ $item['jumlah_paket'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="row"">
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Kategori Berdasarkan Metode Pengadaan</h5>
                    </div>
                    <div class="card-body">
                        <canvas id="myChart2" height="200"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card">
                    <div class="card-header">
                        <h5>Kategori Berdasarkan Metode Pengadaan</h5>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <thead>
                                <th>Metode Pengadaan</th>
                                <th>Jumlah Pagu</th>
                                <th>Jumlah Paket</th>
                            </thead>
                            <tbody>
                                @foreach ($mp as $item) 
                                    <tr>
                                        <td>{{$item['metode_pengadaan']}}</td>
                                        <td>{{"Rp " . number_format($item['pagu'],0,'','.')}}</td>
                                        <td>{{ $item['jumlah_paket'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
<script>
    const ctx = document.getElementById('myChart');
    const ctx1 = document.getElementById('myChart1');
    const ctx2 = document.getElementById('myChart2');
    const myChart = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: [
                @foreach($a as $b)
                '{{ $b["jenis_pengadaan"] }}',
                @endforeach
            ],
            datasets: [{
                label: '# of Votes',
                data: [
                    @foreach($a as $b)
                    '{{ $b["pagu"] }}',
                    @endforeach
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(175, 192, 192, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(175, 192, 192, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    const myChart1 = new Chart(ctx1, {
        type: 'doughnut',
        data: {
            labels: [
                @foreach($e as $b)
                '{{ $b["tipe_paket"] }}',
                @endforeach
            ],
            datasets: [{
                label: '# of Votes',
                data: [
                    @foreach($e as $b)
                    '{{ $b["pagu"] }}',
                    @endforeach
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(175, 192, 192, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(175, 192, 192, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });


    const myChart2 = new Chart(ctx2, {
        type: 'doughnut',
        data: {
            labels: [
                @foreach($mp as $metode)
                '{{ $metode["metode_pengadaan"] }}',
                @endforeach
            ],
            datasets: [{
                label: '# of Votes',
                data: [
                    @foreach($mp as $metode)
                    '{{ $metode["pagu"] }}',
                    @endforeach
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(175, 192, 192, 0.2)',
                    'rgba(45, 30, 192, 0.2)',
                    'rgba(145, 230, 100, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(175, 192, 192, 1)',
                    'rgba(45, 30, 192, 1)',
                    'rgba(145, 230, 100, 1)',
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
  </script>
@endpush