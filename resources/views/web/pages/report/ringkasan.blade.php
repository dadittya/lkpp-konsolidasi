@extends('web.layouts.global')
@section('content')
@php
    $no=1
@endphp
<div class="row mb-4" id="report">
    <div class="col-12">
        <div class="row mt-10">
            <div class="col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <p style="font-size: 14pt; font-weight: bold; text-align: center">Gambaran Umum Pengadaan <br>
                            {{-- @foreach ($res as $item)
                                {{$item['klpd']}}
                            @endforeach --}}
                            {{-- @if ($satker != '')
                                <br>
                                {{ $satker }}
                            @endif --}}

                            {{ $klpdStr}} <br>
                            Tahun Anggaran {{$uip->tahun}}</p>
                            <p style="font-size: 11pt;">
                                @foreach ($res as $item)
                                    {{ "* Diupdate terakhir pada " . $item['klpd_update']}}
                                @endforeach 
                            </p>
                        <div class="table-responsive" style="margin-top: 2rem">
                            <table class="table table-bordered display" style="width:100%">
                                <thead class="table-danger">
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2"><center>K/L/PD</center></th>
                                        <th colspan="2"><center>Penyedia</center></th>
                                        <th colspan="2"><center>Swakelola</center></th>
                                        <th colspan="2"><center>Penyedia dalam Swakelola</center></th>
                                        <th colspan="2"><center>Total</center></th>
                                    </tr>
                                    <tr>
                                        <th>Pkt</th>
                                        <th>Pagu</th>
                                        <th>Keg</th>
                                        <th>Pagu</th>
                                        <th>Pkt</th>
                                        <th>Pagu</th>
                                        <th>Keg + Pkt</th>
                                        <th>Total Pagu</th>
                                    </tr>
                                </thead>
                                <tbody id="t_body">
                                    @foreach ($res as $item)
                                        
                                    <tr>
                                        <td>{{$no++}}</td>
                                        <td>{{$item['klpd']}}</td>
                                        <td>{{ number_format($item['pm']['paket'], 0, ',', '.') }}</td>
                                        <td>{{ "Rp ". number_format($item['pm']['pagu'],0,'','.') }}</td>
                                        <td>{{ number_format($item['sw']['paket'], 0, ',', '.') }}</td>
                                        <td>{{ "Rp ". number_format($item['sw']['pagu'],0,'','.') }}</td>
                                        <td>{{ number_format($item['pds']['paket'], 0, ',', '.') }}</td>
                                        <td>{{ "Rp ". number_format($item['pds']['pagu'],0,'','.') }}</td>
                                        <td>
                                            {{ number_format($item['pm']['paket'] + $item['sw']['paket'] + $item['pds']['paket'], 0, ',', '.') }}
                                        </td>
                                        <td>
                                            @php
                                                $tp = $item['pm']['pagu'] + $item['sw']['pagu'] + $item['pds']['pagu'];
                                            @endphp
                                            {{ "Rp ". number_format($tp,0,'','.') }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
    
            </div>
        </div>
        
        {{-- Chart --}}
        <div class="row" style="margin-top: 3rem;" id="chart">
            <div class="col-12">
                <div class="row">
                    <p style="font-size: 16pt; font-weight: bolder; margin-bottom: 2rem; text-decoration: underline;">Kategori Berdasarkan Cara Pengadaan</p>
                    <div class="col-5" style="width:450px">
                        <canvas id="myChart1" height="200"></canvas>
                    </div>
                    <div class="col-7">
                        <div class="table-responsive">
                            <table id="table_cara" class="table table-stripped table-success border border-light" style="width: 100%">
                                <thead>
                                    <th>Cara Pengadaan</th>
                                    <th class="text-right">Pagu</th>
                                    <th class="text-right">Jumlah Paket</th>
                                </thead>
                                <tbody>
                                    @foreach ($e['items'] as $item)
                                        <tr>
                                            <td>{{$item['tipe_paket']}}</td>
                                            <td class="text-right">{{"Rp " . number_format($item['total_pagu'],0,'','.')}}</td>
                                            <td class="text-right">{{ number_format($item['jumlah_paket'], 0, '', '.') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class="font-weight: bolder;">Total</td>
                                        <td class="text-right">{{"Rp ". number_format($e['total']['total_pagu'],0,'','.') }}</td>
                                        <td class="text-right">{{ number_format($e['total']['jumlah_paket'], 0, '', '.') }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
        
                <div class="row" style="margin-top: 3rem;">
                    <p style="font-size: 16pt; font-weight: bolder; margin-bottom: 2rem; text-decoration: underline;">Kategori Berdasarkan Jenis Pengadaan</p>
                    <div class="col-5" style="width:450px">
                        <canvas id="myChart" height="200"></canvas>
                    </div>
                    <div class="col-7">
                        <div class="table-responsive">
                            <table id="table_jenis" class="table table-stripped table-success border border-light">
                                <thead>
                                    <th>Jenis Pengadaan</th>
                                    <th class="text-right">Pagu</th>
                                    <th class="text-right">Jumlah Paket</th>
                                </thead>
                                <tbody>
                                    @foreach ($a['items'] as $item) 
                                        <tr>
                                            <td>{{$item['jenis_pengadaan']}}</td>
                                            <td class="text-right">{{"Rp " . number_format($item['pagu'],0,'','.')}}</td>
                                            <td class="text-right">{{ number_format($item['jumlah_paket'], 0, ',', '.') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td style="font-weight: bolder;">Total</td>
                                        <td class="text-right" style="font-weight: bolder;">{{ "Rp ". number_format($a['total']['pagu'],0,',','.') }}</td>
                                        <td class="text-right" style="font-weight: bolder;">{{ number_format($a['total']['jumlah_paket'], 0, ',', '.') }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
        
                <div class="row" style="margin-top: 3rem;">
                    <p style="font-size: 16pt; font-weight: bolder; margin-bottom: 2rem; text-decoration: underline;">Kategori Berdasarkan Metode Pengadaan</p>
                    <div class="col-5" style="width:450px">
                        <canvas id="myChart2" height="200"></canvas>
                    </div>
                    <div class="col-7">
                        <div class="table-responsive">
                            <table id="table_metode" class="table table-stripped table-success border border-light">
                                <thead>
                                    <th>Metode Pengadaan</th>
                                    <th  class="text-right">Jumlah Pagu</th>
                                    <th class="text-right">Jumlah Paket</th>
                                </thead>
                                <tbody>
                                    @foreach ($mp['items'] as $item) 
                                        <tr>
                                            <td>{{$item['metode_pengadaan']}}</td>
                                            <td  class="text-right">{{"Rp " . number_format($item['total_pagu'],0,'','.')}}</td>
                                            <td class="text-right">{{ number_format($item['jumlah_paket'], 0, ',', '.') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td style="font-weight: bolder;">Total</td>
                                        <td  class="text-right" style="font-weight: bolder;">{{ "Rp ". number_format($mp['total']['total_pagu'], 0, ',', '.') }}</td>
                                        <td class="text-right" style="font-weight: bolder;">{{ number_format($mp['total']['jumlah_paket'], 0, ',', '.') }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="table-responsive" style="margin-top: 2rem">
                <p style="font-size: 14pt; font-weight: bold; text-align: center">Potensi Konsolidasi <br>
                    {{-- @foreach ($res as $item)
                        {{$item['klpd']}}
                    @endforeach
                    @if($dtSatker )
                        Satuan Kerja 

                        @foreach ($dtSatker as $satker)
                            {{ $satker->nama}}
                        @endforeach
                    @endif
                    

                    @if($dtJenisPengadaan)
                        Dengan Jenis Pengadaan

                        @foreach ($dtJenisPengadaan as $jenis)
                            {{ $jenis->name }}
                        @endforeach
                    @endif --}}
                    {{ $klpdStr}} <br>
                    {!! $satkerStr ? 'Satuan Kerja ' . $satkerStr . '<br' : ''!!} 
                    {{-- {!! $jenisPengadaanStr ? 'Jenis Pengadaan ' . $jenisPengadaanStr . '<br>' : ''!!} --}}
                    Tahun Anggaran {{ $tahunStr}}</p><br>
                    
                <table class="table table-bordered display" style="width:100%" id="tabledata" name="tabledata">
                    <thead class="table-danger">
                        <tr>
                            <th style="width: 2%">No</th>
                            <th >Nama Paket Konsolidasi</th>
                            <th  class="text-right" style="width: 10%">Jumlah Paket</th>
                            <th  class="text-right" style="width: 25%">Pagu</th>
                        </tr>
                    </thead>
                    <tbody id="t_body">
                        @php
                            $index = 1;
                        @endphp
                        @foreach ($paketKonsolidasi as $item)
                       
                        <tr>
                            <td>{{$index++}}</td>
                            <td>{{ucwords($item['paket'])}}</td>
                            <td  class="text-right" >{{number_format($item['jumlah_paket'],0,'','.')}}</td>
                            <td class="text-right">{{number_format($item['pagu'],0,'','.')}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="py-5">
                <div class="card ">
                    <div class="card-body">
                        <h5 class="fw-bold text-lg d-inline">Supply Positioning Model (SPM) :</h5>
                        {{-- <a href="{{ route('get-paket-konsolidasi') }}" class="float-end btn btn-outline-primary">Tambah Kelompok <i class="fas fa-folder-plus"></i></a> --}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mt-5">
                        <div class="card rounded-3 shadow">
                            <div class="card-header">
                                <h5 class="fw-bolder">BOTTLENECK</h5>
                            </div>
        
                            <div class="card-body bg-warning">
                                <div class="p-5">
                                    <table class="table table-striped table-warning table-hover" id="table_1">
                                        <thead>
                                            {{-- <tr>
                                                <th>Nama Paket</th>
                                            </tr> --}}
                                        </thead>
                                        <tbody class="tbody_1">
                                            {{-- <tr>
                                                <td>Paket Konsolidasi 1</td>
                                            </tr> --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mt-5">
                        <div class="card rounded-3 shadow">
                            <div class="card-header">
                                <h5 class="fw-bolder">CRITICAL</h5>
                            </div>
        
                            <div class="card-body bg-danger">
                                <div class="p-5">
                                    <table class="table table-striped table-hover table-danger" id="table_2">
                                        <thead>
                                            {{-- <tr>
                                                <th>Nama Paket</th>
                                            </tr> --}}
                                        </thead>
                                        <tbody class="tbody_2">
                                        {{-- <tr>
                                            <td>1</td>
                                            <td>Paket Konsolidasi 1</td>
                                        </tr> --}}
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mt-5">
                        <div class="card rounded-3 shadow">
                            <div class="card-header">
                                <h5 class="fw-bolder">ROUTINE</h5>
                            </div>
        
                            <div class="card-body bg-info">
                                <div class="p-5 table-responsive">
                                    <table class="table table-striped table-hover table-info" id="table_3">
                                        <thead>
                                            {{-- <tr>
                                                <th>Nama Paket</th>
                                            </tr> --}}
                                        </thead>
                                        <tbody class="tbody_3">
                                            {{-- <tr>
                                                <td>Paket Konsolidasi 1</td>
                                            </tr> --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mt-5">
                        <div class="card rounded-3 shadow">
                            <div class="card-header">
                                <h5 class="fw-bolder">LEVERAGE</h5>
                            </div>
        
                            <div class="card-body bg-success">
                                <div class="p-5">
                                    <table class="table table-striped table-hover table-success" id="table_4">
                                        <thead>
                                            {{-- <tr>
                                                <th>Nama Paket</th>
                                            </tr> --}}
                                        </thead>
                                        <tbody class="tbody_4">
                                        {{-- <tr>
                                            <td>1</td>
                                            <td>Paket Konsolidasi 1</td>
                                        </tr> --}}
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if (!empty($data_1) || !empty($data_2))
                    <div class="card" class="mt-5" style="margin-top: 7rem;">
                        <div class="card-header">
                            <h3 class="fw-bold">Teori Tentang SPM & SPM</h3>
                        </div>
                        <div class="card-body p-5">
                            <div class="row" style="margin-top: 1rem;">
                                <div class="card rounded-3 p-2">
                                    <div class="card-body">
                                        <h2 class="fw-bolder">Supply Position Model</h2>
                                        @foreach ($data_1 as $item)
                                            <div class="mt-4">
                                                <h4 style="font-weight: 600">{{ $item->title }}</h4>
                                                <p class="mt-2">{!! $item->konten !!}</p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5rem;">
                                <div class="card rounded-3 p-2">
                                    <div class="card-body">
                                        <h2 class="fw-bolder">Supplier Perception Model</h2>
                                        @foreach ($data_2 as $item)
                                            <div class="mt-4">
                                                <h4 style="font-weight: 600">{{ $item->title }}</h4>
                                                <p class="mt-2">{!! $item->konten !!}</p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div> 
        
            <div class="row justify-content-md-center mt-4">
                <div class="col-md-auto">
                    <a href="javascript:void(0)" id="downloadPdf" class="btn btn-primary" data-html2canvas-ignore>Unduh Rangkuman Hasil</a>
                    {{-- <a href="{{ route('report.generate') }}" class="btn btn-primary">Unduh Rangkuman Hasil</a> --}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
    <script>
        $(document).ready(function(){

            let kl = "{{ $res[0]['klpd'] }}"
            
            let str = kl.replace(/\s/g, "_")
            const ctx = document.getElementById('myChart');
            const ctx1 = document.getElementById('myChart1');
            const ctx2 = document.getElementById('myChart2');
            const myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: [
                        @foreach($a['items'] as $b)
                        '{{ $b["jenis_pengadaan"] }}',
                        @endforeach
                    ],
                    datasets: [{
                        label: '# of Votes',
                        data: [
                            @foreach($a['items'] as $b)
                            '{{ $b["pagu"] }}',
                            @endforeach
                        ],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(175, 192, 192, 0.2)',
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(175, 192, 192, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                }
            });

            const myChart1 = new Chart(ctx1, {
                type: 'pie',
                data: {
                    labels: [
                        @foreach($e['items'] as $b)
                        '{{ $b["tipe_paket"] }}',
                        @endforeach
                    ],
                    datasets: [{
                        label: '# of Votes',
                        data: [
                            @foreach($e['items'] as $b)
                            '{{ $b["total_pagu"] }}',
                            @endforeach
                        ],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(175, 192, 192, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(175, 192, 192, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                }
            });


            const myChart2 = new Chart(ctx2, {
                type: 'pie',
                data: {
                    labels: [
                        @foreach($mp['items'] as $metode)
                        '{{ $metode["metode_pengadaan"] }}',
                        @endforeach
                    ],
                    datasets: [{
                        label: '# of Votes',
                        data: [
                            @foreach($mp['items'] as $metode)
                            '{{ $metode["total_pagu"] }}',
                            @endforeach
                        ],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(175, 192, 192, 0.2)',
                            'rgba(45, 30, 192, 0.2)',
                            'rgba(145, 230, 100, 0.2)',
                            'rgba(345, 18, 202, 0.3)',
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(175, 192, 192, 1)',
                            'rgba(45, 30, 192, 1)',
                            'rgba(145, 230, 100, 1)',
                            'rgba(345, 18, 202, 1)',
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                }
            });

            $('#downloadPdf').click(function(event) {
                window.jsPDF = window.jspdf.jsPDF;
                window.html2canvas = html2canvas

                html2canvas(document.querySelector('#report')).
                    then(canvas => {
                        const img = canvas.toDataURL('image/png')

                        //var pdf = new jsPDF("p","mm","legal");
                        var pdf = new jsPDF("p","mm","legal", false, true);
                        pdf.addImage(img, 'PNG', 6,2,204,356)
                        pdf.save(str + '.pdf');
                    })
                // get size of report page
                var reportPageHeight = $('#reportPage').innerHeight();
                var reportPageWidth = $('#reportPage').innerWidth();
            
            });
            
                
        });

        $(document).ready(function(){
                getBottleneck()
                getCritical()
                getRoutine()
                getLeverage()
            })

            const getBottleneck = () => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('kelompok-konsolidasi-kecil-tinggi') }}",
                    success: function (res) {
                        console.log(res)
                        $.each(res, function (i, val) {
                            $('.tbody_1').append(
                                `<tr>
                                    <td>
                                        ${val.nama_paket_konsolidasi}<a href="{{ url('paket-konsolidasi-detail-report/kecil/${val.id}/') }}" class="" id="" target="_blank"> (Unduh)</a>
                                    </td>
                                </tr>`
                            );
                        });
                    }
                });
            }
            const getCritical = () => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('kelompok-konsolidasi-besar-tinggi') }}",
                    success: function (res) {
                        console.log(res)
                        $.each(res, function (i, val) {
                            $('.tbody_2').append(
                                `<tr>
                                    <td>
                                        ${val.nama_paket_konsolidasi}<a href="{{ url('paket-konsolidasi-detail-report/besar/${val.id}') }}" class="" id="" target="_blank"> (Unduh)</a>
                                    </td>
                                </tr>`
                            );
                        });
                    }
                });
            }
            const getRoutine = () => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('kelompok-konsolidasi-kecil-rendah') }}",
                    success: function (res) {
                        console.log(res)
                        $.each(res, function (i, val) {
                            $('.tbody_3').append(
                                `<tr>
                                    <td>
                                        ${val.nama_paket_konsolidasi}<a href="{{ url('paket-konsolidasi-detail-report/kecil/${val.id}') }}" class="" id="" target="_blank"> (Unduh)</a>
                                    </td>
                                </tr>`
                            );
                        });
                    }
                });
            }
            const getLeverage = () => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('kelompok-konsolidasi-besar-rendah') }}",
                    success: function (res) {
                        console.log(res)
                        $.each(res, function (i, val) {
                            $('.tbody_4').append(
                                `<tr>
                                    <td>
                                        ${val.nama_paket_konsolidasi}<a href="{{ url('paket-konsolidasi-detail-report/besar/${val.id}') }}" class="" id="" target="_blank"> (Unduh)</a>
                                    </td>
                                </tr>`
                            );
                        });
                    }
                });
            }


    </script>
@endpush