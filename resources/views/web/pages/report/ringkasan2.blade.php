@extends('web.layouts.global')
@section('content')
@php
    $no=1
@endphp
<div class="row mb-4" id="report">
    <div class="col-12">
        <div class="row mt-10">
            <div class="col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <p style="font-size: 14pt; font-weight: bold; text-align: center">Potensi Konsolidasi <br>
                            @foreach ($res as $item)   
                                {{$item['klpd']}}
                            @endforeach
                            Tahun Anggaran {{$uip->tahun}}</p>
                        <div class="table-responsive" style="margin-top: 2rem">
                            <table class="table table-bordered display" style="width:100%">
                                <thead class="table-danger">
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2"><center>K/L/PD</center></th>
                                        <th colspan="2"><center>Penyedia</center></th>
                                        <th colspan="2"><center>Swakelola</center></th>
                                        <th colspan="2"><center>Penyedia dalam Swakelola</center></th>
                                        <th colspan="2"><center>Total</center></th>
                                    </tr>
                                    <tr>
                                        <th>Pkt</th>
                                        <th>Pagu</th>
                                        <th>Keg</th>
                                        <th>Pagu</th>
                                        <th>Pkt</th>
                                        <th>Pagu</th>
                                        <th>Keg + Pkt</th>
                                        <th>Total Pagu</th>
                                    </tr>
                                </thead>
                                <tbody id="t_body">
                                    @foreach ($res as $item)
                                        <tr>
                                            <td>{{$no++}}</td>
                                            <td>{{$item['klpd']}}</td>
                                            <td>{{ number_format($item['paket']) }}</td>
                                            <td>{{ "Rp ". number_format($item['pagu'],0,'','.') }}</td>
                                            <td>{{ number_format($item['paket']) }}</td>
                                            <td>{{ "Rp ". number_format($item['pagu'],0,'','.') }}</td>
                                            <td>{{ number_format($item['paket']) }}</td>
                                            <td>{{ "Rp ". number_format($item['pagu'],0,'','.') }}</td>
                                            <td>
                                                {{ $item['paket'] + $item['paket'] + $item['paket'] }}
                                            </td>
                                            <td>
                                                @php
                                                    $tp = $item['pm']['pagu'] + $item['sw']['pagu'] + $item['pds']['pagu'];
                                                @endphp
                                                {{ "Rp ". number_format($tp,0,'','.') }}
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
    
            </div>
        </div>
        
        <div class="row" style="margin-top: 3rem;" id="chart">
            <div class="col-12">
                <div class="row">
                    <p style="font-size: 16pt; font-weight: bolder; margin-bottom: 2rem; text-decoration: underline;">Kategori Berdasarkan Cara Pengadaan</p>
                    <div class="col-5">
                        <canvas id="myChart1" height="200"></canvas>
                    </div>
                    <div class="col-7">
                        <div class="table-responsive">
                            <table id="table_cara" class="table table-bordered" style="width: 100%">
                                <thead>
                                    <th>Cara Pengadaan</th>
                                    <th>Pagu</th>
                                    <th class="text-right">Jumlah Paket</th>
                                </thead>
                                <tbody>
                                    {{-- <tr>
                                        <td>{{$item['pm']['tipe']}}</td>
                                        <td>{{"Rp " . number_format($item['pm']['pagu'],0,'','.')}}</td>
                                        <td class="text-right">{{ number_format($item['pm']['paket']) }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{$item['sw']['tipe']}}</td>
                                        <td>{{"Rp " . number_format($item['sw']['pagu'],0,'','.')}}</td>
                                        <td class="text-right">{{ number_format($item['sw']['paket']) }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{$item['pds']['tipe']}}</td>
                                        <td>{{"Rp " . number_format($item['pds']['pagu'],0,'','.')}}</td>
                                        <td class="text-right">{{ number_format($item['pds']['paket']) }}</td>
                                    </tr> --}}
                                </tbody>
                                <tfoot>
                                    {{-- <tr>
                                        <td class="font-weight: bolder;">Total</td>
                                        <td>{{"Rp ". number_format($e['total']['total_pagu'],0,'','.') }}</td>
                                        <td class="text-right">{{ number_format($e['total']['jumlah_paket']) }}</td>
                                    </tr> --}}
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="row" style="margin-top: 4rem">
                <div class="col-12">
                    <div class="table-responsive">
                        <table class="table table-stripped table-bordered">
                            <tbody>
                                {{-- @foreach ($paketKonsolidasi as $item) 
                                    <tr>
                                        <td>{{$item->nama_paket_konsolidasi}}</td>
                                    </tr>
                                @endforeach --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row justify-content-md-center mt-4">
    <div class="col-md-auto">
        <a href="javascript:void(0)" id="downloadPdf" class="btn btn-primary" data-html2canvas-ignore>Unduh Rangkuman Hasil</a>
        {{-- <a href="{{ route('report.generate') }}" class="btn btn-primary">Unduh Rangkuman Hasil</a> --}}
    </div>
</div>

@endsection

@push('script')
    <script>
        $(document).ready(function(){

            let data = []
            $.ajax({
                url: "{{ route('report.ringkasan.data') }}",
                success: function(d){
                    data.push(d)
                    console.log(data)
                }
            })


            const ctx1 = document.getElementById('myChart1'); 
            const myChart = new Chart(ctx1, {
                type: 'pie',
                data: {
                    labels: ['Penyedia','Swakelola','Penyedia dalam Swakelola'],
                    datasets: [{
                        label: '# of Votes',
                        data: [
                            
                        ],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(175, 192, 192, 0.2)',
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(175, 192, 192, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                }
            });
        });


    </script>
@endpush