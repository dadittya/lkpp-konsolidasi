@extends('web.layouts.global')
@section('content')
<div class="row" id="report">
    <div class="col-12">
        <div class="row mt-10">
            <div class="col-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        @php
                            $kl = [];
                            // if (count($klpd) == 1) {
                            //     $kl = $klpd[0];
                            // }
                            // else {
                            //     $kl = $klpd;
                            // }
                        @endphp
                        @foreach ($klpd as $item)
                            $kl = $item
                        @endforeach
                        <p style="font-size: 14pt; font-weight: bold; text-align: center">Potensi Konsolidasi 
                            <br>
                            {{ ($satker == '') ? '' : 'Satker '.$satker  }} Tahun Anggaran {{$uip->tahun}}</p>
                        <div class="table-responsive" style="margin-top: 2rem">
                            <table id="table_123" class="table table-bordered display" style="width:100%">
                                <thead class="table-danger">
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th rowspan="2"><center>K/L/PD</center></th>
                                        <th colspan="2"><center>Penyedia</center></th>
                                        <th colspan="2"><center>Swakelola</center></th>
                                        <th colspan="2"><center>Penyedia dalam Swakelola</center></th>
                                        <th colspan="2"><center>Total</center></th>
                                    </tr>
                                    <tr>
                                        <th>Pkt</th>
                                        <th>Pagu</th>
                                        <th>Keg</th>
                                        <th>Pagu</th>
                                        <th>Pkt</th>
                                        <th>Pagu</th>
                                        <th>Keg + Pkt</th>
                                        <th>Total Pagu</th>
                                    </tr>
                                </thead>
                                <tbody id="t_body">
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
        
                {{-- <div class="row" style="margin-top: 4em;">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <h3>Satuan Kerja dengan Anggaran Pengadaan Terbesar</h3>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table id="table_3" class="table table-bordered display" style="width:100%">
                                        <thead class="table-danger">
                                            <tr>
                                                <th rowspan="2"><center>No</center></th>
                                                <th rowspan="2"><center>Satuan Kerja</center></th>
                                                <th colspan="2"><center>Penyedia</center></th>
                                                <th colspan="2"><center>Swakelola</center></th>
                                                <th colspan="2"><center>Penyedia dalam Swakelola</center></th>
                                                <th colspan="2"><center>Total</center></th>
                                            </tr>
                                            <tr>
                                                <th>Paket</th>
                                                <th>Pagu</th>
                                                <th>Paket</th>
                                                <th>Pagu</th>
                                                <th>Paket</th>
                                                <th>Pagu</th>
                                                <th>Total Paket</th>
                                                <th>Total Pagu</th>
                                            </tr>
                                        </thead>
                                        <tbody id="t_body">
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
        
        
        {{-- Chart --}}
        <div class="row" style="margin-top: 3rem;" id="chart">
            <div class="col-12">
                <div class="row">
                    <p style="font-size: 16pt; font-weight: bolder; margin-bottom: 2rem; text-decoration: underline;">Kategori Berdasarkan Cara Pengadaan</p>
                    <div class="col-5">
                        <canvas id="myChart1" height="200"></canvas>
                    </div>
                    <div class="col-7">
                        <div class="table-responsive">
                            <table id="table_cara" class="table table-bordered" style="width: 100%">
                                <thead>
                                    <th>Cara Pengadaan</th>
                                    <th>Pagu</th>
                                    <th class="text-right">Jumlah Paket</th>
                                </thead>
                                <tbody>
                                    @foreach ($e['items'] as $item)
                                        <tr>
                                            <td>{{$item['tipe_paket']}}</td>
                                            <td>{{"Rp " . number_format($item['total_pagu'],0,'','.')}}</td>
                                            <td class="text-right">{{ number_format($item['jumlah_paket']) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td class="font-weight: bolder;">Total</td>
                                        <td>{{"Rp ". number_format($e['total']['total_pagu'],0,'','.') }}</td>
                                        <td class="text-right">{{ number_format($e['total']['jumlah_paket']) }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
        
                <div class="row" style="margin-top: 3rem;">
                    <p style="font-size: 16pt; font-weight: bolder; margin-bottom: 2rem; text-decoration: underline;">Kategori Berdasarkan Jenis Pengadaan</p>
                    <div class="col-5">
                        <canvas id="myChart" height="200"></canvas>
                    </div>
                    <div class="col-7">
                        <div class="table-responsive">
                            <table id="table_jenis" class="table table-stripped table-success border border-light">
                                <thead>
                                    <th>Jenis Pengadaan</th>
                                    <th>Pagu</th>
                                    <th class="text-right">Jumlah Paket</th>
                                </thead>
                                <tbody>
                                    @foreach ($a['items'] as $item) 
                                        <tr>
                                            <td>{{$item['jenis_pengadaan']}}</td>
                                            <td>{{"Rp " . number_format($item['pagu'],0,'','.')}}</td>
                                            <td class="text-right">{{ number_format($item['jumlah_paket']) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td style="font-weight: bolder;">Total</td>
                                        <td style="font-weight: bolder;">{{ "Rp ". number_format($a['total']['pagu'],0,',','.') }}</td>
                                        <td class="text-right" style="font-weight: bolder;">{{ number_format($a['total']['jumlah_paket']) }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
        
                <div class="row" style="margin-top: 3rem;">
                    <p style="font-size: 16pt; font-weight: bolder; margin-bottom: 2rem; text-decoration: underline;">Kategori Berdasarkan Metode Pengadaan</p>
                    <div class="col-5">
                        <canvas id="myChart2" height="200"></canvas>
                    </div>
                    <div class="col-7">
                        <div class="table-responsive">
                            <table id="table_metode" class="table table-stripped table-success border border-light">
                                <thead>
                                    <th>Metode Pengadaan</th>
                                    <th>Jumlah Pagu</th>
                                    <th class="text-right">Jumlah Paket</th>
                                </thead>
                                <tbody>
                                    @foreach ($mp['items'] as $item) 
                                        <tr>
                                            <td>{{$item['metode_pengadaan']}}</td>
                                            <td>{{"Rp " . number_format($item['total_pagu'],0,'','.')}}</td>
                                            <td class="text-right">{{ number_format($item['jumlah_paket']) }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td style="font-weight: bolder;">Total</td>
                                        <td style="font-weight: bolder;">{{ "Rp ". number_format($mp['total']['total_pagu']) }}</td>
                                        <td class="text-right" style="font-weight: bolder;">{{ number_format($mp['total']['jumlah_paket']) }}</td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        
            <div class="py-5">
                <div class="card ">
                    <div class="card-body">
                        <h5 class="fw-bold text-lg d-inline">Supply Positioning Model (SPM) :</h5>
                        {{-- <a href="{{ route('get-paket-konsolidasi') }}" class="float-end btn btn-outline-primary">Tambah Kelompok <i class="fas fa-folder-plus"></i></a> --}}
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mt-5">
                        <div class="card rounded-3 shadow">
                            <div class="card-header">
                                <h5 class="fw-bolder">BOTTLENECK</h5>
                            </div>
        
                            <div class="card-body bg-warning">
                                <div class="p-5">
                                    <table class="table table-striped table-warning table-hover" id="table_1">
                                        <thead>
                                            {{-- <tr>
                                                <th>Nama Paket</th>
                                            </tr> --}}
                                        </thead>
                                        <tbody class="tbody_1">
                                            {{-- <tr>
                                                <td>Paket Konsolidasi 1</td>
                                            </tr> --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mt-5">
                        <div class="card rounded-3 shadow">
                            <div class="card-header">
                                <h5 class="fw-bolder">CRITICAL</h5>
                            </div>
        
                            <div class="card-body bg-danger">
                                <div class="p-5">
                                    <table class="table table-striped table-hover table-danger" id="table_2">
                                        <thead>
                                            {{-- <tr>
                                                <th>Nama Paket</th>
                                            </tr> --}}
                                        </thead>
                                        <tbody class="tbody_2">
                                        {{-- <tr>
                                            <td>1</td>
                                            <td>Paket Konsolidasi 1</td>
                                        </tr> --}}
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mt-5">
                        <div class="card rounded-3 shadow">
                            <div class="card-header">
                                <h5 class="fw-bolder">ROUTINE</h5>
                            </div>
        
                            <div class="card-body bg-info">
                                <div class="p-5 table-responsive">
                                    <table class="table table-striped table-hover table-info" id="table_3">
                                        <thead>
                                            {{-- <tr>
                                                <th>Nama Paket</th>
                                            </tr> --}}
                                        </thead>
                                        <tbody class="tbody_3">
                                            {{-- <tr>
                                                <td>Paket Konsolidasi 1</td>
                                            </tr> --}}
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 mt-5">
                        <div class="card rounded-3 shadow">
                            <div class="card-header">
                                <h5 class="fw-bolder">LEVERAGE</h5>
                            </div>
        
                            <div class="card-body bg-success">
                                <div class="p-5">
                                    <table class="table table-striped table-hover table-success" id="table_4">
                                        <thead>
                                            {{-- <tr>
                                                <th>Nama Paket</th>
                                            </tr> --}}
                                        </thead>
                                        <tbody class="tbody_4">
                                        {{-- <tr>
                                            <td>1</td>
                                            <td>Paket Konsolidasi 1</td>
                                        </tr> --}}
                                    </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if (!empty($data_1) || !empty($data_2))
                    <div class="card" class="mt-5" style="margin-top: 7rem;">
                        <div class="card-header">
                            <h3 class="fw-bold">Teori Tentang SPM & SPM</h3>
                        </div>
                        <div class="card-body p-5">
                            <div class="row" style="margin-top: 1rem;">
                                <div class="card rounded-3 p-2">
                                    <div class="card-body">
                                        <h2 class="fw-bolder">Supply Position Model</h2>
                                        @foreach ($data_1 as $item)
                                            <div class="mt-4">
                                                <h4 style="font-weight: 600">{{ $item->title }}</h4>
                                                <p class="mt-2">{!! $item->konten !!}</p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 5rem;">
                                <div class="card rounded-3 p-2">
                                    <div class="card-body">
                                        <h2 class="fw-bolder">Supplier Perception Model</h2>
                                        @foreach ($data_2 as $item)
                                            <div class="mt-4">
                                                <h4 style="font-weight: 600">{{ $item->title }}</h4>
                                                <p class="mt-2">{!! $item->konten !!}</p>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
        
                {{-- <div class="row justify-content-md-center my-4">
                <div class="col-md-2">
                    <form action="{{ route('report.anggaran_pengadaan') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-primary">Lihat Rangkuman Hasil</button>
                    </form>
                </div>--}}
            </div> 
        
            <div class="row justify-content-md-center mt-4">
                <div class="col-md-auto">
                    <a href="javascript:void(0)" id="downloadPdf" class="btn btn-primary" data-html2canvas-ignore>Unduh Rangkuman Hasil</a>
                    {{-- <a href="{{ route('report.generate') }}" class="btn btn-primary">Unduh Rangkuman Hasil</a> --}}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('script')
    <script>
        $(document).ready(function(){

            let kl = "{{ $klpd[0] }}"
            
            let str = kl.replace(/\s/g, "_")
            console.log(str)
            let table = $('#table_123').DataTable({
                serverSide: true
                , processing: true
                , info: false
                , paging: false
                , searching: false
                , ajax: {
                    type: "POST"
                    , url: "{{route('report.datatable')}}",
                    data: {
                        _token: "{{ csrf_token() }}"
                    }
                , }
                , columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'text-center',
                        orderable: false
                    },
                    {
                        data: 'nama_kementrian_lembaga',
                        name: 'nama_kementrian_lembaga',
                        render: function(data, type, row){
                            return `<p id="kl">${row.nama_kementrian_lembaga}</p>`
                        }
                    },
                    {
                        data: 'jumlah_paket_penyedia',
                        name: 'jumlah_paket_penyedia',
                        render: function(data, type, row){
                            return formatRupiah(String(row.jumlah_paket_penyedia))
                        }
                    },
                    {
                        data: 'total_pagu_penyedia',
                        name: 'total_pagu_penyedia',
                        render: function(data, type, row){
                            return formatRupiah(String(row.total_pagu_penyedia), 'Rp')
                        }
                    },
                    {
                        data: 'jumlah_paket_swakelola',
                        name: 'jumlah_paket_swakelola',
                        render: function(data, type, row){
                            return formatRupiah(String(row.jumlah_paket_swakelola))
                        }
                    },
                    {
                        data: 'total_pagu_swakelola',
                        name: 'total_pagu_swakelola',
                        render: function(data, type, row){
                            return formatRupiah(String(row.total_pagu_swakelola), 'Rp')
                        }
                    },
                    {
                        data: 'jumlah_paket_pds',
                        name: 'jumlah_paket_pds'
                    },
                    {
                        data: 'total_pagu_pds',
                        name: 'total_pagu_pds',
                        render: function(data, type, row){
                            return formatRupiah(String(row.total_pagu_pds), 'Rp')
                        }
                    },
                    {
                        data: 'total_paket',
                        name: 'total_paket',
                        render: function(data, type, row){
                            return formatRupiah(String(row.total_paket))
                        }
                    },
                    {
                        data: 'total_pagu',
                        name: 'total_pagu',
                        render: function(data, type, row){
                            return formatRupiah(String(row.total_pagu), 'Rp')
                        }
                    },
                ]
            })


            let table2 = $('#table_3').DataTable({
                serverSide: true
                , processing: true
                , info: true
                , ajax: {
                    type: "GET"
                    , url: "{{route('report.anggaran_terbesar')}}"
                , }
                , columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'text-center',
                        orderable: false
                    },
                    {
                        data: 'nama_satuan_kerja',
                        name: 'nama_satuan_kerja'
                    },
                    {
                        data: null,
                        name: 'nama_paket',
                        render: function(data, type, row){
                            return row.nama_paket.length
                        }
                    },
                    {
                        data: 'total_pagu_paket',
                        name: 'total_pagu_paket',
                        render: function(data, type, row){
                            return formatRupiah(String(row.total_pagu_paket), 'Rp')
                        }
                    },
                    {
                        data: null,
                        name: 'nama_paket',
                        render: function(data, type, row){
                            return row.nama_paket.length
                        }
                    },
                    {
                        data: 'total_pagu_paket',
                        name: 'total_pagu_paket',
                        render: function(data, type, row){
                            return formatRupiah(String(row.total_pagu_paket), 'Rp')
                        }
                    },
                    {
                        data: null,
                        name: 'nama_paket',
                        render: function(data, type, row){
                            return row.nama_paket.length
                        }
                    },
                    {
                        data: 'total_pagu_paket',
                        name: 'total_pagu_paket',
                        render: function(data, type, row){
                            return formatRupiah(String(row.total_pagu_paket), 'Rp')
                        }
                    },
                    {
                        data: null,
                        name: 'nama_paket',
                        render: function(data, type, row){
                            return row.nama_paket.length
                        }
                    },
                    {
                        data: 'total_pagu_paket',
                        name: 'total_pagu_paket',
                        render: function(data, type, row){
                            return formatRupiah(String(row.total_pagu_paket), 'Rp')
                        }
                    }
                ]
            })


            function formatRupiah(angka, prefix){
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split   		= number_string.split(','),
                sisa     		= split[0].length % 3,
                rupiah     		= split[0].substr(0, sisa),
                ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if(ribuan){
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }
    
                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }


            //Chart
            const ctx = document.getElementById('myChart');
            const ctx1 = document.getElementById('myChart1');
            const ctx2 = document.getElementById('myChart2');
            const myChart = new Chart(ctx, {
                type: 'pie',
                data: {
                    labels: [
                        @foreach($a['items'] as $b)
                        '{{ $b["jenis_pengadaan"] }}',
                        @endforeach
                    ],
                    datasets: [{
                        label: '# of Votes',
                        data: [
                            @foreach($a['items'] as $b)
                            '{{ $b["pagu"] }}',
                            @endforeach
                        ],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(175, 192, 192, 0.2)',
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(175, 192, 192, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                }
            });

            const myChart1 = new Chart(ctx1, {
                type: 'pie',
                data: {
                    labels: [
                        @foreach($e['items'] as $b)
                        '{{ $b["tipe_paket"] }}',
                        @endforeach
                    ],
                    datasets: [{
                        label: '# of Votes',
                        data: [
                            @foreach($e['items'] as $b)
                            '{{ $b["total_pagu"] }}',
                            @endforeach
                        ],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(175, 192, 192, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(175, 192, 192, 1)'
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                }
            });


            const myChart2 = new Chart(ctx2, {
                type: 'pie',
                data: {
                    labels: [
                        @foreach($mp['items'] as $metode)
                        '{{ $metode["metode_pengadaan"] }}',
                        @endforeach
                    ],
                    datasets: [{
                        label: '# of Votes',
                        data: [
                            @foreach($mp['items'] as $metode)
                            '{{ $metode["total_pagu"] }}',
                            @endforeach
                        ],
                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(175, 192, 192, 0.2)',
                            'rgba(45, 30, 192, 0.2)',
                            'rgba(145, 230, 100, 0.2)',
                            'rgba(345, 18, 202, 0.3)',
                        ],
                        borderColor: [
                            'rgba(255, 99, 132, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(175, 192, 192, 1)',
                            'rgba(45, 30, 192, 1)',
                            'rgba(145, 230, 100, 1)',
                            'rgba(345, 18, 202, 1)',
                        ],
                        borderWidth: 1
                    }]
                },
                options: {
                }
            });

            $('#downloadPdf').click(function(event) {
                window.jsPDF = window.jspdf.jsPDF;
                window.html2canvas = html2canvas

                html2canvas(document.querySelector('#report')).
                    then(canvas => {
                        const img = canvas.toDataURL('image/png')

                        var pdf = new jsPDF("p","mm","Legal");
                        pdf.addImage(img, 'PNG', 6,5,204,340)
                        pdf.save(str + '.pdf');
                    })
                // get size of report page
                var reportPageHeight = $('#reportPage').innerHeight();
                var reportPageWidth = $('#reportPage').innerWidth();
            
            });

            $(document).ready(function(){
                getBottleneck()
                getCritical()
                getRoutine()
                getLeverage()
            })

            const getBottleneck = () => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('kelompok-konsolidasi-kecil-tinggi') }}",
                    success: function (res) {
                        console.log(res)
                        $.each(res, function (i, val) {
                            $('.tbody_1').append(
                                `<tr>
                                    <td>
                                        <a href="{{ url('paket-konsolidasi-detail/${val.id}') }}" class="" id="">${val.nama_paket_konsolidasi}</a>
                                    </td>
                                </tr>`
                            );
                        });
                    }
                });
            }
            const getCritical = () => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('kelompok-konsolidasi-besar-tinggi') }}",
                    success: function (res) {
                        console.log(res)
                        $.each(res, function (i, val) {
                            $('.tbody_2').append(
                                `<tr>
                                    <td>
                                        <a href="{{ url('paket-konsolidasi-detail/${val.id}') }}" class="" id="">${val.nama_paket_konsolidasi}</a>
                                    </td>
                                </tr>`
                            );
                        });
                    }
                });
            }
            const getRoutine = () => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('kelompok-konsolidasi-kecil-rendah') }}",
                    success: function (res) {
                        console.log(res)
                        $.each(res, function (i, val) {
                            $('.tbody_3').append(
                                `<tr>
                                    <td>
                                        <a href="{{ url('paket-konsolidasi-detail/${val.id}') }}" class="" id="">${val.nama_paket_konsolidasi}</a>
                                    </td>
                                </tr>`
                            );
                        });
                    }
                });
            }
            const getLeverage = () => {
                $.ajax({
                    type: "GET",
                    url: "{{ route('kelompok-konsolidasi-besar-rendah') }}",
                    success: function (res) {
                        console.log(res)
                        $.each(res, function (i, val) {
                            $('.tbody_4').append(
                                `<tr>
                                    <td>
                                        <a href="{{ url('paket-konsolidasi-detail/${val.id}') }}" class="" id="">${val.nama_paket_konsolidasi}</a>
                                    </td>
                                </tr>`
                            );
                        });
                    }
                });
            }
        })

    </script>
@endpush