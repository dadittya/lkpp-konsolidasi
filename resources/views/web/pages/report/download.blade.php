<!DOCTYPE html>
<html>
<head>
	<title>{{$data->nama_kementrian_lembaga}}</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 9pt;
		}

	</style>

	<div class="row" style="margin-right: 0.5rem;">
		<div class="col-12">
			<div style="margin-bottom: 1rem; text-align: center">
				<h5>Potensi Konsolidasi</h5>
				<h5>{{$data->nama_kementrian_lembaga}} Tahun Anggaran {{$tahun_anggaran}}</h5>
			</div>
			<div class="table-responsive">
				<table id="table_2" class="table table-bordered display" style="width:100%">
					<thead class="table-danger">
						<tr>
							<th rowspan="2">No</th>
							<th rowspan="2"><center>K/L/PD</center></th>
							<th colspan="2"><center>Penyedia</center></th>
							<th colspan="2"><center>Swakelola</center></th>
							<th colspan="2"><center>Penyedia dalam Swakelola</center></th>
							<th colspan="2"><center>Total</center></th>
						</tr>
						<tr>
							<th>Pkt</th>
							<th>Pagu</th>
							<th>Keg</th>
							<th>Pagu</th>
							<th>Pkt</th>
							<th>Pagu</th>
							<th>Keg + Pkt</th>
							<th>Total Pagu</th>
						</tr>
					</thead>
					<tbody>
					<tr>
						<td>1</td>
						<td>{{$data->nama_kementrian_lembaga}}</td>
						<td>{{$paket_penyedia}}</td>
						<td>{{"Rp " . number_format($pagu_pm,0,'','.')}}</td>
						<td>{{$paket_swakelola}}</td>
						<td>{{"Rp " . number_format($pagu_sw,0,'','.')}}</td>
						<td>{{$paket_pds}}</td>
						<td>{{"Rp " . number_format($pagu_pds,0,'','.')}}</td>
						<td>{{$total_paket}}</td>
						<td>{{"Rp " . number_format($total_pagu,0,'','.')}}</td>
					</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="row" style="margin-right: 0.5rem">
		<div class="col-md-12" style="margin-top: 5rem">
			<div style="margin-bottom: 0.5rem;">
				<p>Kategori Berdasarkan Cara Pengadaan</p>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered display" style="width:100%">
					<thead>
						<tr>
							<th>Cara Pengadaan</th>
							<th>Jumlah Pagu</th>
							<th>Jumlah Paket</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($e as $row) 
							<tr>
								<td>{{$row['tipe_paket']}}</td>
								<td>{{"Rp " . number_format($row['pagu'],0,'','.')}}</td>
								<td>{{ $row['paket'] }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="row" style="margin-right: 0.5rem">
		<div class="col-md-12" style="margin-top: 5rem">
			<div style="margin-bottom: 0.5rem;">
				<p>Kategori Berdasarkan Jenis Pengadaan</p>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered display" style="width:100%">
					<thead>
						<tr>
							<th>Jenis Pengadaan</th>
							<th>Jumlah Pagu</th>
							<th>Jumlah Paket</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($a as $item) 
							<tr>
								<td>{{$item['jenis_pengadaan']}}</td>
								<td>{{"Rp " . number_format($item['pagu'],0,'','.')}}</td>
								<td>{{ $item['jumlah_paket'] }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="row" style="margin-right: 0.5rem">
		<div class="col-md-12" style="margin-top: 5rem">
			<div style="margin-bottom: 0.5rem;">
				<p>Kategori Berdasarkan Metode Pengadaan</p>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered display" style="width:100%">
					<thead>
						<tr>
							<th>Metode Pengadaan</th>
							<th>Jumlah Pagu</th>
							<th>Jumlah Paket</th>
						</tr>
					</thead>
					<tbody>
						@foreach ($mp as $item) 
							<tr>
								<td>{{$item['metode_pengadaan']}}</td>
								<td>{{"Rp " . number_format($item['pagu'],0,'','.')}}</td>
								<td>{{ $item['jumlah_paket'] }}</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<script>

		const ctx1 = document.getElementById('myChart1');
		const myChart1 = new Chart(ctx1, {
        type: 'doughnut',
        data: {
            labels: [
                @foreach($e as $b)
                '{{ $b["tipe_paket"] }}',
                @endforeach
            ],
            datasets: [{
                label: '# of Votes',
                data: [
                    @foreach($e as $b)
                    '{{ $b["pagu"] }}',
                    @endforeach
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(175, 192, 192, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(175, 192, 192, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });
	</script>
</body>
</html>