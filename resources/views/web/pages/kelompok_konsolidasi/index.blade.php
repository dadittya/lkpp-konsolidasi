@extends('web.layouts.global')
@section('content')
    <a href="" id="btn_back" class="float-start btn btn-outline-primary shadow">Back To Daftar Konsolidasi<i
            class="fas fa-arrow-circle-left ml-2"></i></a>
    <div class="py-5">
        <div class="card ">
            <div class="card-body">
                <h5 class="fw-bold text-lg d-inline">Supply Positioning Model (SPM):</h5>
                {{-- <a href="{{ route('get-paket-konsolidasi') }}" class="float-end btn btn-outline-primary">Tambah Kelompok <i class="fas fa-folder-plus"></i></a> --}}
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 mt-5">
                <div class="card rounded-3 shadow">
                    <div class="card-header">
                        <h5 class="fw-bolder">BOTTLENECK</h5>
                    </div>

                    <div class="card-body bg-warning">
                        <div class="p-5">
                            <table class="table table-striped table-warning table-hover" id="table_1">
                                <thead>
                                    {{-- <tr>
                                        <th>Nama Paket</th>
                                    </tr> --}}
                                </thead>
                                <tbody class="tbody_1">
                                    {{-- <tr>
                                        <td>Paket Konsolidasi 1</td>
                                    </tr> --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="card rounded-3 shadow">
                    <div class="card-header">
                        <h5 class="fw-bolder">CRITICAL</h5>
                    </div>

                    <div class="card-body bg-danger">
                        <div class="p-5">
                            <table class="table table-striped table-hover table-danger" id="table_2">
                                <thead>
                                    {{-- <tr>
                                        <th>Nama Paket</th>
                                    </tr> --}}
                                </thead>
                                <tbody class="tbody_2">
                                {{-- <tr>
                                    <td>1</td>
                                    <td>Paket Konsolidasi 1</td>
                                </tr> --}}
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="card rounded-3 shadow">
                    <div class="card-header">
                        <h5 class="fw-bolder">ROUTINE</h5>
                    </div>

                    <div class="card-body bg-info">
                        <div class="p-5">
                            <table class="table table-striped table-hover table-info" id="table_3">
                                <thead>
                                    {{-- <tr>
                                        <th>Nama Paket</th>
                                    </tr> --}}
                                </thead>
                                <tbody class="tbody_3">
                                    {{-- <tr>
                                        <td>Paket Konsolidasi 1</td>
                                    </tr> --}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 mt-5">
                <div class="card rounded-3 shadow">
                    <div class="card-header">
                        <h5 class="fw-bolder">LEVERAGE</h5>
                    </div>

                    <div class="card-body bg-success">
                        <div class="p-5">
                            <table class="table table-striped table-hover table-success" id="table_4">
                                <thead>
                                    {{-- <tr>
                                        <th>Nama Paket</th>
                                    </tr> --}}
                                </thead>
                                <tbody class="tbody_4">
                                {{-- <tr>
                                    <td>1</td>
                                    <td>Paket Konsolidasi 1</td>
                                </tr> --}}
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (!empty($data_1) || !empty($data_2))
            <div class="card" class="mt-5" style="margin-top: 7rem;">
                <div class="card-header">
                    <h3 class="fw-bold">Teori Tentang SPM & SPM</h3>
                </div>
                <div class="card-body p-5">
                    <div class="row" style="margin-top: 1rem;">
                        <div class="card rounded-3 p-2">
                            <div class="card-body">
                                <h2 class="fw-bolder">Supply Position Model</h2>
                                @foreach ($data_1 as $item)
                                    <div class="mt-4">
                                        <h4 style="font-weight: 600">{{ $item->title }}</h4>
                                        <p class="mt-2">{!! $item->konten !!}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 5rem;">
                        <div class="card rounded-3 p-2">
                            <div class="card-body">
                                <h2 class="fw-bolder">Supplier Perception Model</h2>
                                @foreach ($data_2 as $item)
                                    <div class="mt-4">
                                        <h4 style="font-weight: 600">{{ $item->title }}</h4>
                                        <p class="mt-2">{!! $item->konten !!}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{-- <div class="row justify-content-md-center my-4">
        <div class="col-md-2">
            <form action="{{ route('report.anggaran_pengadaan') }}" method="POST">
                @csrf
                <button type="submit" class="btn btn-primary">Lihat Rangkuman Hasil</button>
            </form>
        </div>
    </div> --}}
        <div class="row justify-content-md-center mt-2">
            <div class="col-md-auto">
                {{-- <a href="{{ route('report.anggaran_pengadaan') }}" class="btn btn-success mt-5">Lihat Rangkuman Hasil</a> --}}
                <a href="{{ route('report.ringkasan') }}" class="btn btn-success mt-5">Lihat Rangkuman Hasil</a>
                {{-- <a href="#" onClick="alert('Under Maintenance')" class="btn btn-success mt-5">Lihat Rangkuman Hasil</a> --}}
            </div>
        </div>
    </div>

@endsection
@push('script')

    <script>
        $('#btn_back').click(function(e) {
            e.preventDefault();
            window.history.back();
        });

        $(document).ready(function () {
            getBottleneck()
            getCritical()
            getRoutine()
            getLeverage()
        });

        const getBottleneck = () => {
            $.ajax({
                type: "GET",
                url: "{{ route('kelompok-konsolidasi-kecil-tinggi') }}",
                success: function (res) {
                    console.log(res)
                    $.each(res, function (i, val) {
                        $('.tbody_1').append(
                            `<tr>
                                <td>
                                    <a href="{{ url('paket-konsolidasi-detail-report/kecil/${val.id}/1/0') }}" class="" target="_blank id="">${val.nama_paket_konsolidasi}</a>
                                </td>
                            </tr>`
                        );
                    });
                }
            });
        }
        const getCritical = () => {
            $.ajax({
                type: "GET",
                url: "{{ route('kelompok-konsolidasi-besar-tinggi') }}",
                success: function (res) {
                    console.log(res)
                    $.each(res, function (i, val) {
                        $('.tbody_2').append(
                            `<tr>
                                <td>
                                    <a href="{{ url('paket-konsolidasi-detail-report/besar/${val.id}/1/0') }}" class="" target="_blank id="">${val.nama_paket_konsolidasi}</a>
                                </td>
                            </tr>`
                        );
                    });
                }
            });
        }
        const getRoutine = () => {
            $.ajax({
                type: "GET",
                url: "{{ route('kelompok-konsolidasi-kecil-rendah') }}",
                success: function (res) {
                    console.log(res)
                    $.each(res, function (i, val) {
                        $('.tbody_3').append(
                            `<tr>
                                <td>
                                    <a href="{{ url('paket-konsolidasi-detail-report/kecil/${val.id}/1/0') }}" class="" target="_blank id="">${val.nama_paket_konsolidasi}</a>
                                </td>
                            </tr>`
                        );
                    });
                }
            });
        }
        const getLeverage = () => {
            $.ajax({
                type: "GET",
                url: "{{ route('kelompok-konsolidasi-besar-rendah') }}",
                success: function (res) {
                    console.log(res)
                    $.each(res, function (i, val) {
                        $('.tbody_4').append(
                            `<tr>
                                <td>
                                    <a href="{{ url('paket-konsolidasi-detail-report/besar/${val.id}/1/0') }}" class="" target="_blank id="">${val.nama_paket_konsolidasi}</a>
                                </td>
                            </tr>`
                        );
                    });
                }
            });
        }

        $(document).ready(function() {
            let table = $('#table_123').DataTable({
                serverSide: true,
                processing: true,
                info: false,
                paging: false,
                ajax: {
                    type: "POST",
                    url: "{{ route('report.datatable') }}",
                    data: {
                        _token: "{{ csrf_token() }}"
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'text-center',
                        orderable: false
                    },
                    {
                        data: 'nama_kementrian_lembaga',
                        name: 'nama_kementrian_lembaga'
                    },
                    {
                        data: 'jumlah_paket_penyedia',
                        name: 'jumlah_paket_penyedia',
                        render: function(data, type, row) {
                            return formatRupiah(String(row.jumlah_paket_penyedia))
                        }
                    },
                    {
                        data: 'total_pagu_penyedia',
                        name: 'total_pagu_penyedia',
                        render: function(data, type, row) {
                            return formatRupiah(String(row.total_pagu_penyedia), 'Rp')
                        }
                    },
                    {
                        data: 'jumlah_paket_swakelola',
                        name: 'jumlah_paket_swakelola',
                        render: function(data, type, row) {
                            return formatRupiah(String(row.jumlah_paket_swakelola))
                        }
                    },
                    {
                        data: 'total_pagu_swakelola',
                        name: 'total_pagu_swakelola',
                        render: function(data, type, row) {
                            return formatRupiah(String(row.total_pagu_swakelola), 'Rp')
                        }
                    },
                    {
                        data: 'jumlah_paket_pds',
                        name: 'jumlah_paket_pds'
                    },
                    {
                        data: 'total_pagu_pds',
                        name: 'total_pagu_pds',
                        render: function(data, type, row) {
                            return formatRupiah(String(row.total_pagu_pds), 'Rp')
                        }
                    },
                    {
                        data: 'total_paket',
                        name: 'total_paket',
                        render: function(data, type, row) {
                            return formatRupiah(String(row.total_paket))
                        }
                    },
                    {
                        data: 'total_pagu',
                        name: 'total_pagu',
                        render: function(data, type, row) {
                            return formatRupiah(String(row.total_pagu), 'Rp')
                        }
                    },
                ]
            })


            function formatRupiah(angka, prefix) {
                var number_string = angka.replace(/[^,\d]/g, '').toString(),
                    split = number_string.split(','),
                    sisa = split[0].length % 3,
                    rupiah = split[0].substr(0, sisa),
                    ribuan = split[0].substr(sisa).match(/\d{3}/gi);

                // tambahkan titik jika yang di input sudah menjadi angka ribuan
                if (ribuan) {
                    separator = sisa ? '.' : '';
                    rupiah += separator + ribuan.join('.');
                }

                rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
                return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
            }


        })
    </script>
@endpush
