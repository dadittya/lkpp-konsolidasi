@extends('web.layouts.global')
@section('slider')
@include('web.layouts.utils.slider')
@endsection
@section('content')
<section>
    <div class="col-lg-12 " style="z-index: 9999999 !important; margin-top: -130px;">
        <div class="card rounded-pill shadow p-5">
            <div class="card-body">
    <table class="table" class="table table-striped table-dark">
        <thead >
          <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Deskripsi</th>
            <th scope="col">Aksi</th>
          </tr>
        </thead>
        @php
            $i = 1;
        @endphp
        <tbody>
            @foreach($data as $item)
                <tr>
                    <th scope="row" style="width: 5%">{{ $i++ }}</th>
                    <td style="width: 20%">{{ $item->name}}</td>
                    <td>{{ $item->description}}</td>
                    <td style="width: 5%"><a href="{{ 'https://' . $item->url }}" class="btn btn-info" role="button" target="_blank">Lihat</a></td>
                </tr>
            @endforeach
          
        </tbody>
      </table>
            </div>
        </div>
    </div>
</section>
@endsection