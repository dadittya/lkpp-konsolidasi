@extends('web.layouts.global')
@section('content')

<div class="row mt-10 py-2">
    <div>
        <div class="row">
            <div>
                <div style="margin-bottom: 1rem; text-align: center">
                    <p style="font-size: 14pt; font-weight: bold; text-align: center">Pareto</p>
                    <p style="font-size: 14pt; font-weight: bold; text-align: center">{{$instansi}} Tahun Anggaran {{$tahun}}</p>
			</div>
                <div class="card shadow-sm mt-3">
                    {{-- <div class="card-header">Pareto {{ $instansi }} {{ $tahun }}</div> --}}
                    <div class="card-body">
                        <div class="table-responsive">
                            <form action="" id="form_konsolidasi" method="POST">
                                @csrf
                                <table id="table_1" class="table table-striped table-hover text-start">
                                    <thead class="table-primary">
                                        <tr>
                                            <th>No</th>
                                            <th>ID Paket</th>
                                            <th>Nama Paket</th>
                                            <th>Pagu Anggaran</th>
                                            <th>Kumulatif Pagu</th>
                                            <th>Presentase</th>
                                            <th>Klasifikasi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="t_body">
                                    </tbody>
                                </table>
                        </div>    
                    </div>
                    <div class="card-header">
                        Total Pagu : {{ number_format($total, 0, ',', '.')}}
                    </div>
                </div>
            </div>
            
            </form>
        </div>
    </div>
</div>


@endsection
@push('script')
<script>
   
    $(document).ready(function() {

        function reloadData(){
            //$('#table_1').DataTable.ajax.reload()
        }

        let table = $('#table_1').DataTable({
            serverSide: true, 
            orderable: false,
            processing: true, 
            pageLength: 50,
            ajax: {
                type: "GET",
                url: "{{route('report.pareto')}}",
                data: {
                    _token: "{{ csrf_token() }}",
                    instansiId: "{{ $instansiId }}",
                    tahun: "{{ $tahun }}"
                }
            }, 
            columns: [{
                    data: 'DT_RowIndex', 
                    name: 'DT_RowIndex',
                    render: $.fn.dataTable.render.number('.', ',', 0, ''),
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'id_paket',
                    name: 'id_paket',
                    orderable: false,
                }, 
                {
                    data: 'nama_paket',
                    name: 'nama_paket',
                    orderable: false,
                }, 
                {
                    data: 'pagu_anggaran',
                    name: 'pagu_anggaran',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number('.', ',', 0, ''),
                    orderable: false,
                    searchable: false
                }, 
                {
                    data: 'kumulatif_pagu',
                    name: 'kumulatif_pagu',
                    className: 'text-right',
                    render: $.fn.dataTable.render.number('.', ',', 0, ''),
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'persentase_terhadap_total',
                    name: 'persentase_terhadap_total',
                    render: $.fn.dataTable.render.number('.', ',', 2, ''),
                    className: 'text-right',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'klasifikasi_anggaran',
                    name: 'klasifikasi_anggaran',
                    orderable: false,
                    searchable: false
                }
            ]
        })

        $(window).focus(function() {
           table.ajax.reload();
        });
    });

</script>
@endpush
