@extends('web.layouts.global')
@section('content')

    <a href="" id="btn_back" class="float-start btn btn-outline-primary shadow">Back To Daftar Konsolidasi <i
            class="fas fa-arrow-circle-left"></i></a>

    <section class="row mt-5">
        <div class="container-fluid">
            <div class="col-lg-12">
                <div class="card shadow-sm">
                    <div class="card-body">
                        <h2 class="fw-bold text-xl d-inline">Detail Paket Konsolidasi</h2>
                        
                        <button type="button" id="btn_save_selected" class="btn btn-sm btn-success shadow float-right d-inline">
                            <i class="fas fa-save"></i> Simpan Sebagai Paket Baru</button>
                        <button type="button" id="btn_delete_all" class="btn btn-sm btn-danger shadow float-right d-inline" data-id="{{ $paket_konsolidasi->id }}">
                            <i class="fas fa-trash"></i> Hapus Paket Terpilih</button>
                            {{-- <div class="card shadow-sm mb-3 p-4"> --}}
                                
                                <input class="form-control mt-5" autocomplete="off" id="nama_paket_konsolidasi"
                                    name="nama_paket_konsolidasi" type="text" placeholder="Masukan Nama Paket"
                                    data-id="{{ $paket_konsolidasi->id }}"
                                    aria-label="default input example" value="{{ $paket_konsolidasi->nama_paket_konsolidasi }}">
                            {{-- </div> --}}
                        <select class="form-select mt-5" name="tingkat_resiko" id="tingkat_resiko"
                            data-id="{{ $paket_konsolidasi->id }}">
                            <option value="">Pilih Resiko</option>
                            <option value="diabaikan" {{ $paket_konsolidasi->tingkat_resiko == 'diabaikan' ? 'selected' : '' }}>
                                Diabaikan</option>
                            <option value="rendah" {{ $paket_konsolidasi->tingkat_resiko == 'rendah' ? 'selected' : '' }}>
                                Rendah</option>
                            <option value="sedang" {{ $paket_konsolidasi->tingkat_resiko == 'sedang' ? 'selected' : '' }}>
                                Sedang</option>
                            <option value="tinggi" {{ $paket_konsolidasi->tingkat_resiko == 'tinggi' ? 'selected' : '' }}>
                                Tinggi</option>
                        </select>
                        <input class="form-control mt-5" autocomplete="off" id="keyword"
                                    name="keyword" type="text" placeholder="Pisahkan Keyword Dengan Spasi"
                                    aria-label="default input example">
                    </div>
                </div>


                <div class="card shadow-sm mt-3">
                    <div class="card-header">Daftar Isi Paket Konsolidasi</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form action="" method="POST" id="form_detail">
                                @csrf
                                <table id="table_1" class="table table-striped table-hover text-start" style="width:100%">
                                    <thead class="table-primary">
                                        <tr class="text-start">
                                            <th>No</th>
                                            <th class="text-center"><input type="checkbox" name="select" id="select">
                                            </th>
                                            <th>Nama Paket Asli</th>
                                            <th>Pagu Anggaran</th>
                                            {{-- <th>Kumulatif Pagu</th>
                                            <th>Persentase Terhadap Total</th> --}}
                                            <th>Klasifikasi Anggaran</th>
                                            <th>Id Sirup</th>
                                            <th>Nama Satker</th>
                                            <th>Metode Pengadaan</th>
                                            <th>Jenis Pengadaan</th>
                                            <th>UKM</th>
                                            <th>Aksi</th>
                                            <th>PPK</th>
                                            <th>Sumber Dana</th>
                                            <th>MAK</th>
                                        </tr>
                                    </thead>
                                    <tbody id="t_body">
                                    </tbody>
                                </table>
                            </form>
                        </div>
                        {{-- <h2 class="fw-bold text-sm  mt-4">Total Pembagi Pareto :
                            {{ number_format($paket_konsolidasi->total_pagu_pareto, 2, ',', '.') ?? '-' }}</h2> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
@push('script')
    <script>
        $(document).ready(function() {
            
            $('[name=select]').click(function() {
                $(':checkbox').prop('checked', this.checked)
            })

            $('#btn_back').click(function(e) {
                e.preventDefault();
                window.history.back();
            });


            //Delete multiple
            $('#btn_save_selected').click(function(e) {
                e.preventDefault();

                if ($('input:checked').length > 1) {

                    $('.loading').attr('hidden', false)
                    $('button').attr('disabled', true)
                    e.preventDefault();
                    // $.post("{{ route('paket-konsolidasi-detail-save-selected') }}",
                    //         $('#form_detail').serialize())
                    //     .done((response) => {
                    //         $('.loading').attr('hidden', true)
                    //         $('button').attr('disabled', false)
                    //         if (response.message == 'success') {
                    //             swal("Data Berhasil Disimpan!", {
                    //                 icon: "success",
                    //             });
                    //             window.history.back();
                    //         }
                    //         else {
                    //             swal("Data Gagal Disimpan!", {
                    //                 icon: "warning",
                    //             });
                    //         }
                    //     })
                    //     .fail((errors) => {
                    //         console.log(errors)
                    //         $('.loading').attr('hidden', true)
                    //         $('button').attr('disabled', false)
                    //         alert('Tidak dapat menyimpan data!')
                    //         return;
                    //     })
                    
                    $.ajax({
                        type: "POST",
                        url: "{{ route('paket-konsolidasi-detail-save-selected') }}",
                        data: {
                            konsolidasi_id: "{{ $paket_konsolidasi->id }}", 
                            nama_paket: document.getElementById('nama_paket_konsolidasi').value,
                            tingkat_resiko: document.getElementById('tingkat_resiko').value,
                            picked: $("input[name='picked[]']:checked").map(function(){return $(this).val();}).get(),
                            _token: "{{ csrf_token() }}"
                        },
                        success: function(res) {
                            if (res.message == 'success') {
                                    swal("Data Berhasil Disimpan!", {
                                        icon: "success",
                                    });
                                    window.history.back();
                                }
                                else {
                                    swal("Data Gagal Disimpan!", {
                                        icon: "warning",
                                    });
                                }
                            },
                        error: function(err) {
                            console.log(err)
                                $('.loading').attr('hidden', true)
                                $('button').attr('disabled', false)
                                alert('Tidak dapat menyimpan data!')
                                return;
                        }
                    }); 

                } else {
                    swal("Pilih Paket Yang Akan Disimpan!");
                }
            });

            $('#btn_delete_all').click(function(e) {
                e.preventDefault();

                if ($('input:checked').length > 1) {
                    swal({
                            title: "Apakah Anda Yakin?",
                            text: "Ini Akan Menghapus Paket Asli Yang Dipilih Dari Konsolidasi!",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        })
                        .then((willDelete) => {
                            if (willDelete) {
                                $('.loading').attr('hidden', false)
                                $('button').attr('disabled', true)
                                $.post("{{ route('paket-konsolidasi-detail-delete-paket-asli-all') }}",
                                        $('#form_detail').serialize())
                                    .done((response) => {
                                        $('.loading').attr('hidden', true)
                                        $('button').attr('disabled', false)
                                        if (response.message == 'success') {
                                            swal("Data Berhasil Dihapus!", {
                                                icon: "success",
                                            });
                                            $('#table_1').DataTable().ajax.reload();
                                        }
                                        else {
                                            swal("Data Gagal Dihapus!", {
                                                icon: "warning",
                                            });
                                        }
                                    })
                                    .fail((errors) => {
                                        console.log(errors)
                                        $('.loading').attr('hidden', true)
                                        $('button').attr('disabled', false)
                                        alert('Tidak dapat menghapus data!')
                                        return;
                                    })
                            } else {
                                 swal("Hapus Dibatalkan!");
                            }
                       });

                } else {
                    swal("Pilih Paket Yang Akan Dihapus!");
                }
            });
            
        });

        $('#table_1').DataTable({
                dom: 'Blfrtip',
                language: {
                    "decimal": ",",
                    "thousands": "."
                },
                bPaginate: false,
                columnDefs: [
                    {targets:2, type: 'num-fmt' }   
                ],
                processing: true,
                bFilter: false,
                language : {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '},
                serverSide: true,
                processing: true,
                ajax: {
                    type: "GET",
                    url: "{{ route('get-paket-konsolidasi-detail-data') }}",
                    // data: {
                    //     konsolidasi_id: "{{ $paket_konsolidasi->id }}",
                    //     keyword: document.getElementById("keyword").value,
                    //     _token: "{{ csrf_token() }}"
                    // },
                    data: function ( d ) {
                            d.konsolidasi_id = "{{ $paket_konsolidasi->id }}";
                            d.keyword = $("#keyword").val();
                            d._token = "{{ csrf_token() }}";
                        }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        className: 'text-center',
                        searchable: false,
                        orderable: false
                    }, 
                    {
                        data: 'pick',
                        name: 'pick',
                        searchable: false,
                        orderable: false
                    }, 
                    {
                        data: 'nama_paket_asli',
                        name: 'nama_paket_asli'
                    }, 
                    {
                        data: 'pagu_anggaran',
                        name: 'pagu_anggaran',
                        className: 'text-center'
                    }, 
                    {
                        data: 'klasifikasi_anggaran',
                        name: 'klasifikasi_anggaran',
                        className: 'text-center'
                    }, 
                    {
                        data: 'id_sirup',
                        name: 'id_sirup',
                        className: 'text-center'
                    },  
                    {
                        data: 'nama_satker',
                        name: 'nama_satker'

                    },
                    {
                        data: 'metode_pengadaan',
                        name: 'metode_pengadaan'

                    }, 
                    {
                        data: 'jenis_pengadaan',
                        name: 'jenis_pengadaan'

                    },
                    {
                        searchable: false,
                      
                        data: 'rup.statususahakecil',
                        name: 'rup.statususahakecil'

                    }, 
                    {
                        searchable: false,
                        orderable: false,
                        render: function(data, type, row) {
                            let btn =
                                `<button class="btn btn-outline-danger" id="btn_hapus" value="${row.id}">Hapus</button>`;
                            return btn;
                        }
                    }, 
                    {
                        data: 'ppk'
                        , name: 'ppk'
                    
                    },
                    {
                        data: 'sumber_dana',
                        name: 'sumber_dana'

                    }, 
                    {
                        data: 'kode_akun',
                        name: 'kode_akun'
                    }
                ]
            })

        $(document).on('change', '#tingkat_resiko', function(e) {
            // console.log($(this).data('id'))
            $.ajax({
                type: "POST",
                url: "{{ route('paket-konsolidasi-update-resiko') }}",
                data: {
                    konsolidasi_id: $(this).data('id'),
                    tingkat_resiko: $(this).val(),
                    _token: "{{ csrf_token() }}"
                },
                success: function(res) {
                    // alert(res.message)
                    swal("Berhasil!", res.message, "success");
                },
                error: function(err) {
                    swal("Error!", err.responseJSON.message, "warning");
                }
            });
        })

        // $(document).on('blur', '#nama_paket_konsolidasi', function(e) {
        //     // console.log($(this).data('id'))
        //     $.ajax({
        //         type: "POST",
        //         url: "{{ route('paket-konsolidasi-update-nama') }}",
        //         data: {
        //             konsolidasi_id: $(this).data('id'),
        //             namaPaketKonsolidasi: $(this).val(),
        //             _token: "{{ csrf_token() }}"
        //         },
        //         success: function(res) {
        //             // alert(res.message)
        //             swal("Berhasil!", res.message, "success");
        //         },
        //         error: function(err) {
        //             swal("Error!", err.responseJSON.message, "warning");
        //         }
        //     });
        // })


        $(document).on('keyup', '#nama_paket_konsolidasi', function(e) {
            // console.log($(this).data('id'))
            if (event.keyCode === 13) {
                $.ajax({
                    type: "POST",
                    url: "{{ route('paket-konsolidasi-update-nama') }}",
                    data: {
                        konsolidasi_id: $(this).data('id'),
                        namaPaketKonsolidasi: $(this).val(),
                        _token: "{{ csrf_token() }}"
                    },
                    success: function(res) {
                        // alert(res.message)
                        swal("Berhasil!", res.message, "success");
                    },
                    error: function(err) {
                        swal("Error!", err.responseJSON.message, "warning");
                    }
                });
            }
        })

        $(document).on('keyup', '#keyword', function(e) {
            
            if (event.keyCode === 13){ 
                $('#table_1').DataTable().ajax.reload();
                
            }
        })

        $(document).on('click', '#btn_hapus', function(e) {
            e.preventDefault()
            swal({
                    title: "Apakah Anda Yakin?",
                    text: "Ini Akan Menghapus Paket Asli Dari Konsolidasi!",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        $.ajax({
                            type: "POST",
                            url: "{{ route('paket-konsolidasi-detail-delete-paket-asli') }}",
                            data: {
                                id: $(this).val(),
                                _token: "{{ csrf_token() }}"
                            },
                            success: function(res) {
                                swal(res.message, {
                                    icon: "success",
                                });
                                $('#table_1').DataTable().ajax.reload();
                            },
                            error: function(xhr) {
                                swal(xhr.responseJSON.message, {
                                    icon: "warning",
                                });
                            }
                        });
                    } else {
                        swal("Hapus Dibatalkan!");
                    }
                });
        })
    </script>
@endpush
