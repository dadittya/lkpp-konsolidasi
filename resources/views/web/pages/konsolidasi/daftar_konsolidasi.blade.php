@extends('web.layouts.global')
@section('content')
<a href="" id="btn_back" class="float-start btn btn-outline-primary shadow">Back To Kata Popular <i class="fas fa-arrow-circle-left"></i></a>

<div class="row mt-10 py-2">
    <div class="col-12">
        <div class="row">
            <div class="col-md-9">
                <div class="card">
                    <div class="card-body">
                        <div class="p-3">
                            <h2 class="fw-bold text-xl d-inline">Daftar Paket Konsolidasi </h2>
                            <button type="button" id="btn_delete_all" class="btn btn-outline-danger shadow float-right d-inline mr-2">
                                <i class="fas fa-trash-alt"></i> Delete Selected</button>
                        </div>
                    </div>
                </div>
                <div class="card shadow-sm mt-3">
                    <div class="card-header">List Paket Konsolidasi</div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <form action="" id="form_konsolidasi" method="POST">
                                @csrf
                                <table id="table_1" class="table table-striped table-hover text-start" style="width:100%">
                                    <thead class="table-primary">
                                        <tr>
                                            <th>No</th>
                                            <th class="text-center"><input type="checkbox" name="select" id="select"></th>
                                            <th>Nama Paket Konsolidasi</th>
                                            <th>Tingkat Resiko</th>
                                            <th>Dibuat Tanggal</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="t_body">
                                    </tbody>
                                </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="card">
                    <div class="card-body">
                        {{-- <a href="{{ route('get-kelompok-konsolidasi') }}" class="btn btn-outline-primary ml-3 p-2">Lihat Kategori Dalam SPM <i class="fas fa-arrow-circle-right"></i></a> --}}
                        <button type="button" id="test_button" class="btn btn btn-outline-primary shadow ml-3 p-2">
                            Lihat Kategori Dalam SPM <i class="fas fa-arrow-circle-right"></i></button>
                    </div>

                </div>
            </div>
            </form>
        </div>
    </div>
</div>


@endsection
@push('script')
<script>
   
    $(document).ready(function() {

        function reloadData(){
            //$('#table_1').DataTable.ajax.reload()
        }
       
        $('#btn_back').click(function (e) {
            e.preventDefault();
            window.history.back();
        });

        $('[name=select]').click(function() {
            $(':checkbox').prop('checked', this.checked)
        })

        //Delete multiple
        $('#btn_delete_all').click(function(e) {
            e.preventDefault();
            if ($('input:checked').length >= 1) {
                swal({
                        title: "Apakah Anda Yakin?"
                        , text: "Ini Akan Menghapus Paket Konsolidasi!"
                        , icon: "warning"
                        , buttons: true
                        , dangerMode: true
                    , })
                    .then((willDelete) => {
                        if (willDelete) {
                            $.post("{{ route('paket-konsolidasi-delete-all') }}", $('#form_konsolidasi').serialize())
                                .done((response) => {
                                    swal("Data Berhasil Dihapus!", {
                                        icon: "success"
                                    , });
                                    $('#table_1').DataTable().ajax.reload();
                                })
                                .fail((errors) => {
                                    console.log(errors)
                                    alert('Tidak dapat menghapus data!')
                                    return;
                                })
                        } else {
                            swal("Hapus Dibatalkan!");
                        }
                    });

            } else {
                swal("Pilih Paket Yang Akan Dihapus!");
            }
        });


        let table = $('#table_1').DataTable({
            serverSide: true
            , processing: true
            , ajax: {
                type: "GET"
                , url: "{{route('get-paket-konsolidasi-data')}}"
                , data: {
                    _token: "{{ csrf_token() }}"
                }
            , }
            , columns: [{
                    data: 'DT_RowIndex'
                    , name: 'DT_RowIndex'
                    , orderable: false
                },
                {
                    targets: 0
                    , searchable: false
                    , className: 'dt-body-center'
                    , orderable: false
                    , render: function(data, type, row) {
                        return '<input class="text-center" type="checkbox" name="paket_konsolidasi[]"  id ="chk_paket" value="' + row.id + '">';
                    }
                }

                , {
                    data: 'nama_paket_konsolidasi'
                    , name: 'nama_paket_konsolidasi'
                    , render: function(data, type, row) {
                        return `<h5 class="fw-bolder">${row.nama_paket_konsolidasi}</h5>`
                    }
                }
                , {
                    data: 'tingkat_resiko'
                    , name: 'tingkat_resiko'
                    , orderable: false
                    , render: function(data, type, row) {
                        return `<h5 class="fw-bolder">${row.tingkat_resiko ? row.tingkat_resiko.toUpperCase() : row.tingkat_resiko == 'tinggi' ? "TINGGI" : '-'}</h5>`
                    }
                }
                , {
                    data: 'created_at'
                    , name: 'created_at'
                }
                , {
                    data: 'aksi',
                    name: 'aksi'
                }
            ]
        })

        $(window).focus(function() {
           table.ajax.reload();
        });
    });

    $(document).on('click', '#test_button', function(e) {
        if ($('input:checked').length >= 1) {
        
            $.post("{{ route('get-kelompok-konsolidasi') }}", $('#form_konsolidasi').serialize())
            .done((response) => {
                if (response.status) {
                    window.location.href = "{{ route('get-kelompok-konsolidasi') }}";
                }
                else {
                    swal("Paket Yang Dipilih Harus Dalam KL/Pemda Yang Sama", {
                                        icon: "warning"
                                    , });
                }
            })
            .fail((errors) => {
                swal("Terjadi Kesalahan", {
                                        icon: "warning"
                                    , });
            })
        }
        else {
            swal("Pilih Paket Terlebih Dahulu!", {
                                        icon: "warning"
                                    , });
            }
    })

    $(document).on('click', '#btn_hapus', function(e) {
        e.preventDefault()
        swal({
                title: "Apakah Anda Yakin?"
                , text: "Tindakan Ini Akan Menghapus Paket"
                , icon: "warning"
                , buttons: true
                , dangerMode: true
            , })
            .then((willDelete) => {
                if (willDelete) {
                    $.ajax({
                        type: "POST"
                        , url: "{{route('kelompok-konsolidasi-delete')}}"
                        , data: {
                            id: $(this).val()
                            , _token: "{{ csrf_token() }}"
                        }
                        , success: function(res) {
                            swal(res.message, {
                                icon: "success"
                            , });
                            $('#table_1').DataTable().ajax.reload();
                        }
                    });
                } else {
                    swal("Hapus Dibatalkan");
                }
            });
    })

</script>
@endpush
