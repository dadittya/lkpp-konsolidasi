@extends('web.layouts.global')
@section('content')
<div class="row mt-10">
    <div class="col-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Daftar Paket</h5>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="table_2" class="table table-bordered" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Paket Konsolidasi</th>
                                        <th>Tingkat Resiko</th>
                                        <th>No</th>
                                        <th>Nama Paket Asli</th>
                                        <th>Pagu Anggaran</th>
                                        <th>ID Sirup</th>
                                        <th>Aksi</th>
                                        <th>Nama Satker</th>
                                        <th>Metode Pengadaan</th>
                                        <th>Jenis Pengadaan</th>
                                        <th>Kode Akun</th>
                                        <th>PPK</th>
                                        <th>Sumber Dana</th>
                                    </tr>
                                </thead>
                                <tbody id="t_body">
                                    <tr>
                                        <td rowspan="8">1</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="8">Nama Paket Konsolidasi 1</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="8">D/R/S/T</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Pengadaan Meja Dan Kursi SDN 1</td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button class="btn btn-outline-danger" id="btn_hide"><i class="fas fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Pengadaan Meja Dan Kursi SDN 2</td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button class="btn btn-outline-danger" id="btn_hide"><i class="fas fa-trash"></i></button>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Pengadaan Meja Dan Kursi SDN 3</td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button class="btn btn-outline-danger" id="btn_hide"><i class="fas fa-trash"></i></button>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Pengadaan Meja Dan Kursi SDN 4</td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button class="btn btn-outline-danger" id="btn_hide"><i class="fas fa-trash"></i></button>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>Pengadaan Meja Dan Kursi SDN 5</td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button class="btn btn-outline-danger" id="btn_hide"><i class="fas fa-trash"></i></button>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
