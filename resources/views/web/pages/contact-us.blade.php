@extends('web.layouts.global')
@section('slider')
@include('web.layouts.utils.slider')
@endsection
@section('content')
<section>
    <div class="col-lg-12 " style="z-index: 9999999 !important; margin-top: -130px;">
        <div class="card rounded-pill shadow p-5">
            <div class="card-body">
                <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-5">
                    <h2><strong>Hubungi Kami</strong></h2>
                    <p>&nbsp;</p>
                    <p>Direkrorat Advokasi Pemerintah Daerah<br />Gedung LKPP Lantai 3<br />Jl. Epicentrum Tengah Lot 11 B, Jakarta Selatan, DKI Jakarta - 12940<br />Email: <a href="mailto:dit.apd@lkpp.go.id">dit.apd@lkpp.go.id</a></p>
                    <p>&nbsp;</p>
                </div>
                <div class="col-lg-5">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.355580173123!2d106.83102041458041!3d-6.216751295499722!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f3c150abcccf%3A0x7567dbac7e5450ee!2sNational%20Public%20Procurement%20Agency!5e0!3m2!1sen!2sid!4v1654126106661!5m2!1sen!2sid" width="400" height="300" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
                <div class="col-lg-1"></div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection