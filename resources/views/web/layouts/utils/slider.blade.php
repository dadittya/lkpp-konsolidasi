<!-- Page header with logo and tagline-->
<div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
    <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
    </div>
    <div class="carousel-inner">
        @forelse($banners as $key => $banner)
        @if ($key == 0)
        <div class="carousel-item active">
            <img style="height: 550px;" src="{{ asset('images/'.$banner->images) }}" class="d-block w-100" alt="...">
        </div>
        @endif
        @if ($key > 0)
        <div class="carousel-item">
            <img style="height: 550px;" src="{{ asset('images/'.$banner->images) }}" class="d-block w-100" alt="...">
        </div>
        @endif
        @empty

        <div class="carousel-item active">
            <img style="height: 550px;" src="https://images.unsplash.com/photo-1614850715649-1d0106293bd1?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item ">
            <img style="height: 550px;" src="https://images.unsplash.com/photo-1614850715649-1d0106293bd1?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80" class="d-block w-100" alt="...">
        </div>
        <div class="carousel-item">
            <img style="height: 550px;" src="https://images.unsplash.com/photo-1614852624356-ca225b786805?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80" class="d-block w-100" alt="...">
        </div>
        @endforelse

    </div>
    {{-- <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
    </button>
    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
    </button> --}}
</div>
<!-- Page content-->
