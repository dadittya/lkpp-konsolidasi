<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Konsolidasi PBJ</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="{{ asset('template/favico.ico') }}" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.0.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.11.3/css/dataTables.bootstrap5.min.css" rel="stylesheet">
    <link href="{{ asset('template/css/styles.css') }}" rel="stylesheet" />
    <link href="{{ asset('template/assets/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />
    {{-- <link href="{{ asset('template/assets/plugins/datatables/datatables.min.css') }}" rel="stylesheet" /> --}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <style>
        #img_loading {
            z-index: 99999900 !important;
            position: absolute;
            position: fixed;
            margin-left: 45%;
            margin-top: 20%;
            width: 100px;
        }

        #text_loading {
            z-index: 99999900 !important;
            position: absolute;
            position: fixed;
            margin-left: 45%;
            margin-top: 26%;
            width: 100px;
        }

        @media only screen and (max-width: 600px) {
            #img_loading {
                z-index: 99999900 !important;
                position: absolute;
                position: fixed;
                margin-left: 40%;
                margin-top: 20%;
                width: 80px;
            }

            #text_loading {
                z-index: 99999900 !important;
                position: absolute;
                position: fixed;
                margin-left: 38%;
                margin-top: 35%;
            }
        }

        thead th {
            height: 70px;
        }

    </style>
    @stack('style')
</head>
<body>
    <div class="loading" hidden>
        <center>
            <img src="{{ asset('images/loading.gif') }}" alt="loading" class="" id="img_loading" class="modal">
            <h5 id="text_loading">Sedang Memproses..</h5>
        </center>
    </div>
    <!-- Responsive navbar-->
    @include('web.includes.navbar')
    @yield('slider')
    <div class="container-fluid p-5">
        @yield('content')
    </div>
    <!-- Footer-->
    <footer class="py-5 bg-dark">
        <div class="container">
            <p class="m-0 text-center text-white">Copyright &copy; 2022 | LKPP</p>
        </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.11.3/js/dataTables.bootstrap5.min.js"></script>
    <!-- Core theme JS-->
    <script src="{{ asset('template/js/scripts.js') }}"></script>
    <script src="{{ asset('template/assets/plugins/select2/js/select2.full.min.js') }}"></script>
    {{-- <script src="{{ asset('template/assets/plugins/datatables/datatables.min.js') }}"></script> --}}
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    {{-- <script src="https://unpkg.com/echarts/dist/echarts.min.js"></script>
    <!-- Chartisan -->
    <script src="https://unpkg.com/@chartisan/echarts/dist/chartisan_echarts.js"></script> --}}
    
    <script src="{{ asset('assets/js/chart.min.js') }}"></script>
    <script src="{{ asset('assets/js/jspdf.umd.min.js') }}"></script>
    <script src="{{ asset('assets/js/html2canvas.js') }}"></script>
    <script src="{{ asset('assets/js/html2canvas.min.js') }}"></script>
    <script src="https://www.kryogenix.org/code/browser/sorttable/sorttable.js"></script>
    @include('sweetalert::alert')
    @stack('script')
    <script>
    </script>
</body>
</html>
