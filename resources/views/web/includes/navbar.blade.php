<nav class="navbar navbar-expand-lg bg-light shadow-xl">
    <div class="container">
        <div>
        <a href="{{ asset('/') }}">
            <img src="{{ asset('template/foto-LKPP.png') }}" width="50" />
        </a>
        </div>
        <div class = "nav-link text-dark" style="font-size: 20px;  padding-left: 20px; font-weight: bolder ">
            Konsolidasi
        </div>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse " id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                <li class="nav-item"><a class="nav-link text-dark" href="{{ url('/') }}" style="font-weight: bolder">Beranda</a></li>
                <li class="nav-item"><a class="nav-link text-dark" href="" style="font-weight: bolder" onclick="window.open('{{ route('tahapan-konsolidasi')}}','popup','width=1024,height=800')"; return false>Tahapan Konsolidasi</a></li>
                <li class="nav-item"><a class="nav-link text-dark" href="{{ route('contoh-konsolidasi')}}" style="font-weight: bolder">Referensi Konsolidasi</a></li>
                {{-- <li class="nav-item"><a class="nav-link active text-dark" aria-current="page" href="#" style="font-weight: bolder">Kontak Kami</a></li> --}}
                <li class="nav-item"><a class="nav-link active text-dark" aria-current="page" href="{{ route('kontak')}}" style="font-weight: bolder">Kontak Kami</a></li>
                <li class="nav-item"><a class="nav-link text-dark btn btn-sm btn-outline-success {{ Auth::check() ? "d-none" : ""}}" aria-current="page" href="{{ route('login') }}" style="font-weight: bolder">Login</a></li>
                @if (Auth::check())
                <li>
                    <form action="{{ route('logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="btn btn-sm btn-outline-danger mt-2 ml-5">
                            <i class="fas fa-sign-out-alt fa-sm fa-fw text-white"></i>
                            Logout
                        </button>
                    </form>
                </li>
                @endif
            </ul>
        </div>
    </div>
</nav>
