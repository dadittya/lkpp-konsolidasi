<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MklpdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = DB::table('data')->select('nama_kementrian_lembaga as text')->distinct()->get();

        foreach ($data as $key => $value) {
            DB::table('m_klpd')->insert([
                'text' => $value->text
            ]);
        }
    }
}
