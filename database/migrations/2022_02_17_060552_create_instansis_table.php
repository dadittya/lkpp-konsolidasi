<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInstansisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instansis', function (Blueprint $table) {
            $table->char('id', 6)->index();
            $table->string('nama');
            $table->string('jenis');
            $table->string('alamat')->nullable();
            $table->char('provinsi_id', 2);
            $table->char('kabupaten_id', 6);
            $table->timestamps();

            $table->foreign('provinsi_id')->references('id')->on('provinsis');
            $table->foreign('kabupaten_id')->references('id')->on('kabupatens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instansis');
    }
}
