<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddKumulatifPaguColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_paket_konsolidasis', function (Blueprint $table) {
            $table->bigInteger('kumulatif_pagu')->nullable();
            $table->string('persentase_terhadap_total')->nullable();
            $table->string('klasifikasi_anggaran')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_paket_konsolidasis', function (Blueprint $table) {
            $table->dropColumn('kumulatif_pagu');
            $table->dropColumn('persentase_terhadap_total');
            $table->dropColumn('klasifikasi_anggaran');
        });
    }
}
