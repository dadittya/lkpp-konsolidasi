<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangePaguAnggaranToDecimalOnDetailPaketKonsolidasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_paket_konsolidasis', function (Blueprint $table) {
            $table->decimal('pagu_anggaran', 15, 0)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('decimal_on_detail_paket_konsolidasis', function (Blueprint $table) {
            //
        });
    }
}
