<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPickedColumnToDetailPaketKonsolidasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_paket_konsolidasis', function (Blueprint $table) {
            $table->boolean('picked')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_paket_konsolidasis', function (Blueprint $table) {
            $table->dropColumn('picked');
        });
    }
}
