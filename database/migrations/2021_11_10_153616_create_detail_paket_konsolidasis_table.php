<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailPaketKonsolidasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_paket_konsolidasis', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('paket_konsolidasi_id');
            $table->unsignedBigInteger('id_paket')->nullable();
            $table->string('nama_paket_asli');
            $table->string('pagu_anggaran')->nullable();
            $table->unsignedBigInteger('id_sirup')->nullable();
            $table->string('nama_satker')->nullable();
            $table->string('metode_pengadaan')->nullable();
            $table->string('jenis_pengadaan')->nullable();
            $table->string('kode_akun')->nullable();
            //$table->string('ppk')->nullable();
            $table->string('sumber_dana')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_paket_konsolidasis');
    }
}
