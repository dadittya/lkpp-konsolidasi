<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Nullable;

class CreateRupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rups', function (Blueprint $table) {
            $table->bigInteger('kode_rup')->index();
            $table->string('nama_paket', 3000);
            $table->string('tahun_anggaran');
            $table->char('instansi_id', 6);
            $table->char('satker_id', 8)->nullable();
            $table->char('provinsi_id', 2)->nullable();
            $table->char('kabupaten_id', 6)->nullable();
            $table->string('detail_lokasi')->nullable();
            $table->string('volume')->nullable();
            $table->string('uraian_pekerjaan')->nullable();
            $table->string('spesifikasi')->nullable();
            $table->string('sumber_dana')->nullable();
            $table->string('mak')->nullable();
            $table->string('nip_ppk')->nullable();
            $table->string('tahun_anggaran_dana')->nullable();
            $table->bigInteger('total_pagu_paket');
            $table->string('pagu_tahun_anggaran')->nullable();
            $table->unsignedBigInteger('jenis_pengadaan_id')->nullable();
            $table->unsignedBigInteger('metode_pengadaan_id')->nullable();
            $table->string('keterangan_pdn')->nullable();;
            $table->string('keterangan_usaha_kecil')->nullable();
            $table->string('keterangan_pradipa')->nullable();
            $table->string('keterangan_konsolidasi')->nullable();
            $table->date('tanggal_akhir_pemanfaatan')->nullable();
            $table->date('tanggal_awal_pemanfaatan')->nullable();
            $table->date('tanggal_akhir_kontrak')->nullable();
            $table->date('tanggal_awal_kontrak')->nullable();
            $table->date('tanggal_akhir_pemilihan')->nullable();
            $table->date('tanggal_awal_pemilihan')->nullable();
            $table->date('tanggal_diperbaharui')->nullable();
            $table->date('tanggal_rup_dibuat')->nullable();
            $table->string('asal_dana')->nullable();
            $table->string('tipe_paket')->nullable();
            $table->string('id_rup_client')->nullable();
            $table->softDeletes();
            $table->timestamps();

            
            $table->foreign('jenis_pengadaan_id')->references('id')->on('jenis_pengadaan');
            $table->foreign('metode_pengadaan_id')->references('id')->on('metode_pengadaan');
            $table->foreign('provinsi_id')->references('id')->on('provinsis');
            $table->foreign('kabupaten_id')->references('id')->on('kabupatens');
            $table->foreign('satker_id')->references('id')->on('satkers');
            $table->foreign('instansi_id')->references('id')->on('instansis');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rups');
    }
}
