<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddActiveColumnsToRupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rups', function (Blueprint $table) {
            $table->boolean('is_active')->default(1);
            $table->boolean('is_swakelola')->default(0);
            $table->boolean('is_kegiatan')->default(0);
            $table->boolean('is_deleted')->default(0);  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('rups', function (Blueprint $table) {
            //
        });
    }
}
