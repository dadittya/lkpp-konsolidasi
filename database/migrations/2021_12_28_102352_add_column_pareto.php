<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnPareto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('paket_konsolidasis', function (Blueprint $table) {
            $table->string('total_pagu_pareto')->nullable()->after('nama_paket_konsolidasi');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paket_konsolidasis', function (Blueprint $table) {
            $table->dropColumn('total_pagu_pareto');
        });
    }
}
