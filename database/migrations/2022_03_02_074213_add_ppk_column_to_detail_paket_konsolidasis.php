<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPpkColumnToDetailPaketKonsolidasis extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('detail_paket_konsolidasis', function (Blueprint $table) {
            $table->string('ppk')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detail_paket_konsolidasis', function (Blueprint $table) {
            $table->dropColumn('ppk');
        });
    }
}
