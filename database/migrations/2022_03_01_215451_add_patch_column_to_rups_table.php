<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPatchColumnToRupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('instansis', function (Blueprint $table) {
            $table->boolean('need_patch')->default(0);
            $table->dateTime('last_patch')->nullable();
            $table->integer('patch_time')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('instansis', function (Blueprint $table) {
            $table->dropColumn('need_patch');
            $table->dropColumn('last_patch');
            $table->dropColumn('patch_time');
        });
    }
}
