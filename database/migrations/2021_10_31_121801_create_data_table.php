<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('kode_rup');
            $table->string('nama_paket');
            $table->string('tahun_anggaran');
            $table->string('jenis_klpd');
            $table->string('nama_kementrian_lembaga');
            $table->string('nama_satuan_kerja');
            $table->string('provinsi');
            $table->string('kabupaten_kota');
            $table->string('detail_lokasi');
            $table->string('volume');
            $table->string('uraian_pekerjaan');
            $table->string('spesifikasi');
            $table->string('sumber_dana');
            $table->string('mak');
            $table->string('tahun_anggaran_dana');
            $table->bigInteger('total_pagu_paket');
            $table->string('pagu_tahun_anggaran');
            $table->string('jenis_pengadaan');
            $table->string('metode_pengadaan');
            $table->string('keterangan_pdn');
            $table->string('keterangan_usaha_kecil');
            $table->string('keterangan_pradipa');
            $table->string('keterangan_konsolidasi');
            $table->date('tanggal_akhir_pemanfaatan');
            $table->date('tanggal_awal_pemanfaatan');
            $table->date('tanggal_akhir_kontrak');
            $table->date('tanggal_awal_kontrak');
            $table->date('tanggal_akhir_pemilihan');
            $table->date('tanggal_awal_pemilihan');
            $table->date('tanggal_diperbaharui');
            $table->date('tanggal_rup_dibuat');
            $table->string('asal_dana');
            $table->string('tipe_paket');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
}
