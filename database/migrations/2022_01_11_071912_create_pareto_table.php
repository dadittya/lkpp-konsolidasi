<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParetoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pareto', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('id_paket');
            $table->string('id_header')->nullable();
            $table->string('nama_paket');
            $table->bigInteger('pagu_anggaran');
            $table->bigInteger('kumulatif_pagu');
            $table->string('persentase_terhadap_total');
            $table->string('klasifikasi_anggaran');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pareto');
    }
}
