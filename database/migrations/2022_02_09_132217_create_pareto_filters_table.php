<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateParetoFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pareto_filters', function (Blueprint $table) {
            $table->id();
            $table->string('nama_kementrian_lembaga')->nullable();
            $table->string('nama_satuan_kerja')->nullable();
            $table->string('jenis_pengadaan')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('pemkot')->nullable();
            $table->string('tahun');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pareto_filters');
    }
}
