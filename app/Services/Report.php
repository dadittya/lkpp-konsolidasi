<?php

namespace App\Services;

use App\Models\Data;
use App\Models\Filter;
use App\Models\Instansi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Models\PaketKonsolidasi;

class Report
{

    public function dtable($request)
    {

        $data = PaketKonsolidasi::where('checked', true)
                ->wherehas('user', function($q){
                    $q->where('user_id', auth()->id());
                })->first();
        $tahun = $data->detailPaketKonsolidasi->first()->rup->tahun_anggaran;
        $instansiID = $data->detailPaketKonsolidasi->first()->rup->instansi_id;
        //$klpd = $uip->nama_kementrian_lembaga;

        $pm = Data::select(DB::raw('COUNT(nama_paket) as paket'))
            ->addSelect('instansi_id', DB::raw('SUM(total_pagu_paket) as pagu'))
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun)
            // ->where('nama_satuan_kerja', $satker)
            ->penyedia()
            ->active()
            ->groupBy('instansi_id');

        //dd($data->detailPaketKonsolidasi->first()->rup);
        $sw = Data::select('instansi_id', DB::raw('COUNT(nama_paket) as paket'))
            ->addSelect('instansi_id', DB::raw('SUM(total_pagu_paket) as pagu'))
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun)
            // ->where('nama_satuan_kerja', $satker)
            ->swakelola()
            ->active()
            ->groupBy('instansi_id');

        $pds = Data::select('instansi_id', DB::raw('COUNT(nama_paket) as paket'))
        ->active()
            ->addSelect('instansi_id', DB::raw('SUM(total_pagu_paket) as pagu'))
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun)
            // ->where('nama_satuan_kerja', $satker)
            ->penyediaSwakelola()
            ->groupBy('instansi_id');

        // if ($satker !== '') {
        //     $pm->where('satker_id', $satker);
        //     $sw->where('satker_id', $satker);
        //     $pds->where('satker_id', $satker);
        // }

        $pm = $pm->get();
        $sw = $sw->get();
        $pds = $pds->get();
        
        $dtpm = [];
        foreach ($pm as $key => $value) {
            //dd($value);
            $KLName = Instansi::find($instansiID)->nama;
            $dtpm[] = [
                'tipe' => 'Penyedia',
                'klpd' => $KLName,
                'paket' => $value->paket,
                'pagu' => $value->pagu
            ];
        }

        $dtsw = [];
        foreach ($sw as $key => $value) {
            $KLName = Instansi::find($instansiID)->nama;
            $dtsw[] = [
                'tipe' => 'Swakelola',
                'klpd' => $KLName,
                'paket' => $value->paket,
                'pagu' => $value->pagu
            ];
        }

        $dtpds = [];
        foreach ($pds as $key => $value) {
            $KLName = Instansi::find($instansiID)->nama;
            $dtpds[] = [
                'tipe' => 'Penyedia dalam Swakelola',
                'klpd' => $KLName,
                'paket' => $value->paket,
                'pagu' => $value->pagu
            ];
        }

        $pm = [];
        foreach ($dtpm as $key => $value) {
            //dd($value);
            //$KLName = Instansi::find($value->instansi_id)->nama;
            $pm[] = [
                'tipe' => $value['tipe'],
                'klpd' => $value['klpd'],
                'paket' => $value['paket'],
                'pagu' => $value['pagu']
            ];
        }

        $swa = [];
        foreach ($dtsw as $key => $value) {
            //$KLName = Instansi::find($value->instansi_id)->nama;
            $swa[] = [
                'tipe' => $value['tipe'],
                'klpd' => $value['klpd'],
                'paket' => $value['paket'],
                'pagu' => $value['pagu']
            ];
        }

        $pdsw = [];
        foreach ($dtpds as $key => $value) {
            //$KLName = Instansi::find($value->instansi_id)->nama;
            $pdsw[] = [
                'tipe' => $value['tipe'],
                'klpd' => $value['klpd'],
                'paket' => $value['paket'],
                'pagu' => $value['pagu']
            ];
        }
        // return $pdsw;

        $res = [];
        //foreach ($klpd as $key => $value) {
            $KL = Instansi::find($instansiID);
            $KLName = $KL->nama;
            $KLUpdate = date_format($KL->last_update, 'd-m-Y H:i:s');
            // $res[] = $key;
            if (empty($pm[$key])) {

                $pm[] = [
                    'tipe' => 'Penyedia Murni',
                    'klpd' => $KLName,
                    'paket' => 0,
                    'pagu' => 0
                ];
            }

            if (empty($swa[$key])) {
                
                $swa[] = [
                    'tipe' => 'Swakelola',
                    'klpd' => $KLName,
                    'paket' => 0,
                    'pagu' => 0
                ];
            }

            if (empty($pdsw[$key])) {
                
                $pdsw[] = [
                    'tipe' => 'Penyedia dalam Swakelola',
                    'klpd' => $KLName,
                    'paket' => 0,
                    'pagu' => 0
                ];
            }

            $res[] = [
                'klpd' => $KLName,
                'klpd_update' => $KLUpdate,
                'pm' => $pm[$key],
                'sw' => $swa[$key],
                'pds' => $pdsw[$key]
            ];
        //}

        return $res;
    }
}
