<?php

namespace App\Jobs;

use App\Models\Data;
use App\Models\Pareto;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class paretoJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $res;
    protected $total;
    protected $filterId;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($res, $total, $filterId)
    {
        
        $this->res = $res;
        $this->total = $total;
        $this->filterId = $filterId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
        $filterId = $this->filterId;
        $total = $this->total;
        $res = $this->res;
        //$totalPareto = Pareto::where('id_header', $filterId)->count();
        //$totalData = count($res);

        //if ($totalPareto != $totalData) {
            $arr = [];
            foreach ($res as $key => $value) {

                if ($key === 0) {
                    $count = ($value['total_pagu_paket'] * 100) / $total;
                    $percent = round($count, 2) . '%';
                    $arr[] = [
                        'id' => $value['kode_rup'],
                        'nama_satuan_kerja' => $value['satker_id'],
                        'nama_paket' => $value['nama_paket'],
                        'total_pagu_paket' => $value['total_pagu_paket'],
                        'kumulatif_pagu' => $value['total_pagu_paket'],
                        'persentase_terhadap_total' => $percent,
                        'klasifikasi_anggaran' => $percent < 80 ? 'BESAR' : 'KECIL',
                        'tahun_anggaran' => $value['tahun_anggaran'],
                        'metode_pengadaan' => $value['metode_pengadaan_id'],
                        'sumber_dana' => $value['sumber_dana'],
                        'kode_rup' => $value['kode_rup'],
                        'mak' => $value['mak'],
                        'jenis_pengadaan' => $value['jenis_pengadaan_id'],
                        'tanggal_awal_pemilihan' => $value['tanggal_awal_pemilihan'],
                    ];
                }

                if ($key > 0) {
                    $kumulatif = $arr[$key - 1];
                    $result = $value['total_pagu_paket'] + $kumulatif['kumulatif_pagu'];

                    $count = ($result * 100) / $total;
                    $percent = round($count, 2) . '%';
                    $arr[] = [
                        'id' => $value['kode_rup'],
                        'nama_satuan_kerja' => $value['satker_id'],
                        'nama_paket' => $value['nama_paket'],
                        'total_pagu_paket' => $value['total_pagu_paket'],
                        'kumulatif_pagu' => $result,
                        'persentase_terhadap_total' => $percent,
                        'klasifikasi_anggaran' => $percent < 80 ? 'BESAR' : 'KECIL',
                        'tahun_anggaran' => $value['tahun_anggaran'],
                        'metode_pengadaan' => $value['metode_pengadaan_id'],
                        'sumber_dana' => $value['sumber_dana'],
                        'kode_rup' => $value['kode_rup'],
                        'mak' => $value['mak'],
                        'jenis_pengadaan' => $value['jenis_pengadaan_id'],
                        'tanggal_awal_pemilihan' => $value['tanggal_awal_pemilihan'],
                    ];
                };
            }

            // DB::beginTransaction();
            try {
                $count_besar = count(array_filter($arr, function ($element) {
                    return $element['klasifikasi_anggaran'] == 'BESAR';
                }));
                $count_kecil = count(array_filter($arr, function ($element) {
                    return $element['klasifikasi_anggaran'] == 'KECIL';
                }));

                // foreach ($arr as $key => $value) {
                //     // DetailPaketKonsolidasi::create([
                //     //     'paket_konsolidasi_id' => $paket_konsolidasi->id,
                //     //     'id_paket' => $value['id'],
                //     //     'nama_paket_asli' => $value['nama_paket'],
                //     //     'pagu_anggaran' => $value['total_pagu_paket'],
                //     //     'kumulatif_pagu' => $value['kumulatif_pagu'],
                //     //     'persentase_terhadap_total' => $value['persentase_terhadap_total'],
                //     //     'klasifikasi_anggaran' => $value['klasifikasi_anggaran'],
                //     //     'id_sirup' => $value['kode_rup'],
                //     //     'nama_satker' => $value['nama_satuan_kerja'],
                //     //     'metode_pengadaan' => $value['metode_pengadaan'],
                //     //     'jenis_pengadaan' => $value['jenis_pengadaan'],
                //     //     'sumber_dana' => $value['sumber_dana'],
                //     //     'kode_akun' => $value['mak']
                //     // ]);

                //     Pareto::updateOrCreate([
                //         'id_paket' => $value['id'],
                //         'id_header' => $filterId,
                //     ],[
                //         'id_paket' => $value['id'],
                //         'id_header' => $filterId,
                //         //'nama_paket' => $value['nama_paket'],
                //         'pagu_anggaran' => $value['total_pagu_paket'],
                //         'kumulatif_pagu' => $value['kumulatif_pagu'],
                //         'persentase_terhadap_total' => $value['persentase_terhadap_total'],
                //         'klasifikasi_anggaran' => $value['klasifikasi_anggaran']
                //     ]);
                // }

                // DB::commit();
            } catch (\Exception $e) {
                // DB::rollBack();
                Log::info($e->getMessage());

                return response()->json(['success' => false, 'message' => 'Ups Terjadi Kesalahan!', 'err' => $e->getMessage()]);
            }
        //}
        
    }
}
