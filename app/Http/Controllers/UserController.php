<?php

namespace App\Http\Controllers;

use App\Models\User;
use GrahamCampbell\ResultType\Success;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function __construct()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function datatableUser()
    {
        $model = User::query();
        return DataTables::of($model)->addIndexColumn()
            ->addColumn('aksi', function($d){
                
                if($d->email_verified_at == '') {
                    $btn = '<div class="d-flex">
                        <form method="post" action="'.route('admin.user.verified', $d->id).'" class="form-activate">
                            '.csrf_field().'
                            <input type="hidden" name="_method" value="post" />';

                    $btn = $btn . '<button type="submit" class="btn btn-sm btn-success m-1"><i class="fa fa-check"></i></button>
                    </form>
                    </div>';
                }
                else {
                    $btn = '<div class="d-flex">
                    </div>';
                }
                            
                return $btn;
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }
    
    public function verified($id)
    {
        $user = User::find($id);
        
        if ($user->email_verified_at == '') {
            $user->email_verified_at = now();
            $user->save();
        }

        return redirect(route('admin.user.index'));
    }
}
