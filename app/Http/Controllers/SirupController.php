<?php

namespace App\Http\Controllers;

use App\Console\Commands\getRup;
use App\Models\Data;
use App\Models\ErrorLog;
use Illuminate\Http\Request;
use App\Models\Provinsi;
use App\Models\Kabupaten;
use App\Models\Instansi;
use App\Models\JenisPengadaan;
use App\Models\MetodePengadaan;
use App\Models\Rup;
use App\Models\Satker;
use App\Models\SyncLog;
use App\Models\Tahun;
use Error;
use Throwable;

class SirupController extends Controller
{
    public static function syncProvince() {
        $provinces = HelperController::getProvince();

        $data = json_decode($provinces->body());
        
        foreach ($data as $key => $value) {
            (new Provinsi())
                ->updateOrCreate([
                    'id' => $value->prp_id
                ],
                [
                    'nama' => $value->prov_nama
                ]);
        }
    }

    public static function syncKabupaten() {
        $kabupaten = HelperController::getKabupaten();

        $data = json_decode($kabupaten->body());
        
        foreach ($data as $key => $value) {
            if ($value->nama_kabupaten) {
                (new Kabupaten())
                    ->updateOrCreate([
                        'id' => $value->kd_kabupaten
                    ],
                    [
                        'nama' => $value->nama_kabupaten,
                        'provinsi_id' => $value->kd_propinsi
                    ]);
            }
        }
    }

    public static function getInstansi() {
        $instansi = HelperController::getInstansi();

        $data = json_decode($instansi->body());
        
        foreach ($data as $key => $value) {
            echo $value->kd_klpd . "\n";
            if ($value->nama_klpd) {
                (new Instansi())
                    ->updateOrCreate([
                        'id' => $value->kd_klpd
                    ],
                    [
                        'nama' => $value->nama_klpd,
                        'jenis' => $value->jenis_klpd,
                        'alamat' => $value->alamat_klpd,
                        'provinsi_id' => $value->kd_provinsi,
                        'kabupaten_id' => $value->kd_kabupaten
                    ]);
            }
        }
    }

    public static function getSatker($year, $instansiId) {
        $satker = HelperController::getSatker($year, $instansiId);

        $data = json_decode($satker->body());

        if ($data) {
            echo $instansiId . "\n";
            $satker = Satker::where('instansi_id', $instansiId)
                        ->update(['show' => 0]);
            
            foreach ($data as $key => $value) {
                if ($value->nama_satker) {
                    echo "\t" . $value->kd_satker . "\n";
                    (new Satker())
                        ->updateOrCreate([
                            'id' => $value->kd_satker
                        ],
                        [
                            'id_str' => $value->kd_satker_str,
                            'nama' => $value->nama_satker,
                            'jenis' => $value->jenis_satker,
                            'status' => $value->status_satker == 'Aktif' ? 1 : 0,
                            'keterangan' => $value->ket_satker,
                            'instansi_id' => $value->kd_klpd,
                            'status_satker' => $value->status_satker,
                            'show' => $value->status_satker == 'Aktif' ? 1 : 0
                        ]);
                }
            }
        }
    }

    public static function getRup($year, $instansiId, $time = 'present') {

        //clear log
        SyncLog::whereDate( 'created_at', '<=', now()->subDays(30))->delete();
        ErrorLog::whereDate( 'created_at', '<=', now()->subDays(30))->delete();

        $instansi = Instansi::find($instansiId);
        $namapaket = '';
        ini_set('memory_limit', '-1');

        if ($instansi) {
            $instansi->last_status = 0;
            $instansi->last_update = now();
            //$instansi->save();

            $startDate = now();
            $syncLog = new SyncLog();
            $syncLog->klpd = $instansiId;
            $syncLog->start_date = $startDate;
            $syncLog->save();

                try {
                $rup = HelperController::getRupDetail($year, $instansiId);
                $data = null;

                if ($rup->body() != null) {
                    if (SirupController::validJson($rup->body())) {
                        $data = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $rup->body()));
                    }
                }
                if ($data) {
                    
                    //dd('foo');
                    foreach ($data as $key => $value) {
                        try {
                        if (isset($value->idrup)) {
                            echo "\t Processing Penyedia " . $value->idrup . "\n";
                            $idProvinsi = null;
                            $idKabupaten = null;
                            
                            if ($value->lokasi_prov) {
                                $provinsi = Provinsi::where('nama', $value->lokasi_prov)
                                                    ->first();
                                if($provinsi) {
                                    $idProvinsi = $provinsi->id;
                                }
                            }

                            if ($value->lokasi_kab) {
                                $kabupaten = Kabupaten::where('nama', $value->lokasi_kab)
                                                    ->first();
                                if($kabupaten) {
                                    $idKabupaten = $kabupaten->id;
                                }
                            }
                            $satkerId = null;
                            $kodeSatker = $value->kodesatker;
                            $idSatker = $value->idsatker;
                            if ($value->namasatker) {
                                    $satker = Satker::where('instansi_id', $instansiId)
                                                    ->where(function($query) use ($kodeSatker,$idSatker){
                                                        $query->where('id_str', $kodeSatker);
                                                        $query->orwhere('id', $idSatker);
                                                    })
                                                ->first();

                                if ($satker) {
                                    $satkerId = $satker->id;
                                }
                                else {
                                    $satker = Satker::where('nama', $value->namasatker)
                                                ->where('instansi_id', $instansiId)
                                                ->first();
                                    if ($satker) {
                                        $satkerId = $satker->id;
                                    }
                                    else {
                                        $satkerId = null;
                                    }
                                }
                            }

                            $jenisId = null;
                            if (($value->jenispengadaan) && ($value->jenispengadaan != "-") && ($value->idjenispengadaan > 0)) {
                                
                                $arrayName = explode(';',$value->jenispengadaan);
                                $arrayId = explode(';',$value->idjenispengadaan);

                                $i = 0;
                                foreach($arrayName as $key => $item) {
                                    $jenis = JenisPengadaan::where('name', $item)
                                                            ->first();   
                                    if (!$jenis) {
                                        $jenis = new JenisPengadaan();
                                        $jenis->id = $arrayId[$i];
                                        $jenis->name = $item;
                                        $jenis->save();
                                    }
                                    $jenisId = $jenis->id;
                                    $i++;
                                }
                            }
                            else {
                                $jenis = JenisPengadaan::where('name', '-')
                                                            ->first();   
                                    if (!$jenis) {
                                        $jenis = new JenisPengadaan();
                                        $jenis->id = 999;
                                        $jenis->name = '-';
                                        $jenis->save();
                                    }
                                    $jenisId = $jenis->id;
                            }


                            $metodeId = null;
                            if (($value->metodepengadaan) && ($value->metodepengadaan != "-") && ($value->idmetodepengadaan > 0)) {
                                $metode = MetodePengadaan::where('nama', $value->metodepengadaan)
                                                        ->first();
                                if (!$metode) {
                                    $metode = new MetodePengadaan();
                                    $metode->id = $value->idmetodepengadaan;
                                    $metode->nama = $value->metodepengadaan;
                                    $metode->save();
                                }
                                    
                                $metodeId = $metode->id;
                            }
                            else {
                                $metode = MetodePengadaan::where('nama', '-')
                                                        ->first();
                                if (!$metode) {
                                    $metode = new MetodePengadaan();
                                    $metode->id = 999;
                                    $metode->nama = '-';
                                    $metode->save();
                                }
                                    
                                $metodeId = $metode->id;
                            }
                        
                            if ($value->namapaket != '') {
                                $namapaket = trim(preg_replace('/\s+/', ' ', $value->namapaket));
                            } 
                            (new Rup())
                                ->updateOrCreate([
                                    'kode_rup' => $value->idrup
                                ],
                                [
                                    'nama_paket' => substr($namapaket, 0, 4200),
                                    'tahun_anggaran' => $value->tahunanggaran,
                                    'instansi_id' => $value->kd_klpd,
                                    'satker_id' => $satkerId,
                                    'provinsi_id' => $idProvinsi,
                                    'kabupaten_id' => $idKabupaten,
                                    'nip_ppk' => $value->ppk,
                                    'total_pagu_paket' => $value->jumlahpagu,
                                    'jenis_pengadaan_id' => $jenisId,
                                    'metode_pengadaan_id' => $metodeId,
                                    'id_rup_client' => $value->id_rup_client,
                                    'tanggal_awal_pemilihan' => $value->tanggalawalpemilihan,
                                    'is_swakelola' =>false,
                                    'id_swakelola' => $value->id_swakelola,
                                    'is_active' => $value->statusaktifpaket,
                                    'is_deleted' => $value->statusdeletepaket,
                                    'status_umumkan' => $value->statusumumkan,
                                    'statususahakecil' => $value->statususahakecil ?? null
                                ]);

                            }
                        } catch (Throwable $t) {
                            // you may want to add some logging here...
                            $errorLog = new ErrorLog();
                            $errorLog->klpd = $instansiId;
                            $errorLog->tahun = $year;
                            $errorLog->message = $t->getMessage();
                            $errorLog->save();
                            continue;
                        }
                    }

                    if ($time == 'present') {
                        $instansi->last_status = 1;
                        $instansi->last_update = now();
                        $instansi->need_patch = 1;
                        $diffInSeconds = now()->diffInSeconds($startDate);
                        $instansi->last_time_execution = $diffInSeconds;
                    }
                    else if ($time == 'past') {
                        $instansi->past_status = 1;
                    }
                    else if ($time == 'future') {
                        $instansi->future_status = 1;
                    }
                    
                    $instansi->save();
                }
            } catch (Throwable $t) {
                // you may want to add some logging here...
                $errorLog = new ErrorLog();
                $errorLog->klpd = $instansiId;
                $errorLog->tahun = $year;
                $errorLog->message = $t->getMessage();
                $errorLog->save();
            }
            //$instansi->last_status = 0;
            // $instansi->last_update = now();
            // $instansi->need_patch = 0;
            // $instansi->save();

            $rup = null;
            $data = null;
            try {
                $rup = HelperController::getRupSwakelola($year, $instansiId);
                //dd(SirupController::validJson($rup->body()));

                $data = null;

                if ($rup->body() != null) {
                    if (SirupController::validJson($rup->body())) {
                        $data = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $rup->body()));
                    }
                }
                
                if ($data) {
                    
                    //dd($rup->body());
                    foreach ($data as $key => $value) {
                        try{
                            echo "\t Processing Swakelola " . $value->idrup . "\n";
                            $idProvinsi = null;
                            $idKabupaten = null;
                            
                            if ($value->lokasi_prov) {
                                $provinsi = Provinsi::where('nama', $value->lokasi_prov)
                                                    ->first();
                                if($provinsi) {
                                    $idProvinsi = $provinsi->id;
                                }
                            }

                            if ($value->lokasi_kab) {
                                $kabupaten = Kabupaten::where('nama', $value->lokasi_kab)
                                                    ->first();
                                if($kabupaten) {
                                    $idKabupaten = $kabupaten->id;
                                }
                            }
                            $satkerId = null;
                            if ($value->namasatker) {
                                $satker = Satker::where('nama', $value->namasatker)
                                                ->where('instansi_id', $instansiId)
                                                ->first();

                                if ($satker) {
                                    $satkerId = $satker->id;
                                }
                                else {
                                    $satker = Satker::where('id_str', $value->kodesatker)
                                                ->orwhere('id', $value->idsatker)
                                                ->first();
                                    if ($satker) {
                                        $satkerId = $satker->id;
                                    }
                                    else {
                                        $satkerId = null;
                                    }
                                }
                            }

                            // $jenisId = null;
                            // if (($value->jenispengadaan) && ($value->jenispengadaan != "-") && ($value->idjenispengadaan > 0)) {
                                
                            //     $arrayName = explode(';',$value->jenispengadaan);
                            //     $arrayId = explode(';',$value->idjenispengadaan);

                            //     $i = 0;
                            //     foreach($arrayName as $key => $item) {
                            //         $jenis = JenisPengadaan::where('name', $item)
                            //                                 ->first();   
                            //         if (!$jenis) {
                            //             $jenis = new JenisPengadaan();
                            //             $jenis->id = $arrayId[$i];
                            //             $jenis->name = $item;
                            //             $jenis->save();
                            //         }
                            //         $jenisId = $jenis->id;
                            //         $i++;
                            //     }
                            // }


                            // $metodeId = null;
                            // if (($value->metodepengadaan) && ($value->metodepengadaan != "-") && ($value->idmetodepengadaan > 0)) {
                            //     $metode = MetodePengadaan::where('nama', $value->metodepengadaan)
                            //                             ->first();
                            //     if (!$metode) {
                            //         $metode = new MetodePengadaan();
                            //         $metode->id = $value->idmetodepengadaan;
                            //         $metode->nama = $value->metodepengadaan;
                            //         $metode->save();
                            //     }
                                    
                            //     $metodeId = $metode->id;
                            // }
                            if ($value->namapaket != '') {
                                $namapaket = trim(preg_replace('/\s+/', ' ', $value->namapaket));
                            } 

                            (new Rup())
                                ->updateOrCreate([
                                    'kode_rup' => $value->idrup
                                ],
                                [
                                    'nama_paket' => substr($namapaket, 0, 4290),
                                    'tahun_anggaran' => $value->tahunanggaran,
                                    'instansi_id' => $value->kd_klpd,
                                    'satker_id' => $satkerId,
                                    'provinsi_id' => $idProvinsi,
                                    'kabupaten_id' => $idKabupaten,
                                    'nip_ppk' => $value->ppk,
                                    'total_pagu_paket' => $value->jumlahpagu,
                                    //'jenis_pengadaan_id' => $jenisId,
                                    //'metode_pengadaan_id' => $metodeId,
                                    'id_swakelola' => null,
                                    'id_rup_client' => $value->id_rup_client,
                                    //'tanggal_awal_pemilihan' => $value->tanggalawalpemilihan,
                                    'is_active' => $value->statusaktifpaket,
                                    'is_swakelola' => true,
                                    'is_deleted' => $value->statusdeletepaket,
                                    'status_umumkan' => $value->statusumumkan,
                                    'statususahakecil' => $value->statususahakecil ?? null
                                ]);
                            } catch (Throwable $t) {
                                // you may want to add some logging here...
                                
                                $errorLog = new ErrorLog();
                                $errorLog->klpd = $instansiId;
                                $errorLog->tahun = $year;
                                $errorLog->message = $t->getMessage();
                                $errorLog->save();
                                continue;
                            }
                    }

                    if ($time == 'present') {
                        $instansi->last_status = 1;
                        $instansi->last_update = now();
                        $instansi->need_patch = 1;
                        $diffInSeconds = now()->diffInSeconds($startDate);
                        $instansi->last_time_execution = $diffInSeconds;
                        $instansi->save();
                    }
                    else if ($time == 'past') {
                        $instansi->past_status = 1;
                    }
                    else if ($time == 'future') {
                        $instansi->future_status = 1;
                    }
                }
                
                $instansi->last_update = now(); 
                $instansi->need_patch = 0;
                $instansi->save();
                $syncLog->end_date = now();
                $syncLog->save();

                $instansi = null;
                $rup = null;
                $data = null;
                $value = null;
                $provinsi = null;
                $kabupaten = null;
                $satker = null;
                $syncLog = null;
            } catch (Throwable $t) {
                // you may want to add some logging here...
                $errorLog = new ErrorLog();
                $errorLog->klpd = $instansiId;
                $errorLog->tahun = $year;
                $errorLog->message = $t->getMessage();
                $errorLog->save();
            }

        }

    }

    public static function getAllRup() {
        $klpd = Instansi::orderby('last_status')
                    ->orderby('last_update')
                    ->get();
        // $klpd = Instansi::where('last_status', 0)
        //             ->orderby('last_update')
        //             ->get();

        foreach ($klpd as $val) {
            echo $val->id . "\n";
            
            $year = date("Y");
            SirupController::getRup($year, $val->id);
            
            if (time() > strtotime('6 am')) {
                exit;
            }
        }
    }

    public static function getPastRup() {
        if (date("n") < 8) {
            $klpd = Instansi::orderby('past_status')
                        ->orderby('last_update')
                        ->get();
            // $klpd = Instansi::where('last_status', 0)
            //             ->orderby('last_update')
            //             ->get();

            foreach ($klpd as $val) {
                echo $val->id . "\n";
                
                $year = date("Y") - 1;
                
                SirupController::getRup($year, $val->id, 'past');
                
                if (time() > strtotime('6 am')) {
                    exit;
                }
            }
        }
    }

    public static function getFutureRup() {
        if (date("n") > 7) {
            $klpd = Instansi::orderby('future_status')
                        ->orderby('last_update')
                        ->get();
            // $klpd = Instansi::where('last_status', 0)
            //             ->orderby('last_update')
            //             ->get();

            foreach ($klpd as $val) {
                echo $val->id . "\n";
                
                $year = date("Y") + 1;
                break;
                SirupController::getRup($year, $val->id, 'future');
                
                
                if (time() > strtotime('6 am')) {
                    $rup = Rup::where('tahun_anggaran', $year)->first();
            
                    if ($rup) {
                        $tahun = Tahun::updateorcreate([
                            'nama' => $year
                        ]);
                    }
                    exit;
                }
            }
            $rup = Rup::where('tahun_anggaran', $year)->first();
            
            if ($rup) {
                $tahun = Tahun::updateorcreate([
                    'nama' => $year
                ]);
            }
        }
    }

    public static function patchAllRup() {
        $klpd = Instansi::where('need_patch', 1)
                    ->orderby('last_update')
                    ->get();
        // $klpd = Instansi::where('last_status', 0)
        //             ->orderby('last_update')
        //             ->get();

        foreach ($klpd as $val) {
            echo $val->id . "\n";
            
            $year = date("Y");
            SirupController::patchRup($year, $val->id);
            
            if (time() > strtotime('7 am')) {
                exit;
            }
        }
    }

    public static function patchRup($year, $instansiId) {
        $rup = HelperController::getRup($year, $instansiId); 

        $instansi = Instansi::find($instansiId);
        $data = json_decode($rup->body());

        if ($data) {
            $lastCode = "";
            foreach ($data as $key => $value) {
                if ($lastCode != $value->koderup) {
                    $data = Rup::where('kode_rup', $value->koderup)
                                    ->orWhere('id_rup_client', $value->id_rup_client)
                                    ->first();
                    
                    if ($data) {
                        echo "\t Updating " . $value->koderup . "\n";
                        $data->sumber_dana = $value->sumberdana;
                        $data->mak = $value->mak;

                        $data->save();
                    }
                    $lastCode = $value->koderup;
                }
            }
            $instansi->need_patch = 0;
        }
    }

    public static function validJson($str) {
        $json = json_decode($str);
        return $json && $str != $json;
    }
}
