<?php

namespace App\Http\Controllers;

use App\Models\Repository;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class RepositoryController extends Controller
{
    public function __construct()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.repository.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.repository.insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'url' => 'required'
        ]);

        $repository = new Repository();
        $repository->name = $request->name;
        $repository->description = $request->description;
        $repository->url = $request->url;;
        $repository->save();

        return redirect()->route('admin.repository.index')->withSuccess('Berhasil, Data berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $repository = Repository::find($id);
        return view('admin.repository.edit', compact('repository'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'url' => 'required'
        ]);

        $repository = Repository::find($id);
        $repository->name = $request->name;
        $repository->description = $request->description;
        $repository->url = $request->url;
        $repository->save();

        return redirect()->route('admin.repository.index')->withSuccess('Berhasil, Data berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $repository = Repository::find($id);
        $repository->delete();

        return redirect()->route('admin.repository.index')->withSuccess('Berhasil, Data berhasil dihapus!');
    }

    public function datatableRepository()
    {
        $model = Repository::query();
        return DataTables::of($model)->addIndexColumn()
            ->addColumn('aksi', function($d){
                $btn = '<div class="d-flex">
                        <a class="btn btn-sm btn-primary m-1" href="'. route('admin.repository.edit', ['id' => $d->id]) .'"><i class="fa fa-edit"></i></a>
                        <form method="post" action="'.route('admin.repository.destroy', $d->id).'" class="form-delete">
                            '.csrf_field().'
                            <input type="hidden" name="_method" value="delete" />
                            <button type="submit" class="btn btn-sm btn-danger m-1"><i class="fa fa-trash"></i></button>
                        </form>
                        </div>';
                return $btn;
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }
}
