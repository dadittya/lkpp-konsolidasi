<?php

namespace App\Http\Controllers;

use App\Models\Data;
use App\Models\DetailPaketKonsolidasi;
use App\Models\Filter;
use App\Models\Instansi;
use App\Models\JenisPengadaan;
use App\Models\KelompokKonsolidasiDetail;
use App\Models\MetodePengadaan;
use App\Models\PaketKonsolidasi;
use App\Models\Pareto;
use App\Models\Satker;
use App\Services\Report;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Spatie\Browsershot\Browsershot;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Crypt;

class ReportController extends Controller
{
    protected $reportServ;
    public function __construct(Report $reportServ)
    {
        $this->reportServ = $reportServ;
    }
    public function index(Request $request)
    {
        $e = $this->_caraPengadaan($request);
        $a = $this->_jenisPengadaan($request);
        $mp = $this->_metodePengadaan2($request);
        // dd($e);

        return view('web.pages.report.chart', compact('a', 'mp', 'e'));
    }

    public function getData()
    {
        $model = Data::take(10)->get();
        return response()->json(['data' => $model]);
    }

    

    public function _caraPengadaan($request)
    {
        $data = PaketKonsolidasi::where('checked', true)
                ->wherehas('user', function($q){
                            $q->where('user_id', auth()->id());
                        })->first();
        $tahun = $data->detailPaketKonsolidasi->first()->rup->tahun_anggaran;
        $instansiID = $data->detailPaketKonsolidasi->first()->rup->instansi_id;

        $pm = Data::select('nama_paket')
        ->active()
            ->penyedia()
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun);

        $tpp = Data::select('total_pagu_paket')
        ->active()
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun)
            ->penyedia();

        $sw = Data::select('nama_paket')
        ->active()
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun)
            ->swakelola();

        $tps = Data::select('total_pagu_paket')
        ->active()
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun)
            ->swakelola();

        $pds = Data::select('nama_paket')
        ->active()
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun)
            ->penyediaSwakelola();

        $tppds = Data::select('total_pagu_paket')
        ->active()
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun)
            ->penyediaSwakelola();

        $pm = $pm->count();
        $tpp = $tpp->sum('total_pagu_paket');
        $sw = $sw->count();
        $tps = $tps->sum('total_pagu_paket');
        $pds = $pds->count();
        $tppds = $tppds->sum('total_pagu_paket');


        $total_paket = $pm + $sw + $pds;
        $total_pagu = $tpp + $tps + $tppds;

        $data = [
            'items' => [
                [
                    'tipe_paket' => 'Penyedia',
                    'total_pagu' => $tpp,
                    'jumlah_paket' => $pm
                ],
                [
                    'tipe_paket' => 'Swakelola',
                    'total_pagu' => $tps,
                    'jumlah_paket' => $sw
                ],
                [
                    'tipe_paket' => 'Penyedia dalam Swakelola',
                    'total_pagu' => $tppds,
                    'jumlah_paket' => $pds
                ]
            ],
            'total' => [
                'tipe_paket' => 'Total',
                'total_pagu' => $total_pagu,
                'jumlah_paket' => $total_paket
            ],
        ];

        return $data;
    }

    public function anggaran_pengadaan(Request $request)
    {
        $e = $this->_caraPengadaan($request);
        // dd($e);
        $a = $this->_jenisPengadaan($request);
        $mp = $this->__metodePengadaan($request);
        //$paketKonsolidasi = $this->_paket_konsolidasi();
        // dd($paketKonsolidasi);
        $ipAddress = $request->ip();
        $uip = Filter::where('ip', auth()->id())->latest()->first();
        $s = $uip->nama_satuan_kerja;
        $klpd = $uip->nama_kementrian_lembaga;

        return view('web.pages.report.datatable1', compact('a', 'mp', 'e', 'uip', 'satker', 'klpd'));
    }

    public function ringkasan(Request $request)
    {
        $paketKonsolidasi = $this->_paket_konsolidasi();
        $e = $this->_caraPengadaan($request);
        //dd($e);
        $a = $this->_jenisPengadaan($request);
        $mp = $this->__metodePengadaan($request);
        // dd($paketKonsolidasi);
        $ipAddress = $request->ip();
        $uip = Filter::where('ip', auth()->id())->latest()->first();
        $klpd = str_replace("[","",$uip->nama_kementrian_lembaga);
        $data = PaketKonsolidasi::where('checked', true)
                                ->wherehas('user', function($q){
                                    $q->where('user_id', auth()->id());
                                })->first();
        //dd($data->detailPaketKonsolidasi->first()->rup);
        $klpdStr = $data->detailPaketKonsolidasi->first()->rup->instansi->nama;
        $tahunStr = $data->detailPaketKonsolidasi->first()->rup->tahun_anggaran;

        $selected = DetailPaketKonsolidasi::select('nama_satker')
                        ->wherehas('paketKonsolidasi', function($q) {
                                        $q->where('checked', 1);
                                    })
                        ->wherehas('paketKonsolidasi.user', function($q2){
                            $q2->where('user_id', auth()->id());
                        })
                        
                        ->distinct()
                        ->get();
        $satkerStr = null;
        if ($selected->count() <= 1) {
        foreach($selected as $item) {
                $satkerStr = $satkerStr . $item->nama_satker . ', ';
            }    
        }

        if($satkerStr) {
            $satkerStr = substr($satkerStr, 0, -2);
        }
        
        $selected = DetailPaketKonsolidasi::select('jenis_pengadaan')
                        ->wherehas('paketKonsolidasi', function($q) {
                                        $q->where('checked', 1);
                                    })
                        ->wherehas('paketKonsolidasi.user', function($q2){
                            $q2->where('user_id', auth()->id());
                        })

                        ->distinct()
                        ->get();

        $jenisPengadaanStr = null;
        if ($selected->count() <= 2) {
        foreach($selected as $item) {
                $jenisPengadaanStr = $jenisPengadaanStr . $item->jenis_pengadaan . ', ';
            }    
        }

        if($jenisPengadaanStr) {
            $jenisPengadaanStr = substr($jenisPengadaanStr, 0, -2);
        }
        
        $satker = str_replace("[","",$uip->nama_satuan_kerja);
        $jenisPengadaan = str_replace("[","",$uip->jenis_pengadaan);
        $metodePengadaan = str_replace("[","",$uip->metode_pengadaan);
        $dtSatker = null;
        if($satker) {
            $dtSatker = Satker::wherein('id', $satker)->get();
        }

        $dtJenisPengadaan = null;
        if($jenisPengadaan) {
            $dtJenisPengadaan = JenisPengadaan::whereIn('id', $jenisPengadaan)->get();
        }

        $res = $this->reportServ->dtable($request);

        return view('web.pages.report.ringkasan', compact('a', 'mp', 'e', 'uip', 'paketKonsolidasi', 'klpd', 'res', 'dtSatker', 'dtJenisPengadaan', 'klpdStr', 'satkerStr', 'jenisPengadaanStr', 'tahunStr'));
    }

    public function getRes(Request $request)
    {
        return $this->reportServ->dtable($request);
    }

    public function datatable(Request $request)
    {

        // return $this->reportServ->dtable($request);

        $ipAddress = $request->ip();
        $uip = Filter::where('ip', auth()->id())->latest()->first();
        // dd($ip->ip);
        $lembaga = json_decode($uip->nama_kementrian_lembaga);
        $provinsi = $uip->provinsi;
        $pemkot = $uip->pemkot;
        $jenis_pengadaan = $uip->jenis_pengadaan;
       
        $s = json_decode($uip->nama_satuan_kerja);
        $data = PaketKonsolidasi::where('checked', true)
                                ->wherehas('user', function($q){
                                    $q->where('user_id', auth()->id());
                                })->first();
        $tahun = $data->detailPaketKonsolidasi->first()->rup->tahun_anggaran;
        $instansiID = $data->detailPaketKonsolidasi->first()->rup->instansi_id;

        $pm = Data::select('nama_paket')
            ->where('nama_kementrian_lembaga', $instansiID)
            ->where('tahun_anggaran', $tahun)
            //->where('tipe_paket', 'like', "Penyedia Murni%");
            ->penyedia();

        $tpp = Data::select('total_pagu_paket')
            ->where('nama_kementrian_lembaga', $instansiID)
            ->where('tahun_anggaran', $tahun)
            //->where('tipe_paket', 'like', "Penyedia Murni%");
            ->penyedia();


        $sw = Data::select('nama_paket')
            ->where('nama_kementrian_lembaga', $instansiID)
            ->where('tahun_anggaran', $tahun)
            //->where('tipe_paket', 'like', "Swakelola%");
            ->swakelola();

        $tps = Data::select('total_pagu_paket')
            ->where('nama_kementrian_lembaga', $instansiID)
            ->where('tahun_anggaran', $tahun)
            //->where('tipe_paket', 'like', "Swakelola%");
            ->swakelola();

        $pds = Data::select('nama_paket')
            ->where('nama_kementrian_lembaga', $instansiID)
            ->where('tahun_anggaran', $tahun)
            //->where('tipe_paket', 'like', "%Penyedia Dalam Swakelola%");
            ->penyediaSwakelola();

        $tppds = Data::select('total_pagu_paket')
            ->where('nama_kementrian_lembaga', $instansiID)
            ->where('tahun_anggaran', $tahun)
            //->where('tipe_paket', 'like', "%Penyedia Dalam Swakelola%");
            ->penyediaSwakelola();

        // if ($satker !== '') {
        //     $pm->where('nama_satuan_kerja', $satker);
        //     $tpp->where('nama_satuan_kerja', $satker);
        //     $sw->where('nama_satuan_kerja', $satker);
        //     $tps->where('nama_satuan_kerja', $satker);
        //     $pds->where('nama_satuan_kerja', $satker);
        //     $tppds->where('nama_satuan_kerja', $satker);
        // }

        $pm = $pm->count();
        $tpp = $tpp->sum('total_pagu_paket');
        $sw = $sw->count();
        $tps = $tps->sum('total_pagu_paket');
        $pds = $pds->count();
        $tppds = $tppds->sum('total_pagu_paket');

        $total_paket = $pm + $sw + $pds;
        $total_pagu = $tpp + $tps + $tppds;

        return DataTables::of($data)
            ->addColumn('jumlah_paket_penyedia', function ($d) use ($pm) {
                return $pm;
            })

            ->addColumn('total_pagu_penyedia', function ($pp) use ($tpp) {
                return $tpp;
            })
            ->addColumn('jumlah_paket_swakelola', function ($d) use ($sw) {
                return $sw;
            })
            ->addColumn('total_pagu_swakelola', function ($d) use ($tps) {
                return $tps;
            })
            ->addColumn('jumlah_paket_pds', function ($d) use ($pds) {
                return $pds;
            })
            ->addColumn('total_pagu_pds', function ($d) use ($tppds) {
                return $tppds;
            })
            ->addColumn('total_paket', function ($d) use ($total_paket) {
                return $total_paket;
            })
            ->addColumn('total_pagu', function ($d) use ($total_pagu) {
                return $total_pagu;
            })
            ->addIndexColumn()
            ->rawColumns([
                'jumlah_paket_penyedia', 'total_pagu_penyedia',
                'jumlah_paket_penyedia', 'total_pagu_swakelola',
                'jumlah_paket_pds', 'total_pagu_pds',
                'total_paket', 'total_pagu'
            ])
            ->make(true);
    }

    public function anggaranTerbesar()
    {
        $model = Data::select('nama_paket', 'nama_satuan_kerja', 'total_pagu_paket')->distinct('nama_satuan_kerja')
            ->active()
            ->take(10)
            ->orderBy('total_pagu_paket', 'desc')
            ->get();
        // dd($model);

        return DataTables::of($model)
            ->addIndexColumn()
            ->make(true);
    }

    public function _jenisPengadaan($request)
    {
        $data = PaketKonsolidasi::where('checked', true)
                ->wherehas('user', function($q){
                            $q->where('user_id', auth()->id());
                        })->first();
        $tahun = $data->detailPaketKonsolidasi->first()->rup->tahun_anggaran;
        $instansiID = $data->detailPaketKonsolidasi->first()->rup->instansi_id;
        

        $modelPenyedia = Data::select('jenis_pengadaan_id', DB::raw('SUM(total_pagu_paket) as pagu'))
            ->addSelect('jenis_pengadaan_id', DB::raw('COUNT(nama_paket) as paket'))
            //->whereIn('jenis_pengadaan_id', $ajp)
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun)
            ->groupBy('jenis_pengadaan_id')
            ->penyedia()
            ->active()
            ->get();

        $modelPenyediaSwakelola = Data::select('jenis_pengadaan_id', DB::raw('SUM(total_pagu_paket) as pagu'))
            ->addSelect('jenis_pengadaan_id', DB::raw('COUNT(nama_paket) as paket'))
            //->whereIn('jenis_pengadaan_id', $ajp)
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun)
            ->groupBy('jenis_pengadaan_id')
            ->penyediaSwakelola()
            ->active()
            ->get();

        $pagu = [];
        $total = '';
        foreach ($modelPenyedia as $key => $value) {
            $jenisPengadaan = JenisPengadaan::find($value->jenis_pengadaan_id);
            $pagu[$value->jenis_pengadaan_id] = [
                'jenis_pengadaan' => $jenisPengadaan->name,
                'pagu' => $value->pagu,
                'jumlah_paket' => $value->paket
            ];
        }
        
        foreach ($modelPenyediaSwakelola as $key => $value) {
            $jenisPengadaan = JenisPengadaan::find($value->jenis_pengadaan_id);
            if (array_key_exists($value->jenis_pengadaan_id, $pagu)) {
                $pagu[$value->jenis_pengadaan_id]['pagu'] = $pagu[$value->jenis_pengadaan_id]['pagu'] + $value->pagu;
                $pagu[$value->jenis_pengadaan_id]['jumlah_paket'] = $pagu[$value->jenis_pengadaan_id]['jumlah_paket'] + $value->paket;
            }
            else {
                $pagu[$value->jenis_pengadaan_id] = [
                    'jenis_pengadaan' => $jenisPengadaan->name,
                    'pagu' => $value->pagu,
                    'jumlah_paket' => $value->paket
                ];
            }
        }

        $tpagu = [];
        $tpaket = [];
        foreach ($pagu as $key => $value) {
            $tpagu[] = $value['pagu'];
            $tpaket[] = $value['jumlah_paket'];
        }

        $totalpg = array_sum($tpagu);
        $totalpk = array_sum($tpaket);
        $a = [
            'jenis_pengadaan' => 'Grand Total',
            'pagu' => $totalpg,
            'jumlah_paket' => $totalpk
        ];
        // $r = array_merge($pagu, $newArr);

        $b = [
            'items' => $pagu,
            'total' => $a
        ];

        return $b;
    }

    public function _paket_konsolidasi()
    {  
        $model = DetailPaketKonsolidasi::select('paket_konsolidasi_id', 'klasifikasi_anggaran', DB::raw('SUM(pagu_anggaran) as pagu'))
            ->addSelect('paket_konsolidasi_id', DB::raw('COUNT(id_paket) as paket'))
            ->whereHas('paketKonsolidasi.user', function ($q) {
                    $q->where('user_id', auth()->id());
                })
            ->whereHas('paketKonsolidasi', function($q) {
                $q->where('checked', true);
            })
           
            ->groupBy('paket_konsolidasi_id')
            ->groupBy('klasifikasi_anggaran')
            ->with('PaketKonsolidasi')
            ->get();
        $paket = [];
        foreach ($model as $key => $value) {
            $namaPaket = '';
            if ($value->klasifikasi_anggaran == "BESAR") {
                if ($value->paketKonsolidasi->tingkat_resiko == "diabaikan" || $value->paketKonsolidasi->tingkat_resiko == "rendah") {
                    //leverage
                    $namaPaket = $value->paketKonsolidasi->nama_paket_konsolidasi . ' - Leverage'; 
                }

                if ($value->paketKonsolidasi->tingkat_resiko == "sedang" || $value->paketKonsolidasi->tingkat_resiko == "tinggi") {
                    //critical
                    $namaPaket = $value->paketKonsolidasi->nama_paket_konsolidasi . ' - Critical';
                }
            }

            if ($value->klasifikasi_anggaran == "KECIL") {
                if ($value->paketKonsolidasi->tingkat_resiko == "diabaikan" || $value->paketKonsolidasi->tingkat_resiko == "rendah") {
                    //routine
                    $namaPaket = $value->paketKonsolidasi->nama_paket_konsolidasi . ' - Routine';
                }
                
                if ($value->paketKonsolidasi->tingkat_resiko == "sedang" || $value->paketKonsolidasi->tingkat_resiko == "tinggi") {
                    //bottleneck
                    $namaPaket = $value->paketKonsolidasi->nama_paket_konsolidasi . ' - Bottleneck';
                }
            }
           

            $paket[] = [
                'paket' => $namaPaket,
                'pagu' => $value->pagu,
                'jumlah_paket' => $value->paket
            ];
        }

        return $paket;
    }

    public function konsolidasiDetailData($anggaran, $kosolidasi_id, $allowDownload=true, $hidePareto=true)
    {

        $paket = PaketKonsolidasi::find(Crypt::decrypt($kosolidasi_id));
        $namaPaket = '';
        if ($anggaran == "besar") {
            if ($paket->tingkat_resiko == "diabaikan" || $paket->tingkat_resiko == "rendah") {
                //leverage
                $namaPaket = $paket->nama_paket_konsolidasi . ' - Leverage'; 
            }

            if ($paket->tingkat_resiko == "sedang" || $paket->tingkat_resiko == "tinggi") {
                //critical
                $namaPaket = $paket->nama_paket_konsolidasi . ' - Critical';
            }
        }

        if ($anggaran == "kecil") {
            if ($paket->tingkat_resiko == "diabaikan" || $paket->tingkat_resiko == "rendah") {
                //routine
                $namaPaket = $paket->nama_paket_konsolidasi . ' - Routine';
            }
            
            if ($paket->tingkat_resiko == "sedang" || $paket->tingkat_resiko == "tinggi") {
                //bottleneck
                $namaPaket = $paket->nama_paket_konsolidasi . ' - Bottleneck';
            }
        }

        $data = DetailPaketKonsolidasi::where('paket_konsolidasi_id', Crypt::decrypt($kosolidasi_id))
                                    ->where('klasifikasi_anggaran',$anggaran)
                                    ->get();
        $instansiId = $data->first()->rup->instansi_id;
        $tahun = $data->first()->rup->tahun_anggaran;
        
        return view('web.pages.report.report-konsolidasi-list', compact('data', 'paket', 'namaPaket', 'allowDownload', 'hidePareto', 'instansiId', 'tahun'));
    }

    public function __metodePengadaan($request)
    {
        $kl = Filter::where('ip', auth()->id())->latest()->first();
        $klpd = $kl->nama_kementrian_lembaga;

        $data = PaketKonsolidasi::where('checked', true)
                ->wherehas('user', function($q){
                            $q->where('user_id', auth()->id());
                        })->first();
        $tahun = $data->detailPaketKonsolidasi->first()->rup->tahun_anggaran;
        $instansiID = $data->detailPaketKonsolidasi->first()->rup->instansi_id;

        $modelPenyedia = Data::select('metode_pengadaan_id', DB::raw('SUM(total_pagu_paket) as pagu'))
            ->addSelect('metode_pengadaan_id', DB::raw('COUNT(nama_paket) as paket'))
            ->where('instansi_id', $instansiID)
            ->where('tahun_anggaran', $tahun)
            ->groupBy('metode_pengadaan_id')
            ->penyedia()
            ->active()
            ->get();
        
        $modelPenyediaSwakelola = Data::select('metode_pengadaan_id', DB::raw('SUM(total_pagu_paket) as pagu'))
                ->addSelect('metode_pengadaan_id', DB::raw('COUNT(nama_paket) as paket'))
                ->where('instansi_id', $instansiID)
                ->where('tahun_anggaran', $tahun)
                ->groupBy('metode_pengadaan_id')
                ->penyediaSwakelola()
                ->active()
                ->get();

        $pagu = [];
        $total = '';

        foreach ($modelPenyedia as $key => $value) {
            $metodePengadaan = MetodePengadaan::find($value->metode_pengadaan_id);
            $pagu[$value->metode_pengadaan_id] = [
                'metode_pengadaan' => $metodePengadaan->nama,
                'total_pagu' => $value->pagu,
                'jumlah_paket' => $value->paket
            ];
        }
        
        foreach ($modelPenyediaSwakelola as $key => $value) {
            //$jenisPengadaan = JenisPengadaan::find($value->jenis_pengadaan_id);
            $metodePengadaan = MetodePengadaan::find($value->metode_pengadaan_id);
            if (array_key_exists($value->metode_pengadaan_id, $pagu)) {
                $pagu[$value->metode_pengadaan_id]['total_pagu'] = $pagu[$value->metode_pengadaan_id]['total_pagu'] + $value->pagu;
                $pagu[$value->metode_pengadaan_id]['jumlah_paket'] = $pagu[$value->metode_pengadaan_id]['jumlah_paket'] + $value->paket;
            }
            else {
                $pagu[$value->metode_pengadaan_id] = [
                    'metode_pengadaan' => $metodePengadaan->nama,
                    'total_pagu' => $value->pagu,
                    'jumlah_paket' => $value->paket
                ];
            }
        }

        $tpagu = [];
        $tpaket = [];
        foreach ($pagu as $key => $value) {
            $tpagu[] = $value['total_pagu'];
            $tpaket[] = $value['jumlah_paket'];
        }

        $totalpg = array_sum($tpagu);
        $totalpk = array_sum($tpaket);
        $a = [
            'metode_pengadaan' => 'Grand Total',
            'total_pagu' => $totalpg,
            'jumlah_paket' => $totalpk
        ];
        // $r = array_merge($pagu, $newArr);

        $b = [
            'items' => $pagu,
            'total' => $a
        ];

        return $b;
    }

    public function pareto(Request $request)
    {
        $instansiID = $request->instansiId;
        $tahun = $request->tahun;

        $data = Pareto::wherehas('filter', function($q) use($instansiID, $tahun) {
            $q->where('nama_kementrian_lembaga', $instansiID);
            $q->where('tahun', $tahun);
        });

        if ($request->keyword != '') {
            $keyword = $request->keyword;

            $data = $data->where(function($q) use($keyword) {
                $q->where('id_paket', 'like', '%'. $keyword .'%');
                $q->orWhere('nama_paket', 'like', '%'. $keyword. '%');
            });
        }

        return DataTables::of($data)
                    ->addIndexColumn()
                    ->make(true);
    }

    public function paretoPage($instansiId, $tahun)
    {
        $data = Pareto::wherehas('filter', function($q) use($instansiId, $tahun) {
            $q->where('nama_kementrian_lembaga', $instansiId);
            $q->where('tahun', $tahun);
        });

        $total = $data->sum('pagu_anggaran');

        $instansi = Instansi::find($instansiId);
        $instansi = $instansi->nama;
        
        return view('web.pages.pareto.index', compact('instansiId', 'tahun', 'total', 'instansi'));
    }

}
