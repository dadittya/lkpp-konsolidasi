<?php

namespace App\Http\Controllers;

use App\Models\Data;
use App\Models\Filter;
use App\Models\KelompokKonsolidasi as ModelKelompokKonsolidasi;
use App\Models\KelompokKonsolidasiDetail;
use App\Models\Keterangan;
use App\Models\PaketKonsolidasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class KelompokKonsolidasi extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) { 
            //uncheck semua paket
            PaketKonsolidasi::whereHas('user', function ($q) {
                $q->where('user_id', auth()->id());
            })->update(['checked' => 0]);

            PaketKonsolidasi::wherein('id', $request->paket_konsolidasi)
                            ->update(['checked' => 1]);

            $tempKlpd = "";
           
            $arrPaket = [];
            foreach ($request->paket_konsolidasi as $item) {

                
                $paket = PaketKonsolidasi::find($item);
                //var_dump($paket->detailPaketKonsolidasi->first()->rup->instansi_id);
                if ($tempKlpd == "") {
                    $tempKlpd = $paket->detailPaketKonsolidasi->first()->rup->instansi_id;
                }
                else {
                    if ($paket->detailPaketKonsolidasi->first()->rup->instansi_id != $tempKlpd ) {
                        return response()->json(['status' => false]);
                    }
                }
            }        
            return response()->json(['status' => true]);

            // foreach ($request->paket_konsolidasi as $item) {
            //     $paket = PaketKonsolidasi::find($item);
            //     $paket->checked = true;
            //     $paket->save();
            // }          
            //return response()->json(['status' => true]);
        }
        
        return view('web.pages.kelompok_konsolidasi.index');
    }

    public function kondisiPertama(Request $request)
    {
        $data = PaketKonsolidasi::whereHas('detailPaketKonsolidasi', function ($q) {
            $q->where('klasifikasi_anggaran', 'KECIL');
        })->whereHas('user', function ($q) {
            $q->where('user_id', auth()->id());
        })
        ->where(function($q) {
            $q->where('tingkat_resiko', 'tinggi');
            $q->orwhere('tingkat_resiko', 'sedang');
        })
        ->where('checked', 1)
        ->get();

        // return DataTables::of($data)->addIndexColumn()->editColumn('aksi', function ($data) {
        //     $enc = Crypt::encrypt($data->id);
        //     $url = url("paket-konsolidasi-detail/$enc");

        //     $btn = '<a href="' . $url . '" class="" id="">' . $data->nama_paket_konsolidasi . '</a>';
        //     return $btn;
        // })->rawColumns(['aksi', 'nama_paket_konsolidasi'])->make(true);

        $res = [];
        foreach ($data as $key => $value) {
            $id = Crypt::encrypt($value->id);
            $res[] = [
                'id' => $id,
                'nama_paket_konsolidasi' => $value->nama_paket_konsolidasi
            ];
        }
        return response()->json($res);
    }

    public function kondisiKedua(Request $request)
    {
        $data = PaketKonsolidasi::whereHas('detailPaketKonsolidasi', function ($q) {
            $q->where('klasifikasi_anggaran', 'BESAR');
        })->whereHas('user', function ($q) {
            $q->where('user_id', auth()->id());
        })
        ->where(function($q) {
            $q->where('tingkat_resiko', 'tinggi');
            $q->orwhere('tingkat_resiko', 'sedang');
        })
        ->where('checked', 1)
        ->get();

        // return DataTables::of($data)->addIndexColumn()->editColumn('nama_paket_konsolidasi', function ($data) {
        //     $enc = Crypt::encrypt($data->id);
        //     $url = url("paket-konsolidasi-detail/$enc");

        //     $btn = '<a href="' . $url . '" class="" id="">' . $data->nama_paket_konsolidasi . '</a>';
        //     return $btn;
        // })->rawColumns(['aksi', 'nama_paket_konsolidasi'])->make(true);


        // dd($data);
        $res = [];
        foreach ($data as $key => $value) {
            $id = Crypt::encrypt($value->id);
            $res[] = [
                'id' => $id,
                'nama_paket_konsolidasi' => $value->nama_paket_konsolidasi
            ];
        }
        return response()->json($res);
    }

    public function kondisiKetiga(Request $request)
    {
        $data = PaketKonsolidasi::whereHas('detailPaketKonsolidasi', function ($q) {
            $q->where('klasifikasi_anggaran', 'KECIL');
        })->whereHas('user', function ($q) {
            $q->where('user_id', auth()->id());
        })
        ->where(function($q) {
            $q->where('tingkat_resiko', 'rendah');
            $q->orwhere('tingkat_resiko', 'diabaikan');
        })
       
        ->where('checked', 1)
        ->get();

        // return DataTables::of($data)->addIndexColumn()->editColumn('nama_paket_konsolidasi', function ($data) {
        //     $enc = Crypt::encrypt($data->id);
        //     $url = url("paket-konsolidasi-detail/$enc");

        //     $btn = '<a href="' . $url . '" class="" id="">' . $data->nama_paket_konsolidasi . '</a>';
        //     return $btn;
        // })->rawColumns(['aksi', 'nama_paket_konsolidasi'])->make(true);

        $res = [];
        foreach ($data as $key => $value) {
            $id = Crypt::encrypt($value->id);
            $res[] = [
                'id' => $id,
                'nama_paket_konsolidasi' => $value->nama_paket_konsolidasi
            ];
        }
        return response()->json($res);
    }

    public function kondisiKeempat(Request $request)
    {
        $data = PaketKonsolidasi::whereHas('detailPaketKonsolidasi', function ($q) {
            $q->where('klasifikasi_anggaran', 'BESAR');
        })->whereHas('user', function ($q) {
            $q->where('user_id', auth()->id());
        })
        ->where(function($q) {
            $q->where('tingkat_resiko', 'rendah');
            $q->orwhere('tingkat_resiko', 'diabaikan');
        })
        ->where('checked', 1)
        ->get();

        // return DataTables::of($data)->addIndexColumn()->editColumn('nama_paket_konsolidasi', function ($data) {
        //     $enc = Crypt::encrypt($data->id);
        //     $url = url("paket-konsolidasi-detail/$enc");

        //     $btn = '<a href="' . $url . '" class="" id="">' . $data->nama_paket_konsolidasi . '</a>';
        //     return $btn;
        // })->rawColumns(['aksi', 'nama_paket_konsolidasi'])->make(true);
        $res = [];
        foreach ($data as $key => $value) {
            $id = Crypt::encrypt($value->id);
            $res[] = [
                'id' => $id,
                'nama_paket_konsolidasi' => $value->nama_paket_konsolidasi
            ];
        }
        return response()->json($res);
    }

    public function delete(Request $request)
    {
        try {
            $paket_konsolidasi = PaketKonsolidasi::find($request->id);
            $paket_konsolidasi->delete();

            return response()->json(['message' => 'Data Berhasil Dihapus']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }
    }


    public function _jenisPengadaan($request)
    {
        $kl = Filter::where('ip', auth()->id())->first();
        $jp = Data::select('jenis_pengadaan')->distinct('jenis_pengadaan')->take(4)->get();
        $ajp = [];

        foreach ($jp as $key => $value) {
            // $arr = explode(';', $value->jenis_pengadaan);
            $ajp[] = $value->jenis_pengadaan;
        }

        $model = Data::select('jenis_pengadaan', DB::raw('SUM(total_pagu_paket) as pagu'))
            ->addSelect('jenis_pengadaan', DB::raw('COUNT(nama_paket) as paket'))
            ->whereIn('jenis_pengadaan', $ajp)
            ->where('nama_kementrian_lembaga', $kl->nama_kementrian_lembaga)
            ->where('tahun_anggaran', $kl->tahun)
            ->groupBy('jenis_pengadaan')
            ->get();

        $pagu = [];
        foreach ($model as $key => $value) {
            $pagu[] = [
                'jenis_pengadaan' => $value->jenis_pengadaan,
                'pagu' => $value->pagu,
                'jumlah_paket' => $value->paket
            ];
        }
        return $pagu;
    }

    public function _metodePengadaan($request)
    {
        $kl = Filter::where('ip', auth()->id())->first();
        $model = Data::select('metode_pengadaan')->distinct('metode_pengadaan')->take(10)->get();
        // dd($jp);
        $ajp = [];
        // dd($model);

        foreach ($model as $key => $value) {
            $ajp[] = $value->metode_pengadaan;
        }

        $model = Data::select('metode_pengadaan', DB::raw('SUM(total_pagu_paket) as pagu'))
            ->addSelect('metode_pengadaan', DB::raw('COUNT(nama_paket) as paket'))
            ->whereIn('metode_pengadaan', $ajp)
            ->where('nama_kementrian_lembaga', $kl->nama_kementrian_lembaga)
            ->where('tahun_anggaran', $kl->tahun)
            ->groupBy('metode_pengadaan')
            ->get();

        $pagu = [];
        foreach ($model as $key => $value) {
            $pagu[] = [
                'metode_pengadaan' => $value->metode_pengadaan,
                'pagu' => $value->pagu,
                'jumlah_paket' => $value->paket
            ];
        }

        return $pagu;
    }

    public function _caraPengadaan($request)
    {
        $kl = Filter::where('ip', auth()->id())->first();
        $model = ['Penyedia Murni', 'Swakelola'];

        $pm = Data::select('tipe_paket', DB::raw('SUM(total_pagu_paket) as pagu'))
            ->addSelect('tipe_paket', DB::raw('COUNT(nama_paket) as paket'))
            // ->whereIn('tipe_paket', $model)
            ->where(function ($e) use ($model) {
                foreach ($model as $key => $value) {
                    $e->orWhere('tipe_paket', 'like', "%$value%");
                }
            })
            // ->orWhere('tipe_paket', 'like', "Swakelola%")
            ->where('nama_kementrian_lembaga', $kl->nama_kementrian_lembaga)
            ->where('tahun_anggaran', $kl->tahun)
            ->groupBy('tipe_paket')
            ->get();

        // $sw = Data::select('tipe_paket', DB::raw('SUM(total_pagu_paket) as pagu'))
        //     ->addSelect('tipe_paket', DB::raw('COUNT(nama_paket) as paket'))
        //     ->whereIn('tipe_paket', 'like', "$model%")
        //     ->where('tipe_paket', 'like', "Swakelola%")
        //     ->where('tipe_paket', 'like', "Swakelola%")
        //     ->where('nama_kementrian_lembaga', $kl->nama_kementrian_lembaga)
        //     ->groupBy('tipe_paket')
        //     ->get();
        // // dd($model);

        $data = [];
        foreach ($pm as $key => $value) {
            $str = preg_replace('/\d+/', '', $value->tipe_paket);
            $data[] = [
                'tipe_paket' => preg_replace('/\r\n/', '', $str),
                'paket' => $value->paket,
                'pagu' => $value->pagu
            ];
        }

        // dd(count($data));
        if (count($data) > 0) {

            $p = [];
            $pk = [];
            $tp = [];
            foreach ($data as $key => $value) {
                if ($value['tipe_paket'] > 0) {
                    # code...
                    $tp[] = $value['tipe_paket'];
                }
                $p[] = $value['pagu'];
                $pk[] = $value['paket'];
            }

            $pg = array_sum($p);
            $pkt = array_sum($pk);

            $res[] = [
                'tipe_paket' => $tp[0],
                'paket' => $pkt,
                'pagu' => $pg
            ];

            $data = $res;
        }

        return $data;
    }
}
