<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class ArtikelController extends Controller
{
    public function __construct()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.artikel.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.artikel.insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'body' => 'required',
            'foto' => 'mimes:jpg,jpeg,bmp,png'
        ]);

        $image = $request->file('foto');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images/artikel'),$imageName);

        $artikel = new Artikel;
        $artikel->judul = $request->judul;
        $artikel->plain_text = strip_tags($request->body);
        $artikel->body = $request->body;
        $artikel->images = $imageName;
        $artikel->save();

        return redirect()->route('admin.artikel.index')->withSuccess('Berhasil, Data berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artikel = Artikel::find($id);
        return view('admin.artikel.edit', compact('artikel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'body' => 'required'
        ]);

        $artikel = Artikel::find($id);
        $artikel->judul = $request->judul;
        $artikel->body = $request->body;
        $artikel->save();

        return redirect()->route('admin.artikel.index')->withSuccess('Berhasil, Data berhasil diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artikel = Artikel::find($id);
        $artikel->delete();

        return redirect()->route('admin.artikel.index')->withSuccess('Berhasil, Data berhasil dihapus!');
    }

    public function datatableArtikel()
    {
        $model = Artikel::query();
        return DataTables::of($model)->addIndexColumn()
            ->addColumn('aksi', function($d){
                $btn = '<div class="d-flex">
                        <a class="btn btn-sm btn-primary m-1" href="'. route('admin.artikel.edit', ['id' => $d->id]) .'"><i class="fa fa-edit"></i></a>
                        <form method="post" action="'.route('admin.artikel.destroy', $d->id).'" class="form-delete">
                            '.csrf_field().'
                            <input type="hidden" name="_method" value="delete" />
                            <button type="submit" class="btn btn-sm btn-danger m-1"><i class="fa fa-trash"></i></button>
                        </form>
                        </div>';
                return $btn;
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }
}
