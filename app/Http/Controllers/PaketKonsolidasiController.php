<?php

namespace App\Http\Controllers;

use App\Models\Data;
use App\Models\DetailPaketKonsolidasi;
use App\Models\JenisPengadaan;
use App\Models\MetodePengadaan;
use App\Models\PaketKonsolidasi;
use App\Models\Pareto;
use App\Models\PembuatPaket;
use App\Models\Satker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class PaketKonsolidasiController extends Controller
{
    public function storePaket(Request $request)
    {
        if (auth()->check() === false) return response()->json(['message' => "Maaf Anda Harus Login Terlebih Dahulu!"]);
        $rules = [
            'nama_paket_konsolidasi' => 'required'
        ];

        $message = [
            'nama_paket_konsolidasi.required' => 'Kolom nama paket tidak boleh kosong!',
        ];

        $validasi = Validator::make($request->all(), $rules, $message);
        if ($validasi->fails()) {
            return response()->json(['message' =>  $validasi->errors()->first(), 422]);
        }

        $paket_konsolidasi = PaketKonsolidasi::create(['nama_paket_konsolidasi' => $request->nama_paket_konsolidasi]);
        PembuatPaket::create(['user_id' => auth()->id(), 'paket_konsolidasi_id' => $paket_konsolidasi->id]);

        $arr = [];

        $keywords = $request->kata;
        //foreach ($request->kata as $key => $val) {

            $res = Data::where(function($q) use($keywords) {
                    foreach($keywords as $val) {
                        $q->orwhere('nama_paket', 'like',  '%' . $val . '%');
                    }
                })
                ->active()
                ->orderBy('total_pagu_paket','desc')
                ->where('tahun_anggaran', $request->tahun_anggaran)
                ->where(function ($query) use ($request) {
                    for ($i = 0; $i < count($request->lembaga); $i++) {
                        $query->orwhere('instansi_id', $request->lembaga[$i]);
                    }
                })
                ->where(function ($query) use ($request) {
                    if ($request->filled('satker')) {
                        for ($i = 0; $i < count($request->satker); $i++) {
                            $keyword = $request->satker[$i];
                            $query->orwhereHas('satker', function($q) use($keyword) {
                                $q->where('nama', $keyword);
                            });
                        }
                    }
                })
                ->where(function ($query) use ($request) {
                    if ($request->filled('jenis_pengadaan')) {
                        for ($i = 0; $i < count($request->jenis_pengadaan); $i++) {
                            $query->orwhere('jenis_pengadaan_id', $request->jenis_pengadaan[$i]);
                        }
                    }
                })
                ->where(function ($query) use ($request) {
                    if ($request->filled('provinsi')) {
                        for ($i = 0; $i < count($request->provinsi); $i++) {
                            for ($i = 0; $i < count($request->provinsi); $i++) {
                                if ($request->provinsi[$i] != null) {
                                    $query->orwhere('provinsi_id', $request->provinsi[$i]);
                                }
                            }
                        }
                    }
                })
                ->where(function ($query) use ($request) {
                    if ($request->filled('kota')) {
                        for ($i = 0; $i < count($request->kota); $i++) {
                            $query->orwhere('kabupaten_id', $request->kota[$i]);
                        }
                    }
                })
                ->select('nip_ppk', 'satker_id', 'nama_paket', 'total_pagu_paket', 'tahun_anggaran', 'metode_pengadaan_id', 'sumber_dana', 'kode_rup', 'tanggal_awal_pemilihan', 'mak', 'jenis_pengadaan_id')
                ->with('pareto', 'jenisPengadaan', 'metodePengadaan', 'satker')
                ->get();

            foreach ($res as $key => $value) {
                if ($key === 0) {

                    $arr[] = [
                        'paket_konsolidasi_id' => $paket_konsolidasi->id,
                        'id_paket' => $value['kode_rup'],
                        'nama_paket_asli' => $value['nama_paket'],
                        'pagu_anggaran' => $value['total_pagu_paket'],
                        'kumulatif_pagu' => $value->pareto->kumulatif_pagu,
                        'persentase_terhadap_total' => $value->pareto->persentase_terhadap_total,
                        'klasifikasi_anggaran' => $value->pareto->klasifikasi_anggaran,
                        'id_sirup' => $value['kode_rup'],
                        'nama_satker' => $value['nama_paket'],
                        'metode_pengadaan' => $value->jenisPengadaan->name,
                        'jenis_pengadaan' => $value->metodePengadaan->nama,
                        'sumber_dana' => $value['sumber_dana'],
                        'kode_akun' =>$value['mak'],
                        'ppk' => $value['nip_ppk'],
                    ];
                    
                }

                if ($key > 0) {
                    $arr[] = [
                        'paket_konsolidasi_id' => $paket_konsolidasi->id,
                        'id_paket' => $value['kode_rup'],
                        'nama_paket_asli' => $value['nama_paket'],
                        'pagu_anggaran' => $value['total_pagu_paket'],
                        'kumulatif_pagu' => $value->pareto->kumulatif_pagu,
                        'persentase_terhadap_total' => $value->pareto->persentase_terhadap_total,
                        'klasifikasi_anggaran' => $value->pareto->klasifikasi_anggaran,
                        'id_sirup' => $value['kode_rup'],
                        'nama_satker' => $value->satker->nama,
                        'metode_pengadaan' =>  $value->jenisPengadaan->name,
                        'jenis_pengadaan' => $value->metodePengadaan->nama,
                        'sumber_dana' => $value['sumber_dana'],
                        'kode_akun' =>$value['mak'],
                        'ppk' => $value['nip_ppk'],
                    ];
                };
            }
            
            try {

                $insert_data = collect($arr); 

                
                $chunks = $insert_data->chunk(500);

                foreach ($chunks as $chunk) {

                    //dd($chunk);die();
                    DetailPaketKonsolidasi::insert($chunk->toArray());
                }

                

                //DB::commit();
            } catch (\Exception $e) {
                //DB::rollBack();
                if (env('APP_DEBUG'))
                    dd($e->getMessage());

                return response()->json(['success' => false, 'message' => 'Ups Terjadi Kesalahan!', 'err' => $e->getMessage()]);
            }


        return response()->json(['success' => true, 'message' => 'Paket Berhasil Ditambahkan']);
    }

    public function getAllData()
    {
        return view('web.pages.konsolidasi.daftar_konsolidasi');
    }

    public function konsolidasiDatatable()
    {
        $data = PaketKonsolidasi::whereHas('user', function ($q) {
            $q->where('user_id', auth()->id());
        })->orderBy('id', 'desc')->get();

        return DataTables::of($data)->addIndexColumn()
            ->editColumn('created_at', function ($request) {
                return $request->created_at->format('d-m-Y H:i:s');
            })->editColumn('updated_at', function ($request) {
                return $request->updated_at->toDayDateTimeString();
            })->addColumn('aksi', function ($data) {
                $enc = Crypt::encrypt($data->id);
                $url = url("paket-konsolidasi-detail/$enc");

                $btn = '<a href="' . $url . '" class="btn btn-outline-primary m-1" id="">Detail</a>
            <button class="btn btn-outline-danger m-1" value="' . $data->id . '" id="btn_hapus">Hapus</button>';
                return $btn;
            })
            ->rawColumns(['aksi', 'tingkat_resiko'])->make(true);
    }


    public function konsolidasiDetail($konsolidasi_id, $klasifikasi = null)
    {
        $paket_konsolidasi = PaketKonsolidasi::find(Crypt::decrypt($konsolidasi_id));
        return view('web.pages.konsolidasi.detail_konsolidasi', compact('paket_konsolidasi', 'klasifikasi'));
    }

    public function konsolidasiChecked(Request $request)
    {
        try {
            $paket_konsolidasi = PaketKonsolidasi::find($request->id);
            $paket_konsolidasi->checked = $request->status;
            

            return response()->json(['message' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'failed']);
        }
    }

    public function konsolidasiDelete(Request $request)
    {
        try {
            $paket_konsolidasi = PaketKonsolidasi::find($request->id);
            DetailPaketKonsolidasi::where('paket_konsolidasi_id', $request->id)->delete();
            $paket_konsolidasi->delete();

            return response()->json(['message' => 'Data Berhasil Dihapus']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Data Gagal Dihapus!']);
        }
    }

    public function konsolidasiDeleteAll(Request $request)
    {
        try {
            foreach ($request->paket_konsolidasi as $key => $value) {
                $konsolidasi = PaketKonsolidasi::find($value)->delete();
                DetailPaketKonsolidasi::where('paket_konsolidasi_id', $request->id)->delete();
            }

            return response()->json(['message' => 'Data Berhasil Dihapus']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Data Gagal Dihapus!']);
        }
    }



    public function konsolidasiDetailData(Request $request)
    {
        $data = DetailPaketKonsolidasi::where('paket_konsolidasi_id', $request->konsolidasi_id)
                    ->with('rup');

        if ($request->keyword != '') {
            $str = $request->keyword;
            $delimiter = ' ';
            $words = explode($delimiter, $str);
        
            $data = $data->where(function($q) use ($words) {
                foreach ($words as $word) {
                    $q->orWhere('nama_paket_asli',  'regexp',  '[[:<:]]' . $word . '[[:>:]]');   
                }   
            });
            
        }

        if ($request->kategori != '') {
            $data = $data->where('klasifikasi_anggaran', $request->kategori);
        }

        //$data = $data->distinct('kode_rup');
        return DataTables::of($data)
        ->editColumn('pagu_anggaran', function ($data) {
            return number_format($data->pagu_anggaran,0,',','.');
        })
        ->editColumn('kumulatif_pagu', function ($data) {
            return number_format($data->kumulatif_pagu,2,',','.');
        })
        ->editColumn('rup.statususahakecil', function($request) {
            return $request->rup->statususahakecil == 'UsahaKecil' ? 'YA' : 'TIDAK'; 
        })
        ->addColumn('pick', function ($data) {
            if ($data->picked) {
                $checked = 'checked';
            }
            else {
                $checked = '';
            }
            $action = '<input class="text-center" type="checkbox" name="picked[]" value="' .
            $data->id . '" ' . $checked . '>'; 
            
            return $action;
        })
        ->addIndexColumn()->rawColumns(['aksi', 'pick'])->make(true);
    }

    public function konsolidasiUpdateResiko(Request $request)
    {
        try {
            $data = PaketKonsolidasi::find($request->konsolidasi_id);
            $data->update(['tingkat_resiko' => $request->tingkat_resiko]);
            return response()->json(['message' => 'Data Berhasil Diupdate']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Data Gagal Diupdate']);
        }

        return response()->json(['message' => 'Data Berhasil Diupdate']);
    }

    public function konsolidasiUpdateName(Request $request)
    {
        try {
            $data = PaketKonsolidasi::find($request->konsolidasi_id);
            $data->update(['nama_paket_konsolidasi' => $request->namaPaketKonsolidasi]);
            return response()->json(['message' => 'Data Berhasil Diupdate']);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Data Gagal Diupdate']);
        }

        return response()->json(['message' => 'Data Berhasil Diupdate']);
    }

    public function konsolidasiDeletePaket(Request $request)
    {
        try {
            $data = DetailPaketKonsolidasi::find($request->id);
            $data->delete();
            DetailPaketKonsolidasi::where('paket_konsolidasi_id', $request->id)->delete();
        } catch (\Exception $e) {
            return response()->json(['message' => 'Data Gagal Diupdate']);
        }

        return response()->json(['message' => 'Data Berhasil Dihapus']);
    }

    //Delete Selected / All Detail Paket
    public function deletePaketAll(Request $request)
    {
        try {
            foreach ($request->picked as $key => $value) {
                $paket_konsolidasi = DetailPaketKonsolidasi::find($value);
                $paket_konsolidasi->delete();
            }

            return response()->json(['message' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }
    }

    //Simpan Selected / All Detail Paket
    public function saveSelected(Request $request)
    {
        try {
            $paketName = $request->nama_paket;
            $resiko = $request->tingkat_resiko;
            $paketItems = DetailPaketKonsolidasi::wherein('id', $request->picked)
                                ->get();

            $paket_konsolidasi = PaketKonsolidasi::create([
                'nama_paket_konsolidasi' => $paketName,
                'tingkat_resiko' => $resiko
            ]);

            PembuatPaket::create(['user_id' => auth()->id(), 'paket_konsolidasi_id' => $paket_konsolidasi->id]);

            foreach ($paketItems as $item) {
                $arr[] = [
                    'paket_konsolidasi_id' => $paket_konsolidasi->id,
                    'id_paket' => $item->id_paket,
                    'nama_paket_asli' => $item->nama_paket_asli, 
                    'pagu_anggaran' => $item->pagu_anggaran,
                    'kumulatif_pagu' => $item->kumulatif_pagu,
                    'persentase_terhadap_total' => $item->persentase_terhadap_total,
                    'klasifikasi_anggaran' => $item->klasifikasi_anggaran,
                    'id_sirup' => $item->id_sirup,
                    'nama_satker' =>$item->nama_satker,
                    'metode_pengadaan' =>  $item->metode_pengadaan,
                    'jenis_pengadaan' => $item->jenis_pengadaan,
                    'sumber_dana' => $item->sumber_dana,
                    'kode_akun' => $item->kode_akun,
                    'ppk' => $item->ppk,
                ];
            }

            $insert_data = collect($arr); 

                
            $chunks = $insert_data->chunk(500);

            foreach ($chunks as $chunk) {

                //dd($chunk);die();
                DetailPaketKonsolidasi::insert($chunk->toArray());
            }

            return response()->json(['message' => 'success']);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage()]);
        }
    }
}
