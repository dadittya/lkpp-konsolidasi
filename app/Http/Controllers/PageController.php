<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Repository;
use Illuminate\Http\Request;

class PageController extends Controller
{
    public function contohKonsolidasi()
    {
        $data = Repository::orderBy('updated_at', 'desc')->get();
        $banners = Banner::get();

        return view('web.pages.contoh-konsolidasi', compact('data', 'banners'));
    }

    public function kontak()
    {
        $banners = Banner::get();

        return view('web.pages.contact-us', compact( 'banners'));   
    }
}
