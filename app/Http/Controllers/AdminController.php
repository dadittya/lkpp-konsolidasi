<?php

namespace App\Http\Controllers;

use App\Models\DetailPaketKonsolidasi;
use App\Models\PaketKonsolidasi;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{

    public function __construct()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('admin.login');
    }

    public function reloadCaptcha()
    {
        return response()->json(['captcha' => captcha_img()]);
    }

    public function listKonsolidasi()
    {
        return view('admin.konsolidasi.index');
    }

    public function konsolidasiData()
    {
        $data = PaketKonsolidasi::with('user.user')->orderBy('id', 'desc')->get();

        return DataTables::of($data)->addIndexColumn()
        ->editColumn('created_at', function ($request) {
            return $request->created_at->toDayDateTimeString();
        })->make(true);
    }

    public function konsolidasiDataDetail(Request $request)
    {
        // dd($request->all());
        $data = DetailPaketKonsolidasi::with('paketKonsolidasi')->orderBy('id', 'asc')->where('paket_konsolidasi_id',$request->id);

        return DataTables::of($data)->addIndexColumn()
        ->editColumn('pagu_anggaran', function ($data) {
            return number_format($data->pagu_anggaran);
        })
        ->editColumn('kumulatif_pagu', function ($data) {
            return number_format($data->kumulatif_pagu);
        })
        ->make(true);
    }
}
