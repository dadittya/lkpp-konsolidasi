<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class MailController extends Controller
{
    public function composeEmail() {
        require base_path("vendor/autoload.php");
        $mail = new PHPMailer(true);     // Passing `true` enables exceptions

        try {

            // Email server settings
            $mail->SMTPDebug = 0;
            $mail->isSMTP();
            $mail->Host = 'smtp.gmail.com';             //  smtp host
            $mail->SMTPAuth = true;
            $mail->Username = 'dadittya.smtp@gmail.com';   //  sender username
            $mail->Password = 'Dadittya1234';       // sender password
            $mail->SMTPSecure = 'tls';                  // encryption - ssl/tls
            $mail->Port = 587;                          // port - 587/465

            $mail->setFrom('dadittya.smtp@gmail.com', 'test lkpp');
            $mail->addAddress('dadittya.adi@gmail.com');
            // $mail->addCC($request->emailCc);
            // $mail->addBCC($request->emailBcc);

            //$mail->addReplyTo('sender-reply-email', 'sender-reply-name');

            // if(isset($_FILES['emailAttachments'])) {
            //     for ($i=0; $i < count($_FILES['emailAttachments']['tmp_name']); $i++) {
            //         $mail->addAttachment($_FILES['emailAttachments']['tmp_name'][$i], $_FILES['emailAttachments']['name'][$i]);
            //     }
            // }


            $mail->isHTML(true);                // Set email content format to HTML

            $mail->Subject = 'Test LKPP';
            $mail->Body    = 'This content';

            // $mail->AltBody = plain text version of email body;

            if( !$mail->send() ) {
                return response('failed');
            }
            
            else {
                return response('success');
            }

        } catch (Exception $e) {
             return response('failed');
        }
    }
}
