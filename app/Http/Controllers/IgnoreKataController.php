<?php

namespace App\Http\Controllers;

use App\Imports\IgnoreKataImport;
use App\Models\IgnoreKata;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;
use Maatwebsite\Excel\Facades\Excel;


class IgnoreKataController extends Controller
{
    public function __construct()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.ignore-kata.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.ignore-kata.insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'kata' => 'required|unique:ignore_katas,kata'
        ]);

        $kata = new IgnoreKata;
        $kata->kata = $request->kata;
        $kata->save();

        return redirect()->route('admin.ignore-kata.index')->withSuccess('Data berhasil ditambah');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kata = IgnoreKata::find($id);
        return view('admin.ignore-kata.edit', compact('kata'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'kata' => 'required'
        ]);

        $kata = IgnoreKata::find($id);
        $kata->kata = $request->kata;
        $kata->save();

        return redirect()->route('admin.ignore-kata.index')->withSuccess('Data berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kata = IgnoreKata::find($id);
        $kata->delete();

        return redirect()->route('admin.ignore-kata.index')->withSuccess('Data berhasil dihapus');
    }

    public function datatable()
    {
        $model = IgnoreKata::query();
        return DataTables::of($model)->addIndexColumn()
            ->addColumn('aksi', function ($d) {
                $btn = '<div class="d-flex">
                        <a class="btn btn-sm btn-primary m-1" href="' . route('admin.ignore-kata.edit', ['id' => $d->id]) . '"><i class="fa fa-edit"></i></a>
                        <form method="post" action="' . route('admin.ignore-kata.destroy', $d->id) . '" class="form-delete">
                            ' . csrf_field() . '
                            <input type="hidden" name="_method" value="delete" />
                            <button type="submit" class="btn btn-sm btn-danger m-1"><i class="fa fa-trash"></i></button>
                        </form>
                        </div>';
                return $btn;
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }

    public function import(Request $request)
    {
        // validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
 
		// menangkap file excel
		$file = $request->file('file');
 
		// membuat nama file unik
		$nama_file = rand().$file->getClientOriginalName();
 
		// upload ke folder file_siswa di dalam folder public
		$file->move('file_import',$nama_file);
 
		// import data
		Excel::import(new IgnoreKataImport, public_path('/file_import/'.$nama_file));
 
		// alihkan halaman kembali
		return redirect()->route('admin.ignore-kata.index')->withSuccess('Data berhasil ditambahkan');
    }
}
