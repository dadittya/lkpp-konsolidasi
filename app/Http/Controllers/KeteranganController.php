<?php

namespace App\Http\Controllers;

use App\Models\Keterangan;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class KeteranganController extends Controller
{
    public function __construct()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));
    }

    public function index()
    {
        return view('admin.keterangan.index');
    }

    public function create()
    {
        return view('admin.keterangan.insert');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'konten' => 'required',
            'tipe' => 'required'
        ]);

        $model = new Keterangan;
        $model->title = $request->title;
        $model->konten = $request->konten;
        $model->konten_text = strip_tags($request->konten);
        $model->tipe = $request->tipe;
        $model->save();

        return redirect()->route('admin.keterangan.index')->withSuccess('Data berhasil disimpan');
    }

    public function edit($id)
    {
        $data = Keterangan::find($id);
        return view('admin.keterangan.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'konten' => 'required',
            'tipe' => 'required'
        ]);

        $model = Keterangan::find($id);
        $model->title = $request->title;
        $model->konten = $request->konten;
        $model->konten_text = strip_tags($request->konten);
        $model->tipe = $request->tipe;
        $model->save();

        return redirect()->route('admin.keterangan.index')->withSuccess('Data berhasil diubah');
    }

    public function destroy($id)
    {
        $model = Keterangan::find($id);
        $model->delete();

        return redirect()->route('admin.keterangan.index')->withSuccess('Data berhasil diubah');
    }

    public function datatable()
    {
        $model = Keterangan::query();
        return DataTables::of($model)->addIndexColumn()
            ->addColumn('aksi', function($d){
                $btn = '<div class="d-flex">
                        <a class="btn btn-sm btn-primary m-1" href="'. route('admin.keterangan.edit', ['id' => $d->id]) .'"><i class="fa fa-edit"></i></a>
                        <form method="post" action="'.route('admin.keterangan.destroy', $d->id).'" class="form-delete">
                            '.csrf_field().'
                            <input type="hidden" name="_method" value="delete" />
                            <button type="submit" class="btn btn-sm btn-danger m-1"><i class="fa fa-trash"></i></button>
                        </form>
                        </div>';
                return $btn;
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }
}
