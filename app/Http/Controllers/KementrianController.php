<?php

namespace App\Http\Controllers;

use App\Jobs\paretoJob;
use App\Models\Banner;
use App\Models\Data;
use App\Models\Filter;
use App\Models\IgnoreKata;
use App\Models\Instansi;
use App\Models\JenisPengadaan;
use App\Models\Kabupaten;
use App\Models\MetodePengadaan;
use App\Models\Mklpd;
use App\Models\PaketKonsolidasi;
use App\Models\ParetoFilter;
use Illuminate\Http\Request;
use App\Models\Pareto;
use App\Models\Provinsi;
use App\Models\Satker;
use App\Models\Tahun;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Response;


class KementrianController extends Controller
{
    public function index()
    {
        $banners = Banner::all();
        $tahun_anggaran = Tahun::select('nama')
                                ->orderby('nama')
                                ->get();      

        return view('welcome', compact('banners',  'tahun_anggaran'));
    }

    public function getKl(Request $request)
    {
        //$data = Mklpd::orderBy('text', 'asc')->get();
        $data = Instansi::orderBy('nama')->pluck('id', 'nama');
        return response()->json($data);
    }

    public function getSatker(Request $request)
    {
        try {
            if ($request->lembagas) {
                $satker = Satker::select('nama')
                                ->where(function ($q) use ($request) {
                                foreach ($request->lembagas as $key => $value) {
                                    $q->orwhere('instansi_id', $value);
                                }})
                                ->groupBy('nama')
                                ->orderBy('nama')
                                ->pluck('nama', 'nama');

                return response()->json($satker);
            }

            return response()->json(['message' => 'Tidak ada data yang ditemukan!', 'success' => false]);
        } catch (\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'success' => false]);
        }
    }

    public function getJenisPengadaan(Request $request)
    {
        $data = JenisPengadaan::where('id', '!=', '999')
                            ->orderBy('name')
                            ->pluck('id', 'name');
        return response()->json($data);
    }

    public function kataPopular(Request $request)
    {
        $request->validate([
            'kl' => 'required',
            'tahun_anggaran' => 'required',
        ], [
            'kl.required' => 'Minimal Pilih Satu KL/PD Untuk Melanjutkan!',
            'tahun_anggaran.required' => 'Tahun Anggaran Tidak Boleh Kosong!',
        ]);

        $lembaga[] = $request->kl;
        $satker[] = $request->satker ? $request->satker : [];
        $jenisPengadaan[] = $request->jenis_pengadaan ? $request->jenis_pengadaan : [];
        $provinsi[] = $request->provinsi ? $request->provinsi : [];
        $kota[] = $request->pemkot ? $request->pemkot : [];
        $tahun_anggaran = $request->tahun_anggaran;
        $suku_kata = $request->suku_kata;

        // $strProvinsi = "";
        // if ($provinsi != "") {
        //     dd($provinsi);
        //     $strProvinsi = Provinsi::find($provinsi[1])->nama;
        // }

        // $strKota = "";
        // if ($kota != "") {
        //     $strKota = Kabupaten::find($kota[1])->nama;
        // }
        //dd($request->ip());
        $userId = auth()->id();
        $filterin = Filter::updateOrCreate(['ip' => $userId ?? '-'], [
            'nama_kementrian_lembaga' => $request->kl,
            'nama_satuan_kerja' => $request->satker,
            'jenis_pengadaan' => $request->jenis_pengadaan ,
            'provinsi' => $request->provinsi,
            'pemkot' => $request->pemkot,
            'tahun' => $tahun_anggaran
        ]);
        $strSatker = "";
        $strJenis = "";
        $strProvinsi = "";
        $strKota = "";

        if ($request->satker) {
            $strSatker = implode("|",$request->satker);
        }

        if ($request->jenis_pengadaan) {
            $strJenis = implode("|",$request->jenis_pengadaan);
        }

        if ($request->provinsi) {
            $strProvinsi = implode("|",$request->provinsi);
        }

        if ($request->pemkot) {
            $strKota = implode("|",$request->pemkot);
        }

        $filter = ParetoFilter::updateOrCreate(
             [
             'nama_kementrian_lembaga' => implode("|",$request->kl),
             'nama_satuan_kerja' => $strSatker,
             'jenis_pengadaan' => $strJenis,
             'provinsi' => $strProvinsi,
             'pemkot' => $strKota,
             'tahun' => $tahun_anggaran
         ]);

         
        
        $totalPareto = Pareto::where('id_header', $filter->id)->count();
        $totalData = Data::where('tahun_anggaran', $request->tahun_anggaran)
            ->active()
            ->where(function ($query) use ($request) {
                for ($i = 0; $i < count($request->kl); $i++) {
                    $query->orwhere('instansi_id', $request->kl[$i]);
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('satker')) {
                    for ($i = 0; $i < count($request->satker); $i++) {
                        $keyword = $request->satker[$i];
                        $query->orwhereHas('satker', function($q) use($keyword) {
                            $q->where('nama', $keyword);
                        });
                    }
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('jenis_pengadaan')) {
                    for ($i = 0; $i < count($request->jenis_pengadaan); $i++) {                        
                        $query->orwhere('jenis_pengadaan_id', $request->jenis_pengadaan[$i]);
                    }
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('provinsi')) {
                    for ($i = 0; $i < count($request->provinsi); $i++) {
                        if ($request->provinsi[$i] != null) {
                            $query->orwhere('provinsi_id', $request->provinsi[$i]);
                        }
                    }
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('kota')) {
                    for ($i = 0; $i < count($request->kota); $i++) {
                        if ($request->kota[$i] != null) {
                            $query->orwhere('kabupaten_id', $request->kota[$i]);
                        }
                    }
                }
            })->count('kode_rup');

           
        if (($totalData != $totalPareto) && $totalData > 0) {
            $pareto = self::getPareto($request);

            self::fillPareto($pareto[0], $pareto[1], $filter->id);
        }

        $strLembaga = Instansi::wherein('id', $lembaga[0])->pluck('nama');
        $strSatker = $satker[0];
        
        // if (count($satker) > 0) {
            
        //     if (count($satker[0]) > 0) {
        //         foreach ($i = 0; $i < count($satker[]); $i++) {

        //         }
        //     }
        // }
        // /dd($strSatker);

        $strProvinsi = [];
        if (count($provinsi) > 0) {
            
            if (count($provinsi[0]) > 0) {
                $strProvinsi = Provinsi::wherein('id', $provinsi[0])->pluck('nama');
            }
        }

        $strKota = [];
        if (count($kota) > 0) {
            
            if (count($kota[0]) > 0) {
                $strKota = Kabupaten::wherein('id', $kota[0])->pluck('nama');
            }
        }

        $jenisStr = [];

        if (count($jenisPengadaan)) {
            if (count($jenisPengadaan[0]) > 0) {
                $jenisStr = JenisPengadaan::wherein('id', $jenisPengadaan[0])->pluck('name');
            }
        }

        //dd($jenisStr);


        return view('web.pages.page_1', compact('strLembaga','lembaga', 'satker', 'strSatker', 'jenisPengadaan', 'jenisStr','provinsi', 'tahun_anggaran', 'kota', 'suku_kata', 'strKota', 'strProvinsi'));
    }

    public function getLastPaket()
    {
        try {
            $data = PaketKonsolidasi::whereHas('user', function ($q) {
                $q->where('user_id', auth()->id());
            })->orderBy('id', 'desc')->paginate(5);
            return response()->json(['data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['data' => []]);
        }
    }

    public function datatable(Request $request)
    {
        $ig = [];
        $ignore = IgnoreKata::all('kata');
        foreach ($ignore as $key => $value) {
            $ig[] = strtolower($value->kata);
        }

        $jenisNull = Data::where('tahun_anggaran', $request->tahun_anggaran)
                            ->where(function ($query) use ($request) {
                                for ($i = 0; $i < count($request->lembaga); $i++) {
                                    $query->orwhere('instansi_id', $request->lembaga[$i]);
                                }
                            })
                            ->wherenull('jenis_pengadaan_id')
                            ->count();
        if ($jenisNull > 0) {
            $jenis = JenisPengadaan::firstOrCreate([
                'id' => 999,
                'name' => '-'
            ]);

            //dd($jenis->id);

            Data::where('tahun_anggaran', $request->tahun_anggaran)
            ->where(function ($query) use ($request) {
                for ($i = 0; $i < count($request->lembaga); $i++) {
                    $query->orwhere('instansi_id', $request->lembaga[$i]);
                }
            })
            ->wherenull('jenis_pengadaan_id')
            ->update(['jenis_pengadaan_id' => $jenis->id]);
        }

        $metodeNull = Data::where('tahun_anggaran', $request->tahun_anggaran)
                            ->where(function ($query) use ($request) {
                                for ($i = 0; $i < count($request->lembaga); $i++) {
                                    $query->orwhere('instansi_id', $request->lembaga[$i]);
                                }
                            })
                            ->wherenull('metode_pengadaan_id')
                            ->count();
                            
        if ($metodeNull > 0) {
            $metode = MetodePengadaan::firstOrCreate([
                'id' => 999,
                'nama' => '-'
            ]);

            //dd($jenis->id);

            Data::where('tahun_anggaran', $request->tahun_anggaran)
            ->where(function ($query) use ($request) {
                for ($i = 0; $i < count($request->lembaga); $i++) {
                    $query->orwhere('instansi_id', $request->lembaga[$i]);
                }
            })
            ->wherenull('metode_pengadaan_id')
            ->update(['metode_pengadaan_id' => $metode->id]);
        }
        $model = Data::select('nama_paket')
            ->active()
            ->where('tahun_anggaran', $request->tahun_anggaran)
            ->where(function ($query) use ($request) {
                for ($i = 0; $i < count($request->lembaga); $i++) {
                    $query->orwhere('instansi_id', $request->lembaga[$i]);
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('satker')) {
                    for ($i = 0; $i < count($request->satker); $i++) {
                        $keyword = $request->satker[$i];
                        $query->orwhereHas('satker', function($q) use($keyword) {
                            $q->where('nama', $keyword);
                        });
                    }
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('jenis_pengadaan')) {
                    for ($i = 0; $i < count($request->jenis_pengadaan); $i++) {
                        $query->orwhere('jenis_pengadaan_id', $request->jenis_pengadaan[$i]);
                    }
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('provinsi')) {
                    for ($i = 0; $i < count($request->provinsi); $i++) {
                        if($request->provinsi[$i] != null) {
                            $query->orwhere('provinsi_id', $request->provinsi[$i]);
                        }
                    }
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('kota')) {
                    for ($i = 0; $i < count($request->kota); $i++) {
                        if($request->kota[$i] != null) {
                            $query->orwhere('kabupaten_id', $request->kota[$i]);
                        }
                    }
                }
            });

        $data = $model->get();

        $kata = [];
        //$checkDuplicate = [];
        $p = [];
        foreach ($data as $val) {
            if ($request->suku_kata > 1) {
                $p = preg_split('/\s/', $val->nama_paket);     
                //$p = $val->nama_paket;
                
            }
            else {
                //$p = preg_split('/[.,\/?!;-]/', $val->nama_paket);     
                $p = preg_replace('/[.,\/?!;-]/', ' ', $val->nama_paket);     
                $p = str_word_count($p,1);
            }
            
            
            $words = "";
            
            foreach ($p as $key => $item) {
                //foreach (str_word_count($p,1) as $item) {
                    
                    $wordsCount = $words != "" ? count(preg_split('/\s/', $words)) : 0;    
                    //var_dump($wordsCount);
                    if ($item !== "" && !empty($item) && !in_array(strtolower($item), $ig)) {
                        
                        if ($request->suku_kata > 1) {

                            if ($wordsCount < $request->suku_kata) {           
                                if ($words == "") {
                                    $words .= $item;
                                }
                                else {
                                    $words .= " " . $item;
                                }
                                
                            }
                        }
                        else {
                            if (strlen($item) >= 3) {
                                if ($wordsCount < $request->suku_kata) {           
                                    
                                    if ($words == "") {
                                        $words .= $item;
                                    }
                                    else {
                                        $words .= " " . $item;
                                    }
                                    
                                }
                            }
                        }
                        
                        $wordsCount = $words != "" ? count(preg_split('/\s/', $words)) : 0;

                        if ($wordsCount == $request->suku_kata) {
                            if ($words != ""  && $wordsCount == $request->suku_kata ) {
                                //var_dump($words);
                                //$checkDuplicate[] = strtolower($words);
                                //$kata[] = $words;
                                $kata[] = strtolower($words);
                            }
                            $words = substr(strstr($words," "), 1);
                        }
                    }
                    else {
                        if ($words != "") {
                            $words = "";
                        }
                    }
                }
            //}
        }
//die();
       
        $datas = [];
        $results = array_count_values($kata);
        $getMax = array_count_values($kata);
        rsort($getMax);

        $check  = isset($getMax[100]) ? $getMax[100] : '';

        foreach ($results as $key => $value) {
            if ($value >= $check) {
                if (count($datas) < 100) {
                    $keyword = ltrim($key, " '");
                    //var_dump($keyword);die();
                    //$jmlPaket = $this->getJumlahPaketByWord($keyword, $request);
                    $datas[] = [
                            'word' => ucwords($key), 
                            'jumlah_kata' => $value, 
                            //'jumlah_paket' => $jmlPaket
                        ];
                }
            }
        }
        

        $coll = collect($datas);
        $coll->sortByDesc('jumlah_kata');
        return response()->json(['data' => $coll->unique('word'), 'message' => 'Berhasil Mengambil Data!']);
    }

    public function getJumlahPaketByWord($keyword, $request)
    {
        $model = Data::where(function($query) use ($keyword){
                        $query->orwhere('nama_paket', 'like', $keyword . ' %');
                        $query->orwhere('nama_paket', 'like', '% ' . $keyword);
                        $query->orwhere('nama_paket', 'like', '% ' . $keyword . ' %');
                        $query->orwhere('nama_paket', 'like', '%/' . $keyword . '%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . '/%');
                        $query->orwhere('nama_paket', 'like', '%-' . $keyword . '%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . '-%');
                        $query->orwhere('nama_paket', 'like', '%-' . $keyword . '%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . '-%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . '.%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . ',%');
                        $query->orwhere('nama_paket', 'like', '%(' . $keyword . '%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . ')%');

                    })
            ->where('tahun_anggaran', $request->tahun_anggaran)
            ->active()
            ->where(function ($query) use ($request) {
                for ($i = 0; $i < count($request->lembaga); $i++) {
                    $query->orwhere('instansi_id', $request->lembaga[$i]);
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('satker')) {
                    for ($i = 0; $i < count($request->satker); $i++) {
                        $keyword = $request->satker[$i];
                        $query->orwhereHas('satker', function($q) use($keyword) {
                            $q->where('nama', $keyword);
                        });
                    }
                }
            })
            /*->where(function ($query) use ($request) {
                if ($request->filled('jenis_pengadaan')) {
                    for ($i = 0; $i < count($request->jenis_pengadaan); $i++) {
                        $query->orwhere('jenis_pengadaan_id', $request->jenis_pengadaan[$i]);
                    }
                }
            })*/
            ->where(function ($query) use ($request) {
                if ($request->filled('provinsi')) {
                    for ($i = 0; $i < count($request->provinsi); $i++) {
                        $query->orwhere('provinsi_id', $request->provinsi[$i]);
                    }
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('kota')) {
                    for ($i = 0; $i < count($request->kota); $i++) {
                        $query->orwhere('kabupaten_id', $request->kota[$i]);
                    }
                }
            })->count();

        return $model;
    }

    public function getJumlahPaket(Request $request)
    {
        $keyword = $request->word;
        $sukuKata = $request->suku_kata;
        
        if ($sukuKata == 1) {
        $model = Data::where(function($query) use ($keyword){
                        $query->orwhere('nama_paket', 'like', $keyword . ' %');
                        $query->orwhere('nama_paket', 'like', '% ' . $keyword);
                        $query->orwhere('nama_paket', 'like', '% ' . $keyword . ' %');
                        $query->orwhere('nama_paket', 'like', '%/' . $keyword . '%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . '/%');
                        $query->orwhere('nama_paket', 'like', '%-' . $keyword . '%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . '-%');
                        $query->orwhere('nama_paket', 'like', '%-' . $keyword . '%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . '-%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . '.%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . ',%');
                        $query->orwhere('nama_paket', 'like', '%(' . $keyword . '%');
                        $query->orwhere('nama_paket', 'like', '%' . $keyword . ')%');

                    });
        }
        else {
            $model = Data::where(function($query) use ($keyword){
                $query->orwhere('nama_paket', 'like', $keyword . ' %');
                $query->orwhere('nama_paket', 'like', '% ' . $keyword);
                $query->orwhere('nama_paket', 'like', '% ' . $keyword . ' %');
            });
        }

        $model  = $model->active();
        $model  = $model->where('tahun_anggaran', $request->tahun_anggaran);
        $model  = $model->where(function ($query) use ($request) {
                for ($i = 0; $i < count($request->lembaga); $i++) {
                    $query->orwhere('instansi_id', $request->lembaga[$i]);
                }
            });
        $model  = $model->where(function ($query) use ($request) {
                if ($request->filled('satker')) {
                    for ($i = 0; $i < count($request->satker); $i++) {
                        $keyword = $request->satker[$i];
                        $query->orwhereHas('satker', function($q) use($keyword) {
                            $q->where('nama', $keyword);
                        });
                    }
                }
            });
            $model = $model->where(function ($query) use ($request) {
                if ($request->filled('jenis_pengadaan')) {
                    for ($i = 0; $i < count($request->jenis_pengadaan); $i++) {
                        $query->orwhere('jenis_pengadaan_id', $request->jenis_pengadaan[$i]);
                    }
                }
            });
        $model  = $model->where(function ($query) use ($request) {
                if ($request->filled('provinsi')) {
                    for ($i = 0; $i < count($request->provinsi); $i++) {
                        if ($request->provinsi[$i] != null) {
                            $query->orwhere('provinsi_id', $request->provinsi[$i]);
                        }
                    }
                }
            });
        $model  = $model->where(function ($query) use ($request) {
                if ($request->filled('kota')) {
                    for ($i = 0; $i < count($request->kota); $i++) {
                        if ($request->kota[$i] != null) {
                            $query->orwhere('kabupaten_id', $request->kota[$i]);
                        }
                    }
                }
            });
        $model  = $model->count('kode_rup');

        return response()->json(['data' => $model, 'message' => 'Berhasil Mengambil Data!']);
    }

    public function getJumlahKata(Request $request)
    {
        $model = Data::where('nama_paket', 'like', '%' . $request->word . '%')
            ->active()
            ->where('tahun_anggaran', $request->tahun_anggaran)
            ->where(function ($query) use ($request) {
                for ($i = 0; $i < count($request->lembaga); $i++) {
                    $query->orwhere('instansi_id', $request->lembaga[$i]);
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('satker')) {
                    for ($i = 0; $i < count($request->satker); $i++) {
                        $keyword = $request->satker[$i];
                        $query->orwhereHas('satker', function($q) use($keyword) {
                            $q->where('nama', $keyword);
                        });
                    }
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('jenis_pengadaan')) {
                    for ($i = 0; $i < count($request->jenis_pengadaan); $i++) {
                        $query->orwhere('jenis_pengadaan_id', $request->jenis_pengadaan[$i]);
                    }
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('provinsi')) {
                    for ($i = 0; $i < count($request->provinsi); $i++) {
                        if ($request->provinsi[$i] != null) {
                            $query->orwhere('provinsi_id', $request->provinsi[$i]);
                        }
                    }
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('kota')) {
                    for ($i = 0; $i < count($request->kota); $i++) {
                        if ($request->kota[$i] != null) {
                            $query->orwhere('kabupaten_id', $request->kota[$i]);
                        }
                    }
                }
            })->select('nama_paket')->get();

        $kata = [];
        //var_dump($request->word);die();
            foreach ($model as $val) {
            $p = preg_split('/[.,\/?!;-]/', $val->nama_paket);
            foreach ($p as $key => $value) {
                //var_dump($p);
                $words = "";
                $exp = explode(" ", $value);
                //foreach ($exp as $i => $item) {
                foreach (str_word_count($value,1) as $item) {
                    if ($item !== "" && strlen($item) >= 3 && !empty($item)) {
                        if (str_word_count($words) < 2) {
                            $words .= " " . $item;
                        }
                    }
                }
                if (str_word_count($words) == 2) {
                    $kata[] = $words;
                }
            }
        }

        $countWord = [];
        foreach ($kata as $i => $val) {
            if (strcasecmp($val, $request->wordwrap)) {
                $countWord[] = $val;
            }
        }

        return response()->json(['data' => count($countWord), 'message' => 'Berhasil Mengambil Data!']);
    }

    public function detailPaketAsli(Request $request)
    {
        $data = Data::select('nama_satuan_kerja', 'metode_pengadaan', 'sumber_dana', 'kode_rup', 'tanggal_awal_pemilihan', 'mak', 'jenis_pengadaan')->find($request->id);
        return response()->json(['status' => true, 'message' => 'Paket Berhasil Di Ambil', 'data' => $data]);
    }


    public function getKabKota(Request $request)
    {
        $data = Kabupaten::where(function ($q) use ($request) {
            foreach ($request->provinsi as $key => $value) {
                $q->orwhere('provinsi_id', $value);
            }
        })->orderBy('nama')->pluck('id', 'nama');

        return response()->json($data);
    }

    public function getProvinsi()
    {
        $data = Provinsi::orderBy('nama')->pluck('id', 'nama');
        return response()->json($data);
    }

    public function getPareto($request)
    {
        // dd($request->kl);
        $res = Data::orderBy('total_pagu_paket', 'desc')
            ->active()
            ->where('tahun_anggaran', $request->tahun_anggaran)
            ->where(function ($query) use ($request) {
                for ($i = 0; $i < count($request->kl); $i++) {
                    $query->orwhere('instansi_id', $request->kl[$i]);
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('satker')) {
                    for ($i = 0; $i < count($request->satker); $i++) {
                        $keyword = $request->satker[$i];
                        $query->orwhereHas('satker', function($q) use($keyword) {
                            $q->where('nama', $keyword);
                        });
                    }
                }
            })
            /*->where(function ($query) use ($request) {
                if ($request->filled('jenis_pengadaan')) {
                    for ($i = 0; $i < count($request->jenis_pengadaan); $i++) {
                        $query->orwhere('jenis_pengadaan_id', $request->jenis_pengadaan[$i]);
                    }
                }
            })*/
            ->where(function ($query) use ($request) {
                if ($request->filled('provinsi')) {
                    for ($i = 0; $i < count($request->provinsi); $i++) {
                        if ($request->provinsi[$i] != null) {
                            $query->orwhere('provinsi_id', $request->provinsi[$i]);
                        }
                    }
                }
            })
            ->where(function ($query) use ($request) {
                if ($request->filled('kota')) {
                    for ($i = 0; $i < count($request->kota); $i++) {
                        $query->orwhere('kabupaten_id', $request->kota[$i]);
                    }
                }
            })
            ->select('kode_rup', 'satker_id', 'nama_paket', 'total_pagu_paket', 'tahun_anggaran', 'metode_pengadaan_id', 'sumber_dana', 'kode_rup', 'tanggal_awal_pemilihan', 'mak', 'jenis_pengadaan_id')
            ->get();

        $total = Data::where('tahun_anggaran', $request->tahun_anggaran)
        ->active()
        ->where(function ($query) use ($request) {
            for ($i = 0; $i < count($request->kl); $i++) {
                $query->orwhere('instansi_id', $request->kl[$i]);
            }
        })
        ->where(function ($query) use ($request) {
            if ($request->filled('satker')) {
                for ($i = 0; $i < count($request->satker); $i++) {
                    $keyword = $request->satker[$i];
                    $query->orwhereHas('satker', function($q) use($keyword) {
                        $q->where('nama', $keyword);
                    });
                }
            }
        })
       /* ->where(function ($query) use ($request) {
            if ($request->filled('jenis_pengadaan')) {
                for ($i = 0; $i < count($request->jenis_pengadaan); $i++) {
                    $query->orwhere('jenis_pengadaan_id', $request->jenis_pengadaan[$i]);
                }
            }
        })*/
        ->where(function ($query) use ($request) {
            if ($request->filled('provinsi')) {
                for ($i = 0; $i < count($request->provinsi); $i++) {
                    if ($request->provinsi[$i] != null) {
                        $query->orwhere('provinsi_id', $request->provinsi[$i]);
                    }
                }
            }
        })
        ->where(function ($query) use ($request) {
            if ($request->filled('kota')) {
                for ($i = 0; $i < count($request->kota); $i++) {
                    $query->orwhere('kabupaten_id', $request->kota[$i]);
                }
            }
        })->sum('total_pagu_paket');

            return [$res,$total];
    }

    public static function fillPareto($res,$total, $filterId)
    {
        
        //$totalPareto = Pareto::where('id_header', $filterId)->count();
        //$totalData = count($res);

        //if ($totalPareto != $totalData) {
            $arr = [];
            foreach ($res as $key => $value) {

                if ($key === 0) {
                    $count = ($value['total_pagu_paket'] * 100) / $total;
                    $percent = round($count, 2);
                    // $arr[] = [
                    //     'id' => $value['kode_rup'],
                    //     'nama_satuan_kerja' => $value['satker_id'],
                    //     'nama_paket' => $value['nama_paket'],
                    //     'total_pagu_paket' => $value['total_pagu_paket'],
                    //     'kumulatif_pagu' => $value['total_pagu_paket'],
                    //     'persentase_terhadap_total' => $percent,
                    //     'klasifikasi_anggaran' => $percent < 80 ? 'BESAR' : 'KECIL',
                    //     'tahun_anggaran' => $value['tahun_anggaran'],
                    //     'metode_pengadaan' => $value['metode_pengadaan_id'],
                    //     'sumber_dana' => $value['sumber_dana'],
                    //     'kode_rup' => $value['kode_rup'],
                    //     'mak' => $value['mak'],
                    //     'jenis_pengadaan' => $value['jenis_pengadaan_id'],
                    //     'tanggal_awal_pemilihan' => $value['tanggal_awal_pemilihan'],
                    // ];
                    $arr[] = [
                        'id_paket' => $value['kode_rup'],
                        'id_header' => $filterId,
                        'nama_paket' => $value['nama_paket'],
                        'pagu_anggaran' => $value['total_pagu_paket'],
                        'kumulatif_pagu' => $value['total_pagu_paket'],
                        'persentase_terhadap_total' => $percent,
                        'klasifikasi_anggaran' => $percent < 80 ? 'BESAR' : 'KECIL'
                    ];
                }

                if ($key > 0) {
                    $kumulatif = $arr[$key - 1];
                    $result = $value['total_pagu_paket'] + $kumulatif['kumulatif_pagu'];

                    $count = ($result * 100) / $total;
                    $percent = round($count, 2);
                    $arr[] = [
                        'id_paket' => $value['kode_rup'],
                        'id_header' => $filterId,
                        'nama_paket' => $value['nama_paket'],
                        'pagu_anggaran' => $value['total_pagu_paket'],
                        'kumulatif_pagu' => $result,
                        'persentase_terhadap_total' => $percent,
                        'klasifikasi_anggaran' => $percent < 80 ? 'BESAR' : 'KECIL'
                    ];
                };
            }

            // DB::beginTransaction();
            try {
                Pareto::where('id_header', $filterId)->delete();

                $insert_data = collect($arr); 

                $chunks = $insert_data->chunk(100);

                foreach ($chunks as $chunk) {

                    //dd($chunk);die();
                    DB::table('pareto')->insert($chunk->toArray());
                }
                // foreach ($arr as $key => $value) {
                //     // DetailPaketKonsolidasi::create([
                //     //     'paket_konsolidasi_id' => $paket_konsolidasi->id,
                //     //     'id_paket' => $value['id'],
                //     //     'nama_paket_asli' => $value['nama_paket'],
                //     //     'pagu_anggaran' => $value['total_pagu_paket'],
                //     //     'kumulatif_pagu' => $value['kumulatif_pagu'],
                //     //     'persentase_terhadap_total' => $value['persentase_terhadap_total'],
                //     //     'klasifikasi_anggaran' => $value['klasifikasi_anggaran'],
                //     //     'id_sirup' => $value['kode_rup'],
                //     //     'nama_satker' => $value['nama_satuan_kerja'],
                //     //     'metode_pengadaan' => $value['metode_pengadaan'],
                //     //     'jenis_pengadaan' => $value['jenis_pengadaan'],
                //     //     'sumber_dana' => $value['sumber_dana'],
                //     //     'kode_akun' => $value['mak']
                //     // ]);

                //     Pareto::updateOrCreate([
                //         'id_paket' => $value['id'],
                //         'id_header' => $filterId,
                //     ],[
                //         'id_paket' => $value['id'],
                //         'id_header' => $filterId,
                //         //'nama_paket' => $value['nama_paket'],
                //         'pagu_anggaran' => $value['total_pagu_paket'],
                //         'kumulatif_pagu' => $value['kumulatif_pagu'],
                //         'persentase_terhadap_total' => $value['persentase_terhadap_total'],
                //         'klasifikasi_anggaran' => $value['klasifikasi_anggaran']
                //     ]);
                //}

                // DB::commit();
            } catch (\Exception $e) {
                // DB::rollBack();
                //Log::info($e->getMessage());

                return response()->json(['success' => false, 'message' => 'Ups Terjadi Kesalahan!', 'err' => $e->getMessage()]);
            }
        //}
        
    }
}
