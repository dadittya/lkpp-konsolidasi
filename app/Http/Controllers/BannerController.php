<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class BannerController extends Controller
{
    public function __construct()
    {
        if (!empty(session('error_msg')))
            Alert::error('Gagal !', session('error_msg'));
        if (!empty(session('success')))
            Alert::success('Success !', session('success'));
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.banner.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.banner.insert');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required'
        ]);

        $image = $request->file('file');
        $imageName = $image->getClientOriginalName();
        $image->move(public_path('images'), $imageName);

        $imageUpload = new Banner;
        $imageUpload->nama = $request->nama;
        $imageUpload->images = $imageName;
        $imageUpload->save();

        return redirect()->route('admin.banner.index')->withSuccess('Berhasil, data berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::find($id);
        return view('admin.banner.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $banner = Banner::find($id);
        $banner->delete();

        return redirect()->route('admin.banner.index')->withSuccess('Berhasil, data berhasil dihapus!');
    }

    public function datatableBanner()
    {
        $model = Banner::query();
        return DataTables::of($model)->addIndexColumn()
            ->addColumn('aksi', function ($d) {
                $btn = '<div class="d-flex">
                        <a class="btn btn-sm btn-primary m-1" href="' . route('admin.banner.edit', ['id' => $d->id]) . '"><i class="fa fa-eye"></i></a>
                        <form method="post" action="' . route('admin.banner.destroy', $d->id) . '" class="form-delete">
                            ' . csrf_field() . '
                            <input type="hidden" name="_method" value="delete" />
                            <button type="submit" class="btn btn-sm btn-danger m-1"><i class="fa fa-trash"></i></button>
                        </form>
                        </div>';
                return $btn;
            })
            ->rawColumns(['aksi'])
            ->make(true);
    }
}
