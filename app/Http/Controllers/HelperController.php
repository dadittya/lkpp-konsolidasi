<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\ErrorLog;
use App\Models\IgnoreKata;
use App\Models\SyncLog;
use App\Models\User;
use GrahamCampbell\ResultType\Success;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class HelperController extends Controller
{
    public static function getProvince() {
        try {
            $response = Http::get(env('PROVINCE_API'));

            return $response;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getKabupaten() {
        try {
            $response = Http::timeout(10000)
                ->get(env('KABUPATEN_API'));

            return $response;
        } catch (\Exception $e) {
            return false;        
        }
    }

    public static function getInstansi() {
        try {
            $response = Http::get(env('INSTANSI_API'));

            return $response;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function getSatker($year, $instansiId) {
        try {
            $url = env('SATKER_API') . $year . ":" . $instansiId;
            //var_dump($url); die();
            $response = Http::get(env('SATKER_API') . $instansiId . ":" . $year);

            //var_dump($response);die();
            return $response;
        } catch (\Exception $e) {
            return false;
        }
    } 
    
    public static function getRup($year, $instansiId) {
        try {
            $url = env('RUP2_API') . $year . ":" . $instansiId;
            //var_dump($url); die();
            $response = Http::get(env('RUP2_API') . $year . ":" . $instansiId);

            //var_dump($response);die();
            return $response;
        } catch (\Exception $e) {
            return false;
        }
    }

    // penyedia
    public static function getRupDetail($year, $instansiId) {
        try {
            $url = env('RUP_API') . $year . ":" . $instansiId;
            //var_dump($url); die();
            $response = Http::get(env('RUP_API') . $year . ":" . $instansiId);

            //var_dump($response);die();
            return $response;
        } catch (\Exception $e) {
            return false;
        }
    }

    //swakelola 
    public static function getRupSwakelola($year, $instansiId) {
        try {
            $url = env('RUP_SWAKEL_API') . $year . ":" . $instansiId;
            //var_dump($url); die();
            $response = Http::get(env('RUP_SWAKEL_API') . $year . ":" . $instansiId);
            
            return $response;
        } catch (\Exception $e) {
            return false;
        }
    }

    public static function setAdmin() {
        $user = User::where('name', 'admin')
                        ->first();
        $user->email = 'direktoratadvokasi42@gmail.com';
        $user->email_verified_at = now();
        if ($user->save()) {
            echo 'success';
        }
        else {
            echo 'failed';
        }       
    }

    public function clearIgnoreList() {
        //$ignoreList = IgnoreKata::truncate();
        //echo "finish";
    }

    public function getLog() {
        $log = SyncLog::orderby('id', 'desc')
                        ->get();

        return $log->toJson();
    }

    public function getErrorLog() {
        $log = ErrorLog::orderby('id', 'desc')
                        ->get();

        return $log->toJson();
    }

    public function clearErrorLog() {
        if (ErrorLog::truncate()) {
            return 'Success';
        } else {
            return 'Fail';
        }
    }

    public function renameBannerFile() {
        $banner = Banner::first();
        $banner->images = "banner.jpg";
        if ($banner->save()) {
            echo "success";
        }
    }

    public function resetMyPassword() {
        $user = User::where('email', 'dadittya.adi@gmail.com')->first();
        $user->password = bcrypt('admin123');
        $user->save();

    }

    public function addAdmin($email) {
        $user = User::where('email', $email)->first();
        if($user) {
            $user->is_admin = 1;
            if ($user->save()) {
                echo 'Success';
            }
            else {
                echo 'Something Wrong';
            }
        }
        else {
            echo 'Not Found';
        }
        
    }
    public function removeAdmin($email) {
        $user = User::where('email', $email)->first();
        if($user) {
            $user->is_admin = 0;
            if ($user->save()) {
                echo 'Success';
            }
            else {
                echo 'Something Wrong';
            }
        }
        else {
            echo 'Not Found';
        }
        
    }
}
