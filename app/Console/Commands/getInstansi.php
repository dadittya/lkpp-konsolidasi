<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\HelperController;
use App\Http\Controllers\SirupController;
use App\Models\Instansi;

class getInstansi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:instansi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        SirupController::getInstansi();
    }
}
