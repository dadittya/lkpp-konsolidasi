<?php

namespace App\Console\Commands;

use App\Http\Controllers\SirupController;
use App\Models\Instansi;
use Illuminate\Console\Command;

class getSatker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:satker';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $year = date("Y");
        $klpd = Instansi::get();

        foreach ($klpd as $val) {
            SirupController::getSatker($year, $val->id);
        }
    }
}
