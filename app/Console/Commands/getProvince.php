<?php

namespace App\Console\Commands;

use App\Http\Controllers\HelperController;
use App\Http\Controllers\SirupController;
use App\Models\Provinsi;
use Illuminate\Console\Command;

class getProvince extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:provinsi';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Province From Inaproc';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
       SirupController::syncProvince();
    }
}
