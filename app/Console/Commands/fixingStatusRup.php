<?php

namespace App\Console\Commands;

use App\Http\Controllers\SirupController;
use Illuminate\Console\Command;
use App\Models\Data;

class fixingStatusRup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fixing:rup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //ini_set('memory_limit', '512M');
        $instansi = Data::wherenull("status_umumkan")
                                        ->where("tahun_anggaran", '2022')
                                        ->where('updated_at', '<', '2022-04-04 00:00:00')
                                        ->select('instansi_id')
                                        ->distinct()
                                        ->groupBy('instansi_id')
                                        ->get();
                                       
        foreach ($instansi as $item) {
            echo $item->instansi_id . "\n";
            SirupController::getRup('2022', $item->instansi_id);
        }                    
        return 0;
    }
}
