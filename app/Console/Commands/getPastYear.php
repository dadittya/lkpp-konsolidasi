<?php

namespace App\Console\Commands;

use App\Http\Controllers\SirupController;
use Illuminate\Console\Command;

class getPastYear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:pastrup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        SirupController::getPastRup();
        return 0;
    }
}
