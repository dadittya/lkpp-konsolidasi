<?php

namespace App\Console\Commands;

use App\Http\Controllers\SirupController;
use App\Models\Instansi;
use Illuminate\Console\Command;

class getRupYear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:rupyear {tahun}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $year = $this->argument('tahun');
        //$instansi = $this->argument('klpd');
            
        $instansis = Instansi::get();

        foreach($instansis as $instansi) {
            echo "Processing : " . $instansi->nama . "\n";
            SirupController::getRup($year, $instansi->id,'past');
        }
        
        return 0;
    }
}
