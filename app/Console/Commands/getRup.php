<?php

namespace App\Console\Commands;

use App\Http\Controllers\SirupController;
use App\Models\Instansi;
use Illuminate\Console\Command;

class getRup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'get:rup {tahun} {klpd}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return in
     */
    public function handle()
    {
        $year = $this->argument('tahun');
        $instansi = $this->argument('klpd');
            
        SirupController::getRup($year, $instansi, 'past');

        return 0;
    }
}
