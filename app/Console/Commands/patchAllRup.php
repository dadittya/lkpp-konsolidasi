<?php

namespace App\Console\Commands;

use App\Http\Controllers\SirupController;
use Illuminate\Console\Command;

class patchAllRup extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'patch:allrup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        SirupController::patchAllRup();
        return 0;
    }
}
