<?php

namespace App\Imports;

use App\Models\IgnoreKata;
use Maatwebsite\Excel\Concerns\ToModel;

class IgnoreKataImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new IgnoreKata([
            'kata' => $row[0],
        ]);
    }
}
