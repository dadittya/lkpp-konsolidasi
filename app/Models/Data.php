<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Data extends Model
{
    use HasFactory;
    protected $table = 'rups';
    protected $primaryKey = 'kode_rup';
    protected $guarded = [];
    public $incrementing = false;
    /*public $fillable = [
        'kode_rup',
        'nama_paket',
        'tahun_anggaran',
        'instansi_id',
        'satker_id',
        'provinsi_id',
        'kabupaten_id',
        'nip_ppk',
        'total_pagu_paket',
        'jenis_pengadaan_id',
        'metode_pengadaan_id',
        'id_rup_client',
        'sumber_dana',
        'mak',
    ];*/

    public function metodePengadaan() {
        return $this->belongsTo(MetodePengadaan::class, 'metode_pengadaan_id', 'id');
    }

    public function jenisPengadaan() {
        return $this->belongsTo(JenisPengadaan::class, 'jenis_pengadaan_id', 'id');
    }

    public function satker() {
        return $this->belongsTo(Satker::class, 'satker_id', 'id');
    }

    public function pareto() {
        return $this->belongsTo(Pareto::class, 'kode_rup', 'id_paket');
    }

    public function scopeActive($query)
    {
        $query->where('is_deleted', 0);
        $query->where('is_active', 1);
        $query->where('status_umumkan', 'Terumumkan');
    }

    public function scopePenyedia($query)
    {
        $query->where('is_swakelola', false);
        $query->whereNull('id_swakelola');
    }

    public function scopeSwakelola($query) 
    {
        $query->where('is_swakelola', true);
    }

    public function scopePenyediaSwakelola($query)
    {
        $query->whereNotNull('id_swakelola')
                ->where('is_swakelola', false);
    }

    public function scopeCountOnReport($query)
    {
        $query->where(function($query) {
            $query->where('is_swakelola', false);
            $query->orwhereNotNull('id_swakelola');
        });
        
    }
}
