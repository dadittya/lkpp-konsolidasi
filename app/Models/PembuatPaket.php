<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PembuatPaket extends Model
{
    use HasFactory;
    protected $table = 'pembuat_paket';
    protected $guarded = [];

    public function paketKonsolidasi()
    {
        return $this->hasMany(PaketKonsolidasi::class,'id','paket_konsolidasi_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
}
