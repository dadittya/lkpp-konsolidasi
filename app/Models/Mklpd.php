<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Mklpd extends Model
{
    use HasFactory;
    protected $table = 'm_klpd';
    protected $guarded = [];
}
