<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pareto extends Model
{
    use HasFactory;
    protected $table = 'pareto';
    protected $guarded = [];

    public function filter() {
        return $this->belongsTo(ParetoFilter::class, 'id_header');
    }
}
