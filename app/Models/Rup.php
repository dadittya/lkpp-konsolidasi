<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Rup extends Model
{
    use HasFactory;
    protected $table = 'rups';
    protected $primaryKey = 'kode_rup';
    protected $guarded = [];
    public $incrementing = false;
    public $fillable = [
        'kode_rup',
        'nama_paket',
        'tahun_anggaran',
        'instansi_id',
        'satker_id',
        'provinsi_id',
        'kabupaten_id',
        'nip_ppk',
        'total_pagu_paket',
        'jenis_pengadaan_id',
        'metode_pengadaan_id',
        'id_rup_client',
        'sumber_dana',
        'mak',
        'id_swakelola',
        'is_active',
        'is_swakelola',
        'is_kegiatan',
        'is_deleted',
        'status_umumkan',
        'statususahakecil'
    ];

    public function instansi()
    {
        return $this->belongsTo(Instansi::class, 'instansi_id', 'id');
    }

    public function satker()
    {
        return $this->belongsTo(Satker::class, 'satker_id', 'id');
    }
}
