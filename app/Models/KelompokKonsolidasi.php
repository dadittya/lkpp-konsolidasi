<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KelompokKonsolidasi extends Model
{
    use HasFactory;
    protected $table = 'kelompok_konsolidasi';
    protected $guarded = [];

    public function kelompokDetail()
    {
        return $this->hasMany(KelompokKonsolidasiDetail::class,'kelompok_konsolidasi_id','id');
    }

    public function getNamaPaketKonsolidasiAttribute($value) {
        return ucwords($value);
    }
}
