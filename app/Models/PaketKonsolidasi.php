<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaketKonsolidasi extends Model
{
    use HasFactory;
    protected $table = 'paket_konsolidasis';
    protected $guarded = [];

    public function detailPaketKonsolidasi()
    {
        return $this->hasMany(DetailPaketKonsolidasi::class,'paket_konsolidasi_id','id');
    }

    public function user()
    {
        return $this->belongsTo(PembuatPaket::class,'id','paket_konsolidasi_id');
    }
}
