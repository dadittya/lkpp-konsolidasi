<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailPaketKonsolidasi extends Model
{
    use HasFactory;
    protected $table = 'detail_paket_konsolidasis';
    protected $guarded = [];

    public function paketKonsolidasi()
    {
        return $this->belongsTo(PaketKonsolidasi::class,'paket_konsolidasi_id','id');
    }

    public function satker()
    {
        return $this->belongsTo(Satker::class, 'nama_satker', 'id');
    }

    public function rup()
    {
        return $this->belongsTo(Rup::class, 'id_sirup', 'kode_rup');
    }
}
