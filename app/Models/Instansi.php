<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Instansi extends Model
{
    use HasFactory;
    public $incrementing = false;
    protected $dates = ['last_update'];

    protected $fillable = [
        'id',
        'nama',
        'jenis',
        'alamat',
        'provinsi_id',
        'kabupaten_id',
    ];
}
