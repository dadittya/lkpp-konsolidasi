<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MetodePengadaan extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $table = 'metode_pengadaan';
    protected $fillable = ['id','nama'];
}
