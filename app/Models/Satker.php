<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Satker extends Model
{
    use HasFactory;
    public $incrementing = false;

    protected $fillable = [
        'id',
        'id_str',
        'nama',
        'status',
        'keterangan',
        'jenis',
        'instansi_id',
        'status_satker',
        'show'
    ];
}
