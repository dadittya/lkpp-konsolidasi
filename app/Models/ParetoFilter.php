<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParetoFilter extends Model
{
    use HasFactory;
    protected $fillable = ['ip', 'nama_kementrian_lembaga', 'nama_satuan_kerja', 'jenis_pengadaan', 'provinsi', 'pemkot', 'tahun'];

    // protected $casts = [
    //     'nama_kementrian_lembaga' => 'array',
    //     'nama_satuan_kerja' => 'array',
    //     'jenis_pengadaan' => 'array',
    //     'provinsi' => 'array',
    //     'pemkot' => 'array',
    // ];

    public function paretos() {
        return $this->hasMany(Pareto::class, 'id_header');
    }
}
