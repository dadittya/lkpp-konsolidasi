<?php

namespace App\Models;

use App\Http\Controllers\KelompokKonsolidasi;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KelompokKonsolidasiDetail extends Model
{
    use HasFactory;
    protected $table = 'kelompok_konsolidasi_detail';
    protected $guarded = [];


    public function kelompok_konsolidasi()
    {
        return $this->belongsTo(KelompokKonsolidasi::class,'id','kelompok_konsolidasi_id');
    }

    public function paketKonsolidasi()
    {
        return $this->belongsTo(PaketKonsolidasi::class,'paket_konsolidasi_id','id');
    }
}
